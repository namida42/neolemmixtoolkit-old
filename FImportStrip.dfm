object F_ImportStrip: TF_ImportStrip
  Left = 203
  Top = 222
  AutoScroll = False
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Import Image'
  ClientHeight = 369
  ClientWidth = 337
  Color = clBtnFace
  Constraints.MinHeight = 369
  Constraints.MinWidth = 337
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnResize = FormResize
  DesignSize = (
    337
    369)
  PixelsPerInch = 96
  TextHeight = 13
  object imgFrame: TImage32
    Left = 48
    Top = 24
    Width = 241
    Height = 241
    Anchors = [akLeft, akTop, akRight, akBottom]
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clBtnFace
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smNormal
    TabOrder = 0
    OnClick = imgFrameClick
  end
  object btnCancel: TButton
    Left = 176
    Top = 328
    Width = 97
    Height = 33
    Anchors = [akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object btnOK: TButton
    Left = 64
    Top = 328
    Width = 97
    Height = 33
    Anchors = [akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object rgTransparency: TRadioGroup
    Left = 96
    Top = 272
    Width = 145
    Height = 49
    Anchors = [akBottom]
    Caption = 'Transparency'
    Ctl3D = True
    ItemIndex = 0
    Items.Strings = (
      'Use Alpha Channel'
      'Use Transparent Color')
    ParentCtl3D = False
    TabOrder = 3
    OnClick = rgTransparencyClick
  end
end
