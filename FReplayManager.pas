unit FReplayManager;

interface

uses
  CustomPopup,
  FlexiRanks, DatManage, OpenSaveDlgs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FTSysDat, ComCtrls;

type
  TStringArray = array of String;

  TReplayManagerForm = class(TForm)
    btnOK: TButton;
    lvReplays: TListView;
    btnAdd: TButton;
    btnDelete: TButton;
    btnReplace: TButton;
    Label1: TLabel;
    Label2: TLabel;
    ebFilename: TEdit;
    ebNotes: TEdit;
    Label3: TLabel;
    cbClassification: TComboBox;
    cbSuccess: TCheckBox;
    cbUpToDate: TCheckBox;
    btnAllSuccess: TButton;
    btnAllUpToDate: TButton;
    btnSpread: TButton;
    btnExport: TButton;
    btnDeleteFailed: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnReplaceClick(Sender: TObject);
    procedure SetLabels(aValue: TStringArray);
    procedure lvReplaysClick(Sender: TObject);
    procedure ebExit(Sender: TObject);
    procedure ebFilenameChange(Sender: TObject);
    procedure ebNotesChange(Sender: TObject);
    procedure cbClassificationChange(Sender: TObject);
    procedure cbSuccessClick(Sender: TObject);
    procedure cbUpToDateClick(Sender: TObject);
    procedure btnAllSuccessClick(Sender: TObject);
    procedure btnAllUpToDateClick(Sender: TObject);
    procedure btnSpreadClick(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure btnDeleteFailedClick(Sender: TObject);
  private
    fUsedSpread: Boolean;
    fLabels: TStringArray;
    fRanks: TGameRanks;
    fIndexes: array of TReplayStream;
    fListViewWndProc: TWndMethod;
    procedure ListViewWndProc(var Msg: TMessage);
    procedure GenerateList(ClearSelection: Boolean = false);
  public
    constructor Create(Owner: TComponent; aRanks: TGameRanks);
    property Labels: TStringArray read fLabels write SetLabels;
  end;

implementation

uses
  FReplayExport;

{$R *.dfm}

constructor TReplayManagerForm.Create(Owner: TComponent; aRanks: TGameRanks);
begin
  inherited Create(Owner);

  if not (Owner is TForm) then Exit;
  Left := ((TForm(Owner).Width - Width) div 2) + TForm(Owner).Left;
  Top := ((TForm(Owner).Height - Height) div 2) + TForm(Owner).Top;

  fRanks := aRanks;

  fListViewWndProc := lvReplays.WindowProc; // save old window proc
  lvReplays.WindowProc := ListViewWndProc; // subclass
end;

procedure TReplayManagerForm.FormDestroy(Sender: TObject);
begin
  lvReplays.WindowProc := fListViewWndProc;
end;

procedure TReplayManagerForm.GenerateList(ClearSelection: Boolean = false);
var
  r, l, i: Integer;
  Level: TDatSection;
  Replays: TReplayStreams;
  Replay: TReplayStream;
  LI: TListItem;
  CacheCRC: Cardinal;

  ActualLen: Integer;

  procedure EnsureLen;
  begin
    if Length(fIndexes) = ActualLen then
      SetLength(fIndexes, ActualLen+50);
    // No need to cut it down afterwards. Having
    // a few extras doesn't do any harm.
  end;

  procedure LogIndex(aReplay: TReplayStream);
  begin
    EnsureLen;
    fIndexes[ActualLen] := aReplay;
    Inc(ActualLen);
  end;

begin
  if not Assigned(lvReplays.OnClick) then Exit; // good way to test if this is already mid-run

  try
    lvReplays.OnClick := nil;
    ActualLen := 0;
    lvReplays.Items.BeginUpdate;

    for r := 0 to fRanks.Count-1 do
      for l := 0 to fRanks[r].Levels.Count-1 do
      begin
        Level := fRanks[r].Levels[l];
        Replays := Level.Replays;

        CacheCRC := Level.CRC; // avoid re-calculating it every time

        for i := 0 to Replays.Count-1 do
        begin
          Replay := Replays[i];
          if ActualLen < lvReplays.Items.Count then
          begin
            LI := lvReplays.Items[ActualLen];
            LI.SubItems.Clear;
          end else
            LI := lvReplays.Items.Add;
          LI.Caption := Level.LevelTitle + ' (' + fRanks[r].RankName + ' ' + IntToStr(l + 1) + ')';
          if Replay.Classification < Length(fLabels) then
            LI.SubItems.Add(fLabels[Replay.Classification])
          else
            LI.SubItems.Add('<unknown: ' + IntToStr(Replay.Classification) + '>');
          if Replay.Success then
            LI.SubItems.Add('Yes')
          else
            LI.SubItems.Add('No');

          if Replay.IsNXRP then
            LI.SubItems.Add('NXRP')
          else
            LI.SubItems.Add('LRB');

          if Replay.CRC = CacheCRC then
            LI.SubItems.Add(Replay.Note)
          else
            LI.SubItems.Add('< OUTDATED > ' + Replay.Note);

          LogIndex(Replay);
        end;
      end;

    while ActualLen < lvReplays.Items.Count do
      lvReplays.Items.Delete(ActualLen);

    if ClearSelection then
      lvReplays.ItemIndex := -1;
    lvReplays.Items.EndUpdate;

    if lvReplays.ItemIndex = -1 then
    begin
      ebFilename.Text := '';
      ebFilename.Enabled := false;

      ebNotes.Text := '';
      ebNotes.Enabled := false;

      cbClassification.ItemIndex := -1;
      cbClassification.Enabled := false;

      cbSuccess.Checked := false;
      cbSuccess.Enabled := false;

      cbUpToDate.Checked := false;
      cbUpToDate.Enabled := false;

      btnDelete.Enabled := false;
      btnReplace.Enabled := false;
      btnSpread.Enabled := false;
    end else begin
      Replay := fIndexes[lvReplays.ItemIndex];

      ebFilename.Text := Replay.Name;
      ebFilename.Enabled := true;

      ebNotes.Text := Replay.Note;
      ebNotes.Enabled := true;

      cbClassification.ItemIndex := Replay.Classification;
      cbClassification.Enabled := true;

      cbSuccess.Checked := Replay.Success;
      cbSuccess.Enabled := true;

      cbUpToDate.Checked := Replay.CRC = fRanks.Levels[Replay.LevelID].CRC;
      cbUpToDate.Enabled := true;

      btnDelete.Enabled := true;
      btnReplace.Enabled := true;
      btnSpread.Enabled := true;
    end;
  finally
    lvReplays.OnClick := lvReplaysClick;
  end;
end;

procedure TReplayManagerForm.FormShow(Sender: TObject);
begin
  GenerateList(true);
end;

procedure TReplayManagerForm.ListViewWndProc(var Msg: TMessage);
begin
  ShowScrollBar(lvReplays.Handle, SB_HORZ, false);
  ShowScrollBar(lvReplays.Handle, SB_VERT, true);
  fListViewWndProc(Msg); // process message
end;

procedure TReplayManagerForm.btnAddClick(Sender: TObject);
var
  Open: TOpenMulti;
  i, i2: Integer;

  Replay: TReplayStream;
  Level: TDatSection;

  ToSelect: TReplayStream;
begin
  ToSelect := nil;

  Open := TOpenMulti.Create('Replay Files (*.lrb, *.nxrp)|*.lrb;*.nxrp');
  for i := 0 to Open.Count-1 do
  begin
    Replay := TReplayStream.Create;
    Replay.LoadFromFile(Open[i]);
    Level := fRanks.Levels[Replay.LevelID];
    if Level = nil then
      Replay.Free
    else begin
      Replay.Name := ExtractFileName(ChangeFileExt(Open[i], '')) + LeadZeroStr(Level.Replays.Add(Replay), 3);
      Replay.CRC := Level.CRC;
      if ToSelect = nil then ToSelect := Replay;
    end;
  end;
  Open.Free;

  GenerateList;

  if ToSelect <> nil then
    for i := 0 to lvReplays.Items.Count-1 do
      if fIndexes[i] = ToSelect then
      begin
        lvReplays.ItemIndex := i;
        Exit;
      end;
end;

procedure TReplayManagerForm.btnDeleteClick(Sender: TObject);
var
  Replay: TReplayStream;
  Level: TDatSection;
  i: Integer;
begin
  if lvReplays.ItemIndex = -1 then Exit;

  Replay := fIndexes[lvReplays.ItemIndex];
  Level := fRanks.Levels[Replay.LevelID];

  for i := 0 to Level.Replays.Count-1 do
    if Level.Replays[i] = Replay then
    begin
      Level.Replays.Delete(i);
      Break;
    end;

  GenerateList(true);
end;

procedure TReplayManagerForm.btnReplaceClick(Sender: TObject);
var
  s: String;
  Replay: TReplayStream;
  Level: TDatSection;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];

  S := OpenDialog('Replay Files (*.lrb, *.nxrp)|*.lrb;*.nxrp');
  if S = '' then Exit;

  Level := fRanks.Levels[Replay.LevelID];

  Replay.ClearData;
  Replay.LoadFromFile(S);
  Replay.CRC := Level.CRC;
end;

procedure TReplayManagerForm.SetLabels(aValue: TStringArray);
var
  i: Integer;
begin
  fLabels := aValue;
  cbClassification.Items.Clear;
  for i := 0 to Length(fLabels)-1 do
    cbClassification.Items.Add(fLabels[i]);
end;

procedure TReplayManagerForm.lvReplaysClick(Sender: TObject);
begin
  GenerateList;
end;

procedure TReplayManagerForm.ebExit(Sender: TObject);
begin
  GenerateList;
end;

procedure TReplayManagerForm.ebFilenameChange(Sender: TObject);
var
  Replay: TReplayStream;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];
  Replay.Name := ebFilename.Text;
end;

procedure TReplayManagerForm.ebNotesChange(Sender: TObject);
var
  Replay: TReplayStream;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];
  Replay.Note := ebNotes.Text;
end;

procedure TReplayManagerForm.cbClassificationChange(Sender: TObject);
var
  Replay: TReplayStream;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];
  Replay.Classification := cbClassification.ItemIndex;
  GenerateList;
end;

procedure TReplayManagerForm.cbSuccessClick(Sender: TObject);
var
  Replay: TReplayStream;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];
  Replay.Success := cbSuccess.Checked;
  GenerateList;
end;

procedure TReplayManagerForm.cbUpToDateClick(Sender: TObject);
var
  Replay: TReplayStream;
  Level: TDatSection;
begin
  if lvReplays.ItemIndex = -1 then Exit;
  Replay := fIndexes[lvReplays.ItemIndex];

  if cbUpToDate.Checked then
  begin
    Level := fRanks.Levels[Replay.LevelID];
    Replay.CRC := Level.CRC;
  end else
    Replay.CRC := 0;

end;

procedure TReplayManagerForm.btnAllSuccessClick(Sender: TObject);
var
  i: Integer;
  Replay: TReplayStream;
begin
  for i := 0 to lvReplays.Items.Count-1 do
  begin
    Replay := fIndexes[i];
    Replay.Success := true;
  end;

  btnAllUpToDateClick(Sender);
  GenerateList;
end;

procedure TReplayManagerForm.btnAllUpToDateClick(Sender: TObject);
var
  i: Integer;
  Replay: TReplayStream;
  Level: TDatSection;
  CacheCRC: Cardinal;
begin
  if Sender <> btnAllUpToDate then
    if MessageDlg('Also mark all replays as up-to-date?', mtCustom, [mbYes, mbNo], 0) = mrNo then
      Exit;

  Level := nil;
  CacheCRC := 0;

  for i := 0 to lvReplays.Items.Count-1 do
  begin
    Replay := fIndexes[i];
    if fRanks.Levels[Replay.LevelID] <> Level then
    begin
      Level := fRanks.Levels[Replay.LevelID];
      CacheCRC := Level.CRC;
    end;
    Replay.CRC := CacheCRC;
  end;

  if Sender = btnAllUpToDate then
    GenerateList;
end;

procedure TReplayManagerForm.btnSpreadClick(Sender: TObject);
var
  Target: Integer;
  i: Integer;
begin
  if lvReplays.ItemIndex = -1 then Exit;

  if not fUsedSpread then
    if MessageDlg('This will copy the classification of the selected replay to all replays that have the "' + fLabels[0] + '" classification.', mtCustom, [mbOk, mbCancel], 0) = mrCancel then
      Exit;

  fUsedSpread := true;

  Target := fIndexes[lvReplays.ItemIndex].Classification;

  for i := 0 to lvReplays.Items.Count-1 do
    if fIndexes[i].Classification = 0 then
      fIndexes[i].Classification := Target;
end;

procedure TReplayManagerForm.btnExportClick(Sender: TObject);
var
  F: TReplayExportForm;
  Replay: TReplayStream;
  Level: TDatSection;
  i: Integer;
  S: String;
  R, L: Integer;

  procedure FixInvalidFilename;
  var
    c, ci: Integer;
  const
    INVALID_CHARS = '\/:*?"<>|.'; // period is allowed technically, but looks messy
  begin
    for c := 1 to Length(S) do
      for ci := 1 to Length(INVALID_CHARS) do
        if S[c] = INVALID_CHARS[ci] then S[c] := '_';
  end;

  procedure SetRankAndLevel(aLevel: TDatSection);
  var
    LocR, LocL: Integer;
  begin
    for LocR := 0 to fRanks.Count-1 do
      for LocL := 0 to fRanks[LocR].Levels.Count-1 do
        if fRanks[LocR].Levels[LocL] = aLevel then
        begin
          R := LocR;
          L := LocL;
          Exit;
        end;
  end;

  function CheckIfExportReplay(aReplay: TReplayStream): Boolean;
  var
    Nature: Boolean;
  begin
    Result := F.IncludeBySuccess[Replay.Success]
          and F.IncludeByClassification[Replay.Classification];

    if not Result then Exit; // save working out the rest

    if Level = nil then
      Nature := false
    else
      Nature := aReplay.CRC = Level.CRC;

    Result := Result and F.IncludeByUpToDate[Nature];
  end;
begin
  F := TReplayExportForm.Create(self);
  F.Labels := fLabels;

  if F.ShowModal = mrCancel then Exit;

  for i := 0 to lvReplays.Items.Count-1 do
  begin
    Replay := fIndexes[i];
    Level := fRanks.Levels[Replay.LevelID];

    if not CheckIfExportReplay(Replay) then Continue;

    case F.rgNaming.ItemIndex of
      0: S := Trim(Level.LevelTitle);
      1: begin
           SetRankAndLevel(Level);
           S := LeadZeroStr(R+1, 2) + LeadZeroStr(L+1, 2) + ' ' + Trim(Level.LevelTitle);
         end;
      2: begin
           SetRankAndLevel(Level);
           S := fRanks[R].RankName + ' ' + LeadZeroStr(L+1, 2) + ' ' + Trim(Level.LevelTitle);
         end;
      else raise exception.Create('TReplayManagerForm.btnExportClick invalid naming scheme option');
    end;

    S := S + ' - ' + Replay.Name;

    FixInvalidFilename;

    if Replay.IsNXRP then
      Replay.SaveToFile(F.Target + S + '.nxrp')
    else
      Replay.SaveToFile(F.Target + S + '.lrb');
  end;

  F.Free;
end;

procedure TReplayManagerForm.btnDeleteFailedClick(Sender: TObject);
var
  R, L, i: Integer;
begin
  if RunCustomPopup(self, 'Delete Failed Replays?', 'Are you sure you want to delete all unsuccessful replays?', 'Yes|No') = 2 then Exit;
  for R := 0 to fRanks.Count-1 do
    for L := 0 to fRanks[R].Levels.Count-1 do
      with fRanks[R].Levels[L] do
        for i := Replays.Count-1 downto 0 do
          if not Replays[i].Success then
            Replays.Delete(i);
  GenerateList(true);
end;

end.
