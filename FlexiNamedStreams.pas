unit FlexiNamedStreams;

interface

uses
  Classes, SysUtils, Contnrs;

type
  TNamedMemoryStream = class(TMemoryStream)
  private
    fName: String;
  public
    property Name: String read fName write fName;
  end;

  TNamedStreams = class(TObjectList)
  private
    function GetItem(Index: Integer): TNamedMemoryStream;
  public
    function Add(Item: TNamedMemoryStream): Integer;
    procedure Insert(Index: Integer; Item: TNamedMemoryStream);
    procedure SortByNames;
    property Items[Index: Integer]: TNamedMemoryStream read GetItem; default;
    property List;
  end;

  function NameSort(L, R: Pointer): Integer;   

implementation

{ TNamedStreams }

function TNamedStreams.Add(Item: TNamedMemoryStream): Integer;
begin
  Result := inherited Add(Item);
end;

function TNamedStreams.GetItem(Index: Integer): TNamedMemoryStream;
begin
  Result := inherited Get(Index);
end;

procedure TNamedStreams.Insert(Index: Integer; Item: TNamedMemoryStream);
begin
  inherited Insert(Index, Item);
end;

procedure TNamedStreams.SortByNames;
begin
  Sort(NameSort);
end;

function NameSort(L, R: Pointer): Integer;
begin
  Result := CompareStr(TNamedMemoryStream(L).Name, TNamedMemoryStream(R).Name);
end;

end.