unit FlexiRanks;

interface

uses
  FTSysDat, Contnrs, DatManage, StrUtils, Classes, SysUtils;

type
  TGameRank = class
    private
      fName: String;
      fSecretLevels: Integer;
      fLevels: TSections;
    public
      constructor Create;
      destructor Destroy; override;
      property RankName: String read fName write fName;
      property SecretLevels: Integer read fSecretLevels write fSecretLevels;
      property Levels: TSections read fLevels;
  end;

  TGameRanks = class(TObjectList)
  private
    fSysDat: TSystemDat;
    function GetItem(Index: Integer): TGameRank;
    function GetLevelByID(aID: Cardinal): TDatSection;
  public
    constructor Create(aSysDat: TSystemDat);
    function Add(Item: TGameRank): Integer;
    procedure Insert(Index: Integer; Item: TGameRank);
    procedure GetFromSysDat;
    procedure SetToSysDat;
    //procedure SaveDatFiles(aPath: String);
    procedure SaveLvlFiles(aPath: String);
    property Items[Index: Integer]: TGameRank read GetItem; default;
    property List;
    property Levels[ID: Cardinal]: TDatSection read GetLevelByID;
  end;

implementation

{ TGameRank }

constructor TGameRank.Create;
begin
  inherited;
  fLevels := TSections.Create;
end;

destructor TGameRank.Destroy;
begin
  fLevels.Free;
  inherited;
end;

{ TGameRanks }

constructor TGameRanks.Create(aSysDat: TSystemDat);
begin
  inherited Create;
  fSysDat := aSysDat;
end;

function TGameRanks.GetLevelByID(aID: Cardinal): TDatSection;
var
  r, l: Integer;
begin
  for r := 0 to Count-1 do
    for l := 0 to Items[r].Levels.Count-1 do
      if Items[r].Levels[l].LevelID = aID then
      begin
        Result := Items[r].Levels[l];
        Exit;
      end;
  Result := nil;
end;

function TGameRanks.Add(Item: TGameRank): Integer;
begin
  Result := inherited Add(Item);
end;

function TGameRanks.GetItem(Index: Integer): TGameRank;
begin
  Result := inherited Get(Index);
end;

procedure TGameRanks.Insert(Index: Integer; Item: TGameRank);
begin
  inherited Insert(Index, Item);
end;

procedure TGameRanks.GetFromSysDat;
var
  i, i2: Integer;
  TempRank: TGameRank;
  BasePath: String;
  SL, RSL: TStringList;
  S: String;
  RC: Cardinal;
begin
  Clear;
  BasePath := GetCurrentDir;
  if RightStr(BasePath, 1) <> '\' then BasePath := BasePath + '\';
  SL := TStringList.Create;
  RSL := TStringList.Create;
  RSL.Clear;
  SL.LoadFromFile(BasePath + 'levels\levels.ini');
  if FileExists(BasePath + 'replays\replays.ini') then
    RSL.LoadFromFile(BasePath + 'replays\replays.ini');
  for i := 0 to fSysDat.RankCount-1 do
  begin
    TempRank := TGameRank.Create;
    TempRank.RankName := fSysDat.RankName[i];
    {if FileExists(BasePath + 'LEVEL' + LeadZeroStr(i, 3) + '.DAT') then TempRank.Levels.LoadFromFile('LEVEL' + LeadZeroStr(i, 3) + '.DAT');
    i2 := 0;
    while FileExists(BasePath + 'levels\' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.lvl') or (i2 < TempRank.Levels.Count) do
    begin
      if FileExists(BasePath + 'levels\' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.lvl') then
      begin
        if i2 = TempRank.Levels.Count then TempRank.Levels.Add(TDatSection.Create);
        TempRank.Levels[i2].LoadFromFile(BasePath + 'levels\' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.lvl');
      end;
      Inc(i2);
    end;}
    for i2 := 0 to 254 do
    begin
      S := SL.Values[LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2)];
      RC := StrToIntDef('x' + SL.Values['R' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2)], 0);
      if S <> '' then
      begin
        TempRank.Levels.Add(TDatSection.Create);
        TempRank.Levels[i2].LoadFromFile(BasePath + 'levels\' + S);
        TempRank.Levels[i2].FileName := S;

        S := ChangeFileExt(S, '.lrb');
        if FileExists(BasePath + 'replays\' + S) then  // backwards compatibility
        begin
          with TempRank.Levels[i2].Replays.Add do
          begin
            LoadFromFile(BasePath + 'replays\' + S);
            CRC := RC;
            Name := '000';
            Success := true; // assumed
          end;
        end;

        if RSL.Count <> 0 then
          TempRank.Levels[i2].Replays.Load(BasePath, TempRank.Levels[i2].FileName, RSL);
      end else
        Break;
    end;
    Add(TempRank);
  end;
  SL.Free;
  RSL.Free;
end;

procedure TGameRanks.SetToSysDat;
var
  i: Integer;
begin
  fSysDat.RankCount := Count;
  for i := 0 to Count-1 do
  begin
    fSysDat.RankName[i] := Items[i].RankName;
  end;
  for i := Count to 14 do
  begin
    fSysDat.RankName[i] := '';
  end;
end;

(*procedure TGameRanks.SaveDatFiles(aPath: String);
var
  i: Integer;
begin
  aPath := ExtractFilePath(aPath);
  for i := 0 to Count-1 do
  begin
    Items[i].Levels.SaveToFile(aPath + 'LEVEL' + LeadZeroStr(i, 3) + '.DAT');
  end;
end;*)

procedure TGameRanks.SaveLvlFiles(aPath: String);
var
  i, i2, i3: Integer;
  SL: TStringList;
  SaveName, BaseSaveName: String;
  DoSave: Boolean;

  function CheckIfIdentical(aName: String): Boolean;
  var
    MS: TMemoryStream;
    FS: TFileStream;
    b1, b2: Byte;
  begin
    Result := false;
    FS := TFileStream.Create(aPath + 'levels\' + aName, fmOpenRead);
    try
      MS := Items[i].Levels[i2];
      if FS.Size <> MS.Size then Exit;
      MS.Position := 0;
      FS.Position := 0;
      while MS.Position < MS.Size do
      begin
        MS.Read(b1, 1);
        FS.Read(b2, 1);
        if b1 <> b2 then Exit;
      end;
      Result := true;
    finally
      FS.Free;
    end;
  end;
begin
  aPath := ExtractFilePath(aPath);
  ForceDirectories(aPath + 'levels\');
  ForceDirectories(aPath + 'replays\');
  SL := TStringList.Create;
  for i := 0 to Count-1 do
    for i2 := 0 to Items[i].Levels.Count-1 do
    begin
      DoSave := true;
      BaseSaveName := Items[i].Levels[i2].FileName;
      SaveName := BaseSaveName;
      i3 := 0;
      while FileExists(aPath + 'levels\' + SaveName) do
      begin
        if CheckIfIdentical(SaveName) then
        begin
          DoSave := false;
          Break;
        end else begin
          Inc(i3);
          SaveName := ChangeFileExt(ExtractFileName(BaseSaveName), '') + '(' + IntToStr(i3) + ')' + ExtractFileExt(BaseSaveName);
        end;
      end;
      Items[i].Levels[i2].FileName := SaveName;
      SL.Add(LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '=' + SaveName);
      if DoSave then
        Items[i].Levels[i2].SaveToFile(aPath + 'levels\' + SaveName);
    end;

  SL.Add('');

  // Now, save the replays;
  (*for i := 0 to Count-1 do
    for i2 := 0 to Items[i].Levels.Count-1 do
    begin
      if Items[i].Levels[i2].Replay.Size = 0 then Continue;
      SaveName := ChangeFileExt(Items[i].Levels[i2].FileName, '.lrb');
      if not FileExists(aPath + 'replays\' + SaveName) then
        Items[i].Levels[i2].Replay.SaveToFile(aPath + 'replays\' + SaveName);
      SL.Add('R' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '=' + IntToHex(Items[i].Levels[i2].ReplayCRC, 8));
    end;*)

  SL.SaveToFile(aPath + 'levels\levels.ini');
  SL.Free;
end;

end.