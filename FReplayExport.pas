unit FReplayExport;

interface

uses
  FlexiRanks, OpenSaveDlgs, FReplayManager,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FTSysDat, ComCtrls;

type
  TReplayExportForm = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    rgUpToDate: TRadioGroup;
    rgSolving: TRadioGroup;
    lbClassifications: TListBox;
    Label1: TLabel;
    rgNaming: TRadioGroup;
    procedure btnOKClick(Sender: TObject);
  private
    fLabels: TStringArray;
    fTarget: String;
    procedure SetLabels(aValue: TStringArray);
    function GetInclude(Nature: Boolean; Condition: Integer): Boolean;
    function GetIncludeClassification(Index: Integer): Boolean;
  public
    constructor Create(Owner: TComponent);
    property Labels: TStringArray read fLabels write SetLabels;
    property Target: String read fTarget;
    property IncludeByUpToDate[Nature: Boolean]: Boolean index 0 read GetInclude;
    property IncludeBySuccess[Nature: Boolean]: Boolean index 1 read GetInclude;
    property IncludeByClassification[Index: Integer]: Boolean read GetIncludeClassification;
  end;

implementation

{$R *.dfm}

constructor TReplayExportForm.Create(Owner: TComponent);
begin
  inherited Create(Owner);

  if not (Owner is TForm) then Exit;
  Left := ((TForm(Owner).Width - Width) div 2) + TForm(Owner).Left;
  Top := ((TForm(Owner).Height - Height) div 2) + TForm(Owner).Top;
end;

procedure TReplayExportForm.SetLabels(aValue: TStringArray);
var
  i: Integer;
begin
  fLabels := aValue;

  lbClassifications.Clear;
  for i := 0 to Length(fLabels)-1 do
    lbClassifications.Items.Add(fLabels[i]);

  for i := 0 to Length(fLabels)-1 do
    lbClassifications.Selected[i] := true;
end;

function TReplayExportForm.GetInclude(Nature: Boolean; Condition: Integer): Boolean;
var
  Src: TRadioGroup;
begin
  case Condition of
    0: Src := rgUpToDate;
    1: Src := rgSolving;
    else raise exception.Create('TReplayExportForm.GetInclude called for invalid condition');
  end;

  case Src.ItemIndex of
    0: Result := true;
    1: Result := Nature;
    2: Result := not Nature;
    else raise exception.Create('TReplayExportForm.GetInclude a selection has not been made');
  end;
end;

function TReplayExportForm.GetIncludeClassification(Index: Integer): Boolean;
begin
  Result := lbClassifications.Selected[Index];
end;

procedure TReplayExportForm.btnOKClick(Sender: TObject);
begin
  fTarget := SaveDialog('<choose any filename in target folder>|*');
  fTarget := ExtractFilePath(fTarget);
  if fTarget <> '' then ModalResult := mrOk;
end;

end.
