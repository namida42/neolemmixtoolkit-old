object ReplayExportForm: TReplayExportForm
  Left = 545
  Top = 244
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Replay Export'
  ClientHeight = 401
  ClientWidth = 201
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 152
    Width = 66
    Height = 13
    Caption = 'Classifications'
  end
  object btnOK: TButton
    Left = 8
    Top = 368
    Width = 89
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 104
    Top = 368
    Width = 89
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object rgUpToDate: TRadioGroup
    Left = 8
    Top = 8
    Width = 185
    Height = 65
    Caption = 'Up To Date Status'
    ItemIndex = 0
    Items.Strings = (
      'All Replays'
      'Up-To-Date Replays Only'
      'Outdated Replays Only')
    TabOrder = 2
  end
  object rgSolving: TRadioGroup
    Left = 8
    Top = 80
    Width = 185
    Height = 65
    Caption = 'Solving Status'
    ItemIndex = 0
    Items.Strings = (
      'All Replays'
      'Successful Replays Only'
      'Failing Replays Only')
    TabOrder = 3
  end
  object lbClassifications: TListBox
    Left = 8
    Top = 168
    Width = 185
    Height = 97
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 4
  end
  object rgNaming: TRadioGroup
    Left = 8
    Top = 288
    Width = 185
    Height = 65
    Caption = 'Filenaming'
    ItemIndex = 1
    Items.Strings = (
      '[Level Title]'
      '[RRLL] [Level Title]'
      '[Rank] [LL] [Level Title]')
    TabOrder = 5
  end
end
