object FTextsForm: TFTextsForm
  Left = 215
  Top = 324
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'FTextsForm'
  ClientHeight = 417
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 62
    Height = 13
    Caption = 'Preview Text'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 160
    Width = 67
    Height = 13
    Caption = 'Postview Text'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 72
    Top = 315
    Width = 31
    Height = 13
    Caption = 'Music:'
  end
  object Label5: TLabel
    Left = 56
    Top = 355
    Width = 45
    Height = 13
    Caption = 'Filename:'
  end
  object Button1: TButton
    Left = 76
    Top = 384
    Width = 73
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 156
    Top = 384
    Width = 73
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object mmPreview: TMemo
    Left = 8
    Top = 24
    Width = 289
    Height = 113
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object mmPostview: TMemo
    Left = 8
    Top = 176
    Width = 289
    Height = 113
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object ebMusicNum: TEdit
    Left = 104
    Top = 312
    Width = 129
    Height = 21
    TabOrder = 4
  end
  object ebFilename: TEdit
    Left = 104
    Top = 352
    Width = 129
    Height = 21
    TabOrder = 5
  end
end
