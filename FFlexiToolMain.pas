unit FFlexiToolMain;

interface

uses
  //ExeBuild,
  Talis_Main, TalisData,
  FReplayManager, FlexiRanks, FlexiNamedStreams,
  FImportStrip, FGsDownload, FEditDataFiles, PngInterface,
  FTSysDat, FEditTexts, FTextMassEdit, FEditMusicList,
  OpenSaveDlgs, DatManage,
  GameModPlay, UZip,
  Gr32,
  ShellApi,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ExtCtrls, StdCtrls, Contnrs, StrUtils,
  GR32_Image;

const
  SVersion = 'V10.13.15-B:f7940aa';
  // Player versions are minimum compatible, not latest at time!
  PVersion = 'V10.13.XX';
  PVerNum_Format = 10;
  PVerNum_Core = 13;

  MUSIC_EXT_COUNT = 12;
  MUSIC_EXTENSIONS: array[0..MUSIC_EXT_COUNT-1] of string = (
                    '.ogg',
                    '.wav',
                    '.aiff',
                    '.aif',
                    '.mp3',
                    '.mo3',
                    '.it',
                    '.mod',
                    '.xm',
                    '.s3m',
                    '.mtm',
                    '.umx');

  CUSTOM_IMAGE_COUNT = 17;
  ImageFileNames: array[0..CUSTOM_IMAGE_COUNT-1] of String = (
                    'logo.png',
                    'background.png',
                    'sign_play.png',
                    'sign_code.png',
                    'sign_rank.png',
                    'sign_config.png',
                    'sign_talisman.png',
                    'sign_quit.png',
                    'skill_panels.png',
                    'skill_panels_mask.png',
                    'empty_slot.png',
                    'empty_slot_mask.png',
                    'skill_count_digits.png',
                    'skill_count_digits_mask.png',
                    'skill_count_erase.png',
                    'minimap_region.png',
                    'minimap_region_mask.png'
                  );
  ImageDescriptions: array[0..CUSTOM_IMAGE_COUNT-1] of String = (
                       'Title Logo',
                       'Menu Background',
                       'Play Game Sign',
                       'Enter Code Sign',
                       'Rank Select Sign',
                       'Config Menu Sign',
                       'Talisman Menu Sign',
                       'Quit Sign',
                       'Skill Panels',
                       'Skill Panels Mask',
                       'Empty Skill Panel Slot',
                       'Empty Skill Panel Slot Mask',
                       'Skill Count Digits',
                       'Skill Count Digits Mask',
                       'Skill Count Erase',
                       'Minimap Area',
                       'Minimap Area Mask'
                     );

  CUSTOM_LEMMINGS_COUNT = 24;
  LemmingActionDescriptions: array[0..CUSTOM_LEMMINGS_COUNT-1] of String = (
                             'Basher',
                             'Blocker',
                             'Bomber',
                             'Builder',
                             'Burner',
                             'Climber',
                             'Digger',
                             'Disarmer',
                             'Drowner',
                             'Exiter',
                             'Faller',
                             'Floater',
                             'Glider',
                             'Hoister',
                             'Jumper',
                             'Miner',
                             'OhNoer',
                             'Platformer',
                             'Shrugger',
                             'Splatter',
                             'Stacker',
                             'Stoner',
                             'Swimmer',
                             'Walker'
                             );
  // No need for a filename list. They can be taken from the action names, just lowercase them.

type

  TGetReplayNameFunction = function(aLevel: TDatSection): String of object;

  TFFTKMain = class(TForm)
    pcMain: TPageControl;
    pcLevels: TTabSheet;
    pcGraphics: TTabSheet;
    pcOther: TTabSheet;
    pcMusic: TTabSheet;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    pcSettings: TTabSheet;
    Label1: TLabel;
    ebTitle: TEdit;
    Splitter1: TSplitter;
    Label4: TLabel;
    cbOptEnableDump: TCheckBox;
    Splitter2: TSplitter;
    Label5: TLabel;
    ebGameText: TEdit;
    lbRanks: TListBox;
    Label6: TLabel;
    btnAddRank: TButton;
    btnDelRank: TButton;
    btnRankUp: TButton;
    btnRankDown: TButton;
    Label7: TLabel;
    ebRankName: TEdit;
    Splitter4: TSplitter;
    Label9: TLabel;
    lbLevels: TListBox;
    btnAddLevel: TButton;
    btnDelLevel: TButton;
    btnLevelMoveUp: TButton;
    btnLevelMoveDown: TButton;
    btnLevelExtract: TButton;
    Label10: TLabel;
    lbGraphics: TListBox;
    btnGsAdd: TButton;
    btnGsDelete: TButton;
    Label12: TLabel;
    lbMusics: TListBox;
    btnMusicPlay: TButton;
    btnMusicStop: TButton;
    btnMusicExtract: TButton;
    btnMusicDelete: TButton;
    btnMusicAdd: TButton;
    BuildEXE1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    Splitter7: TSplitter;
    btnLevelReplace: TButton;
    mmIssues: TMemo;
    Label15: TLabel;
    btnIssueCheck: TButton;
    btnIssuesSave: TButton;
    btnMassEditTexts: TButton;
    btnMusicList: TButton;
    lbCustomImageList: TListBox;
    Label13: TLabel;
    btnLoadCustomImage: TButton;
    btnSaveCustomImage: TButton;
    btnClearCustomImage: TButton;
    btnResetRotation: TButton;
    btnPrevRank: TButton;
    btnNextRank: TButton;
    cbRankSign: TCheckBox;
    ools1: TMenuItem;
    ExportReplays1: TMenuItem;
    LevelTitle1: TMenuItem;
    RRLLLevelTitle1: TMenuItem;
    RankLLLevelTitle1: TMenuItem;
    ReplayManager1: TMenuItem;
    btnReplayCheck: TButton;
    btnGsDownload: TButton;
    ools2: TMenuItem;
    SetLVLFilenamesFromLevelNames1: TMenuItem;
    Label2: TLabel;
    cbLemmingSprites: TComboBox;
    cbChooseText: TListBox;
    SetReplayFilenamesFromClassifications1: TMenuItem;
    Splitter6: TSplitter;
    Label3: TLabel;
    lblDataCount: TLabel;
    btnManageDataFiles: TButton;
    btnTalismans: TButton;
    estReplays1: TMenuItem;
    cbMacRotation: TCheckBox;
    procedure ebTitleChange(Sender: TObject);
    procedure ebGameTextChange(Sender: TObject);
    procedure cbChooseTextChange(Sender: TObject);
    procedure cbOptEnableDumpClick(Sender: TObject);
    procedure btnAddRankClick(Sender: TObject);
    procedure btnDelRankClick(Sender: TObject);
    procedure btnRankUpClick(Sender: TObject);
    procedure btnRankDownClick(Sender: TObject);
    procedure ebRankNameChange(Sender: TObject);
    procedure lbRanksClick(Sender: TObject);
    procedure btnAddLevelClick(Sender: TObject);
    procedure btnDelLevelClick(Sender: TObject);
    procedure btnLevelMoveUpClick(Sender: TObject);
    procedure btnLevelMoveDownClick(Sender: TObject);
    procedure btnLevelExtractClick(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure btnGsAddClick(Sender: TObject);
    procedure btnGsDeleteClick(Sender: TObject);
    procedure btnMusicPlayClick(Sender: TObject);
    procedure btnMusicStopClick(Sender: TObject);
    procedure btnMusicAdd1Click(Sender: TObject);
    procedure btnMusicDel1Click(Sender: TObject);
    procedure btnMusicExtractClick(Sender: TObject);
    procedure cbMainDatClick(Sender: TObject);
    procedure cbTalismansClick(Sender: TObject);
    procedure lbLevelsDblClick(Sender: TObject);
    procedure BuildEXE1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure btnLevelReplaceClick(Sender: TObject);
    procedure btnIssueCheckClick(Sender: TObject);
    procedure btnIssuesSaveClick(Sender: TObject);
    procedure btnMusicListClick(Sender: TObject);
    procedure btnLoadCustomImageClick(Sender: TObject);
    procedure btnSaveCustomImageClick(Sender: TObject);
    procedure btnClearCustomImageClick(Sender: TObject);
    procedure btnMassEditTextsClick(Sender: TObject);
    procedure btnResetRotationClick(Sender: TObject);
    procedure btnPrevRankClick(Sender: TObject);
    procedure btnNextRankClick(Sender: TObject);
    procedure cbRankSignClick(Sender: TObject);
    procedure LevelTitle1Click(Sender: TObject);
    procedure RRLLLevelTitle1Click(Sender: TObject);
    procedure RankLLLevelTitle1Click(Sender: TObject);
    procedure ReplayManager1Click(Sender: TObject);
    procedure btnReplayCheckClick(Sender: TObject);
    procedure btnGsDownloadClick(Sender: TObject);
    procedure SetLVLFilenamesFromLevelNames1Click(Sender: TObject);
    procedure cbLemmingSpritesChange(Sender: TObject);
    procedure SetReplayFilenamesFromClassifications1Click(Sender: TObject);
    procedure btnManageDataFilesClick(Sender: TObject);
    procedure btnTalismansClick(Sender: TObject);
    procedure estReplays1Click(Sender: TObject);
    procedure cbMacRotationClick(Sender: TObject);
  private
    fMusicHandle: HMUSIC;
    fFileName: String;
    fSystemDat: TSystemDat;
    fRanks: TGameRanks;
    fReplayLabels: TStringArray;
    fGraphicSets: TNamedStreams;
    fMusics: TNamedStreams;
    fMusicList: TStringList;
    fTalismans: TTalismans;
    fCustomImages: TNamedStreams;
    fDataFiles: TNamedStreams;
    fUpdating: Boolean;
    procedure ResetMusicList;
    procedure UpgradePack(aPath: String);
    procedure DoSaveReplays(aPath: String; aNameFunction: TGetReplayNameFunction = nil);
    function GetSaveReplayPath: String;
    function GetReplayNameTitleOnly(aLevel: TDatSection): String;
    function GetReplayNameRRLLTitle(aLevel: TDatSection): String;
    function GetReplayNameRankLLTitle(aLevel: TDatSection): String;
    procedure SetDefaultLabels;
    procedure DoBuildNXP(aSilent: Boolean = false);
    function HasLevelIDConflict(aID: Cardinal = 0): String;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateInterface;
    procedure DoLoad(aPath: String);
    procedure DoSave(aPath: String);
  end;

var
  FFTKMain: TFFTKMain;

implementation

{$R *.dfm}
{$R bass.res}
{$R skill_count_digits.res}

{ TFFTKMain }

constructor TFFTKMain.Create(aOwner: TComponent);
var
  i, i2, n: Integer;
begin
  inherited;
  fSystemDat := TSystemDat.Create;
  fRanks := TGameRanks.Create(fSystemDat);
  fGraphicSets := TNamedStreams.Create;
  fMusics := TNamedStreams.Create;
  fMusicList := TStringList.Create;
  fTalismans := TTalismans.Create;
  fCustomImages := TNamedStreams.Create;
  fDataFiles := TNamedStreams.Create;
  for i := 0 to CUSTOM_IMAGE_COUNT-1 do
  begin
    fCustomImages.Add(TNamedMemoryStream.Create);
    fCustomImages[i].Name := ImageFileNames[i];
  end;
  for i := 0 to 14 do
  begin
    fCustomImages.Add(TNamedMemoryStream.Create);
    fCustomImages[i+CUSTOM_IMAGE_COUNT].Name := 'rank_' + LeadZeroStr(i + 1, 2) + '.png';
  end;

  fFileName := '';
  fUpdating := false;
  BASS_Init(-1, 44100, 0, 0, nil);

  pcMain.TabIndex := 0;

  New1Click(self);
end;

destructor TFFTKMain.Destroy;
var
  i: Integer;
begin
  fSystemDat.Free;
  fRanks.Free;
  fGraphicSets.Free;
  fMusics.Free;
  fMusicList.Free;
  fTalismans.Free;
  fCustomImages.Free;
  fDataFiles.Free;
  BASS_Free;
  inherited;
end;

procedure TFFTKMain.ResetMusicList;
var
  i: Integer;
begin
  fMusicList.Clear;
  for i := 1 to 17 do
    fMusicList.Add('orig_' + LeadZeroStr(i, 2));
  for i := 1 to 6 do
    fMusicList.Add('ohno_' + LeadZeroStr(i, 2));
end;

procedure TFFTKMain.UpgradePack(aPath: String);
var
  i, i2: Integer;
  SL: TStringList;
  S: String;
  DeCmp: TSections;

  function CheckFileMatch(aName: String): String;
  var
    FS1, FS2: TFileStream;
    k: Integer;
    b1, b2: Byte;
  begin
    Result := '';
    FS1 := TFileStream.Create(aPath + 'levels\' + aName, fmOpenRead);
    try
      for k := 0 to SL.Count-1 do
      begin
        FS2 := TFileStream.Create(aPath + 'levels\' + SL.ValueFromIndex[k], fmOpenRead);
        try
          if FS1.Size <> FS2.Size then Continue;
          while (FS1.Position < FS1.Size) do // don't need to check both, since we've already confirmed they're the same size
          begin
            FS1.Read(b1, 1);
            FS2.Read(b2, 1);
            if b1 <> b2 then Break;
            if FS1.Position = FS1.Size then
            begin
              Result := SL.ValueFromIndex[k];
              Exit;
            end;
          end;
        finally
          FS2.Free;
        end;
      end;
    finally
      FS1.Free;
    end;
  end;
begin
  ForceDirectories(aPath + 'levels\');

  // Extract DAT files first, if they exist
  for i := 1 to 15 do
  begin
    if not FileExists(aPath + 'LEVEL' + LeadZeroStr(i-1, 3) + '.DAT') then Continue;
    DeCmp := TSections.Create(true);
    DeCmp.LoadFromFile(aPath + 'LEVEL' + LeadZeroStr(i-1, 3) + '.DAT');
    for i2 := 0 to DeCmp.Count-1 do
    begin
      DeCmp[i2].SaveToFile(aPath + 'levels\' + LeadZeroStr(i, 2) + LeadZeroStr(i2+1, 2) + '.lvl');
    end;
    DeCmp.Free;
    DeleteFile(aPath + 'LEVEL' + LeadZeroStr(i-1, 3) + '.DAT');
  end;

  // Then manage the level files
  SL := TStringList.Create;
  for i := 1 to 15 do
    for i2 := 1 to 255 do
    begin
      if not FileExists(aPath + 'levels\' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl') then Break;
      S := CheckFileMatch(LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl');
      if S = '' then
        SL.Add(LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '=' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl')
      else begin
        SL.Add(LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '=' + S);
        DeleteFile(aPath + 'levels\' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl');
      end;
    end;
  SL.SaveToFile(aPath + 'levels\' + 'levels.ini');
end;

procedure TFFTKMain.DoSave(aPath: String);
var
  i, i2: Integer;
  TempFS: TFileStream;
  b: Byte;
  s: String;
  SL, RSL: TStringList;

  procedure DeleteMatched(aExt: String; aExcept: String = '');
  var
    SearchRec: TSearchRec;
    SearchRec2: TSearchRec;
    Matched: Boolean;
  begin
    if FindFirst(aPath + aExt, faAnyFile, SearchRec) = 0 then
    begin
      repeat
        Matched := false;
        if aExcept <> '' then
        begin
          if (FindFirst(aPath + aExcept, faAnyFile, SearchRec2) = 0) then
          repeat
            if LowerCase(SearchRec.Name) = LowerCase(SearchRec2.Name) then Matched := true;
          until (FindNext(SearchRec2) <> 0) or Matched;
          FindClose(SearchRec2);
        end;
        if not Matched then DeleteFile(aPath + ExtractFilePath(aExt) + SearchRec.Name);
      until FindNext(SearchRec) <> 0;
      FindClose(SearchRec);
    end;
  end;

begin
  aPath := ExtractFilePath(aPath);
  SetCurrentDir(aPath);

  DeleteMatched('*.dat', '*_music.dat');
  DeleteMatched('i????.txt');
  DeleteMatched('i?????.txt');
  DeleteMatched('p????.txt');
  DeleteMatched('p?????.txt');
  DeleteMatched('codes.txt');
  DeleteMatched('music.txt');
  for i := 0 to MUSIC_EXT_COUNT-1 do
    DeleteMatched('*' + MUSIC_EXTENSIONS[i]);
  DeleteMatched('*.png');

  DeleteMatched('levels\*.lvl');
  DeleteMatched('levels\levels.ini');
  DeleteMatched('replays\*.lrb');
  DeleteMatched('replays\*.nxrp');
  DeleteMatched('replays\replays.ini');
  DeleteMatched('files\*');

  SetCurrentDir(aPath);

  fRanks.SetToSysDat;

  // SysDat patching. Prevents setting flags that cannot be changed in new versions; some are auto turned on.
  fSystemDat.Options := fSystemDat.Options * [sdoAllowDump, sdoMacRotation];
  fSystemDat.Options := fSystemDat.Options + [sdoDisableBomberMaskFix, sdoUsePngFiles, sdoNoDirectDrop,
                                              sdoReplayCheck, sdoCenterSkills, sdoUnlockAll, sdoLemmingSpritesName];

  fSystemDat.SaveToFile(aPath + 'SYSTEM.DAT');
  //fRanks.SaveDatFiles(aPath);
  fRanks.SaveLvlFiles(aPath);

  for i := 0 to fGraphicSets.Count-1 do
    fGraphicSets[i].SaveToFile(aPath + fGraphicSets[i].Name + '.dat');

  for i := 0 to fMusics.Count-1 do
    fMusics[i].SaveToFile(aPath + fMusics[i].Name);

  SetCurrentDir(aPath);

  fMusicList.SaveToFile('music.txt');

  fTalismans.FixLevelLinks(fRanks);
  if fTalismans.Count <> 0 then fTalismans.SaveToFile('talisman.dat');

  for i := 0 to fCustomImages.Count-1 do
    if fCustomImages[i].Size <> 0 then fCustomImages[i].SaveToFile(fCustomImages[i].Name);

  ForceDirectories(aPath + 'files\');
  SetCurrentDir(aPath + 'files\');
  for i := 0 to fDataFiles.Count-1 do
    fDataFiles[i].SaveToFile(fDataFiles[i].Name);

  RSL := TStringList.Create;

  for i := 0 to Length(fReplayLabels)-1 do
    RSL.Add(fReplayLabels[i]);
  RSL.Add('');

  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
    begin
      fRanks[i].Levels[i2].Replays.Save(aPath, fRanks[i].Levels[i2].FileName, RSL);
      SetCurrentDir(aPath);

      if fRanks[i].Levels[i2].Text1 <> '' then
      begin
        s := fRanks[i].Levels[i2].Text1;
        TempFS := TFileStream.Create('i' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt', fmCreate);
        TempFS.Seek(0, soFromBeginning);
        while TempFS.Position < Length(s) do
        begin
          b := Ord(s[TempFS.Position+1]);
          TempFS.Write(b, 1);
        end;
        TempFS.Free;
      end;
      if fRanks[i].Levels[i2].Text2 <> '' then
      begin
        s := fRanks[i].Levels[i2].Text2;
        TempFS := TFileStream.Create('p' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt', fmCreate);
        TempFS.Seek(0, soFromBeginning);
        while TempFS.Position < Length(s) do
        begin
          b := Ord(s[TempFS.Position+1]);
          TempFS.Write(b, 1);
        end;
        TempFS.Free;
      end;
    end;

  RSL.SaveToFile(aPath + 'replays\replays.ini');

  RSL.Free;

  if FileExists('codes.txt') then
    DeleteFile('codes.txt');
  SL.Free;

  DoLoad(aPath);
end;

procedure TFFTKMain.DoLoad(aPath: String);
var
  SearchRec: TSearchRec;
  s: String;
  TempStream: TNamedMemoryStream;
  i, i2: Integer;
  b: Byte;
  w: Word;
  SL: TStringList;
  lr, ln: Integer;
  ImgIndex: Integer;

  procedure LoadExtraReplays(aExt: String);
  var
    R: TReplayStream;
    L: TDatSection;

    function DoesReplayExist(aFilename: String): Boolean;
    var
      R, L, i: Integer;
    begin
      Result := false;
      aFilename := ChangeFileExt(aFilename, '');
      for R := 0 to fRanks.Count-1 do
        for L := 0 to fRanks[R].Levels.Count-1 do
          for i := 0 to fRanks[R].Levels[L].Replays.Count-1 do
            if Lowercase(aFilename) = Lowercase(fRanks[R].Levels[L].FileName + '_' + fRanks[R].Levels[L].Replays[i].Name) then
            begin
              Result := true;
              Exit;
            end;
    end;
  begin
    SetCurrentDir(aPath + 'replays\');
    if FindFirst('*.' + aExt, 0, SearchRec) = 0 then
    begin
      repeat
        if DoesReplayExist(SearchRec.Name) then Continue;
        R := TReplayStream.Create;
        R.LoadFromFile(SearchRec.Name);
        R.Name := SearchRec.Name;

        L := fRanks.Levels[R.LevelID];
        if L = nil then
        begin
          R.Free;
          Continue;
        end;

        L.Replays.Add(R);
      until FindNext(SearchRec) <> 0;
      FindClose(SearchRec);
    end;
  end;

  procedure LoadToFiles(aFilename: String; aLoadAs: String = '');
  begin
    if not FileExists(aFilename) then Exit;
    if aLoadAs = '' then aLoadAs := aFilename;
    TempStream := TNamedMemoryStream.Create;
    TempStream.Name := aLoadAs;
    TempStream.LoadFromFile(aFilename);
    fDataFiles.Add(TempStream);
  end;

  procedure SetImgIndex(aName: String);
  var
    i: Integer;
  begin
    for i := 0 to CUSTOM_IMAGE_COUNT-1 do
      if fCustomImages[i].Name = aName then
      begin
        ImgIndex := i;
        Exit;
      end;
    raise Exception.Create('SetImgIndex couldn''t find "' + aName + '"');
  end;

  procedure HandleOldSkillPanel;
  var
    TempBmp, TempBmp2: TBitmap32;
    Notify: Boolean;

    procedure Split(aIsMask: Boolean);
    var
      Mask: String;

      function Validate: Boolean;
      var
        x, y: Integer;
      begin
        Result := false;
        for y := 0 to TempBmp2.Height-1 do
          for x := 0 to TempBmp2.Width-1 do
            if TempBmp2.Pixel[X, Y] and $FF000000 <> 0 then
            begin
              Result := true;
              Exit;
            end;
      end;
    begin
      if aIsMask then
        Mask := '_mask'
      else
        Mask := '';

      TempBmp2.SetSize(16, 23);
      TempBmp.DrawTo(TempBmp2, TempBmp2.BoundsRect, Rect(33, 16, 49, 39));
      SetImgIndex('empty_slot' + Mask + '.png');
      if Validate then
      begin
        fCustomImages[ImgIndex].Clear;
        TPngInterface.SavePngStream(fCustomImages[ImgIndex], TempBmp2);
      end;

      TempBmp.DrawTo(TempBmp2, TempBmp2.BoundsRect, Rect(1, 16, 17, 39));
      if Validate then
      begin
        TempStream := TNamedMemoryStream.Create;
        TempStream.Name := 'icon_rr_minus' + Mask + '.png';
        TPngInterface.SavePngStream(TempStream, TempBmp2);
        fDataFiles.Add(TempStream);
      end;

      TempBmp.DrawTo(TempBmp2, TempBmp2.BoundsRect, Rect(17, 16, 33, 39));
      if Validate then
      begin
        TempStream := TNamedMemoryStream.Create;
        TempStream.Name := 'icon_rr_plus' + Mask + '.png';
        TPngInterface.SavePngStream(TempStream, TempBmp2);
        fDataFiles.Add(TempStream);
      end;

      TempBmp.DrawTo(TempBmp2, TempBmp2.BoundsRect, Rect(161, 16, 177, 39));
      if Validate then
      begin
        TempStream := TNamedMemoryStream.Create;
        TempStream.Name := 'icon_pause' + Mask + '.png';
        TPngInterface.SavePngStream(TempStream, TempBmp2);
        fDataFiles.Add(TempStream);
      end;

      TempBmp.DrawTo(TempBmp2, TempBmp2.BoundsRect, Rect(177, 16, 193, 39));
      if Validate then
      begin
        TempStream := TNamedMemoryStream.Create;
        TempStream.Name := 'icon_nuke' + Mask + '.png';
        TPngInterface.SavePngStream(TempStream, TempBmp2);
        fDataFiles.Add(TempStream);
      end;

      TempBmp2.SetSize(111, 24);
      if aIsMask then
        TempBmp2.Clear(0)
      else
        TempBmp2.Clear($FF000000);
      TempBmp.DrawTo(TempBmp2, 0, 0, Rect(205, 16, 315, 40));
      SetImgIndex('minimap_region' + Mask + '.png');
      if Validate then
      begin
        fCustomImages[ImgIndex].Clear;
        TPngInterface.SavePngStream(fCustomImages[ImgIndex], TempBmp2);
      end;
    end;
  begin
    TempBmp := TBitmap32.Create;
    TempBmp2 := TBitmap32.Create;
    try
      SetImgIndex('skill_panels.png');

      Notify := false;
      if FileExists('skill_icons.png') then
      begin
        fCustomImages[ImgIndex].LoadFromFile('skill_icons.png');
        Notify := true;
      end;
      if FileExists('skill_icons_mask.png') then
      begin
        fCustomImages[ImgIndex+1].LoadFromFile('skill_icons_mask.png');
        Notify := true;
      end;

      if Notify then
        ShowMessage('This pack contains custom blank skill panels. These will likely need manual fixing.' + #13 +
                    'In newer versions of NeoLemmix, these should no longer have a skill count digit area hard-drawn onto them.');

      Notify := false;
      if FileExists('skill_panel.png') then
      begin
        TPngInterface.LoadPngFile('skill_panel.png', TempBmp);
        Split(false);
        LoadToFiles('skill_panel.png');
        Notify := true;
      end;

      if FileExists('skill_panel_mask.png') then
      begin
        TPngInterface.LoadPngFile('skill_panel_mask.png', TempBmp);
        Split(true);
        LoadToFiles('skill_panel_mask.png');
        Notify := true;
      end;

      if Notify then
        ShowMessage('This pack contains a custom skill panel graphic. The relevant sub-graphics have been generated from this.' + #13 +
                    'The unmodified skill panel graphic has been added to custom data files to avoid data loss. Please delete it' + #13 +
                    'from here once you no longer need it, as new versions of NeoLemmix do not make use of this file. Please note' + #13 +
                    'that some images, such as custom graphics for release rate and nuke buttons, may need tidying up.');
    finally
      TempBmp.Free;
      TempBmp2.Free;
    end;
  end;

  procedure HandleOldDigits(aMask: Boolean);
  var
    S: TResourceStream;
    TempBmp, TempBmp2: TBitmap32;
    Mask: String;
  begin
    if aMask then
    begin
      Mask := '_mask';
      S := nil;
    end else begin
      Mask := '';
      S := TResourceStream.Create(HInstance, 'skilldigits', 'archive');
    end;
    TempBmp := TBitmap32.Create;
    TempBmp2 := TBitmap32.Create;
    try
      if not aMask then
        TPngInterface.LoadPngStream(S, TempBmp)
      else begin
        TempBmp.SetSize(56, 8);
        TempBmp.Clear(0);
      end;
      if FileExists('skill_count_digits' + Mask + '.png') then
      begin
        TPngInterface.LoadPngFile('skill_count_digits' + Mask + '.png', TempBmp2);
        if TempBmp2.Width = 56 then Exit;
        TempBmp2.DrawTo(TempBmp, 0, 0);
      end;
      if FileExists('skill_count_infinite' + Mask + '.png') then
      begin
        TPngInterface.LoadPngFile('skill_count_infinite' + Mask + '.png', TempBmp2);
        TempBmp2.DrawTo(TempBmp, 40, 0);
      end;
      if FileExists('skill_count_lock' + Mask + '.png') then
      begin
        TPngInterface.LoadPngFile('skill_count_lock' + Mask + '.png', TempBmp2);
        TempBmp2.DrawTo(TempBmp, 48, 0);
      end;

      SetImgIndex('skill_count_digits' + Mask + '.png');
      TPngInterface.SavePngStream(fCustomImages[ImgIndex], TempBmp);
    finally
      S.Free;
      TempBmp.Free;
      TempBmp2.Free;
    end;
  end;
begin
  aPath := ExtractFilePath(aPath);

  SetCurrentDir(aPath);

  if not FileExists(aPath + 'levels\levels.ini') then
  begin
    if MessageDlg('This pack needs to be upgraded to the file structure used in this version' + #13 +
                  'of the Flexi Toolkit. Once the upgrade is complete, it will not be possible' + #13 +
                  'to open the pack in older versions of Flexi Toolkit. This does not have any' + #13 +
                  'impact on which versions of NeoLemmix can play this pack. Continue?', mtCustom, [mbYes, mbNo], 0) = mrNo then
      Exit;

    UpgradePack(aPath);
  end;

  fSystemDat.LoadFromFile(aPath + 'SYSTEM.DAT');

//  fSystemDat.Options - [sdoAlwaysNormalLemmings, sdoAlwaysXmasLemmings];
  if sdoAlwaysNormalLemmings in fSystemDat.Options then
    fSystemDat.GraphicSet[0] := 'default'
  else if sdoAlwaysXmasLemmings in fSystemDat.Options then
    fSystemDat.GraphicSet[0] := 'xmas'
  else if not (sdoLemmingSpritesName in fSystemDat.Options) then
    fSystemDat.GraphicSet[0] := '';
  fSystemDat.Options := fSystemDat.Options - [sdoAlwaysNormalLemmings, sdoAlwaysXmasLemmings];

  fRanks.GetFromSysDat;
  mmIssues.Clear;

  fGraphicSets.Clear;
  if FindFirst(aPath + '*.dat', faAnyFile, SearchRec) = 0 then
  begin
    repeat
      s := LowerCase(SearchRec.Name);
      if s = 'main.dat' then Continue;
      if s = 'talisman.dat' then Continue;
      if s = 'system.dat' then Continue;
      if (LeftStr(s, 5) = 'level')
        and (StrToIntDef(MidStr(s, 6, 3), -1) <> -1)
        and (MidStr(s, 9, 4) = '.dat') then Continue;

      TempStream := TNamedMemoryStream.Create;
      TempStream.LoadFromFile(SearchRec.Name);

      TempStream.Seek(0, soFromBeginning);
      TempStream.Read(w, 2);
      if w = $4C45 then
      begin
        TempStream.Free;
        Continue;
      end;

      s := ChangeFileExt(s, '');
      TempStream.Name := s;
      fGraphicSets.Add(TempStream);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;

  fGraphicSets.SortByNames;

  SetCurrentDir(aPath + 'files\');

  fDataFiles.Clear;
  if FindFirst(aPath + 'files\*', 0, SearchRec) = 0 then
  begin
    repeat
      TempStream := TNamedMemoryStream.Create;
      TempStream.Name := SearchRec.Name;
      TempStream.LoadFromFile(SearchRec.Name);
      fDataFiles.Add(TempStream);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;

  SetCurrentDir(aPath);

  fMusics.Clear;

  for i := 0 to MUSIC_EXT_COUNT-1 do
    if FindFirst(aPath + '*' + MUSIC_EXTENSIONS[i], faAnyFile, SearchRec) = 0 then
    begin
      repeat
        TempStream := TNamedMemoryStream.Create;
        TempStream.LoadFromFile(aPath + SearchRec.Name);
        TempStream.Name := ExtractFileName(SearchRec.Name);
        fMusics.Add(TempStream);
      until FindNext(SearchRec) <> 0;
      FindClose(SearchRec);
    end;


  if DirectoryExists(aPath + 'MusicPack/') then
    ShowMessage('A "MusicPack" folder for this pack exists. Music packs are now deprecated and Flexi Toolkit' + #13 +
                'no longer supports them. Instead, you should distribute the music files themself, to be placed' + #13 +
                'in NeoLemmix''s "music" folder. Please manually remove this folder; Flexi will not automatically' + #13 +
                'delete it in order to avoid data loss.');

  SetCurrentDir(aPath);

  if FileExists('music.txt') then
    fMusicList.LoadFromFile('music.txt')
  else
    fMusicList.Clear;

  fTalismans.Clear;
  if FileExists('talisman.dat') then fTalismans.LoadFromFile('talisman.dat');
  fTalismans.SetLevelLinks(fRanks);

  for i := 0 to CUSTOM_IMAGE_COUNT-1 do
    if FileExists(ImageFileNames[i]) then
      fCustomImages[i].LoadFromFile(ImageFileNames[i])
    else
      fCustomImages[i].Clear;

  if FileExists('skill_panel.png') or FileExists('skill_icons.png') or FileExists('skill_panel_mask.png') or FileExists('skill_icons_mask.png') then
    HandleOldSkillPanel;

  if FileExists('skill_count_digits.png') or FileExists('skill_count_infinite.png') or FileExists('skill_count_lock.png') then
    HandleOldDigits(false);

  if FileExists('skill_count_digits_mask.png') or FileExists('skill_count_infinite_mask.png') or FileExists('skill_count_lock_mask.png') then
    HandleOldDigits(true);

  for i := 0 to 14 do
    if FileExists('rank_' + LeadZeroStr(i + 1, 2) + '.png') then
      fCustomImages[i + CUSTOM_IMAGE_COUNT].LoadFromFile('rank_' + LeadZeroStr(i + 1, 2) + '.png')
    else
      fCustomImages[i + CUSTOM_IMAGE_COUNT].Clear;

  // Backwards compatibility loading for images that no longer have direct support
  LoadToFiles('scroller_lemmings.png');
  LoadToFiles('scroller_segment.png');
  LoadToFiles('menu_font.png');
  LoadToFiles('talismans.png');
  LoadToFiles('panel_font.png');
  LoadToFiles('panel_font_mask.png');

  // Backwards compatibility loading for lemming sprites
  for i := 0 to CUSTOM_LEMMINGS_COUNT-1 do
  begin
    LoadToFiles('lemming_' + LemmingActionDescriptions[i] + '.png', 'default_' + LemmingActionDescriptions[i] + '.png');
    LoadToFiles('lemming_' + LemmingActionDescriptions[i] + '_mask.png', 'default_' + LemmingActionDescriptions[i] + '_mask.png');
    LoadToFiles('xlemming_' + LemmingActionDescriptions[i] + '.png', 'xmas_' + LemmingActionDescriptions[i] + '.png');
    LoadToFiles('xlemming_' + LemmingActionDescriptions[i] + '_mask.png', 'xmas_' + LemmingActionDescriptions[i] + '_mask.png');
  end;

  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
    begin
      if FileExists('i' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt') then
      begin
        s := '';
        TempStream := TNamedMemoryStream.Create;
        TempStream.LoadFromFile('i' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt');
        TempStream.Seek(0, soFromBeginning);
        while TempStream.Position < TempStream.Size do
        begin
          TempStream.Read(b, 1);
          //if b <> 13 then
            s := s + chr(b);
        end;
        fRanks[i].Levels[i2].Text1 := s;
        TempStream.Free;
      end else
        fRanks[i].Levels[i2].Text1 := '';
      if FileExists('p' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt') then
      begin
        s := '';
        TempStream := TNamedMemoryStream.Create;
        TempStream.LoadFromFile('p' + LeadZeroStr(i+1, 2) + LeadZeroStr(i2+1, 2) + '.txt');
        TempStream.Seek(0, soFromBeginning);
        while TempStream.Position < TempStream.Size do
        begin
          TempStream.Read(b, 1);
          //if b <> 13 then
            s := s + chr(b);
        end;
        fRanks[i].Levels[i2].Text2 := s;
        TempStream.Free;
      end else
        fRanks[i].Levels[i2].Text2 := '';

      if fRanks[i].Levels[i2].MusicName = '*' then fRanks[i].Levels[i2].MusicName := '';
    end;

  if FileExists('replays\replays.ini') then
  begin
    SL := TStringList.Create;
    SL.LoadFromFile('replays\replays.ini');

    for i := 0 to SL.Count-1 do
      if SL[i] = '' then Break;
    SetLength(fReplayLabels, i);

    for i := 0 to SL.Count-1 do
    begin
      if SL[i] = '' then Break;
      fReplayLabels[i] := SL[i];
    end;

    LoadExtraReplays('nxrp');
    LoadExtraReplays('lrb');

    SL.Free;
  end else
    SetDefaultLabels;

  UpdateInterface;
end;

procedure TFFTKMain.DoSaveReplays(aPath: String; aNameFunction: TGetReplayNameFunction = nil);
var
  i, i2, i3: Integer;
  S: String;

  procedure FixInvalidFilename;
  var
    c, ci: Integer;
  const
    INVALID_CHARS = '\/:*?"<>|.'; // period is allowed technically, but looks messy
  begin
    for c := 1 to Length(S) do
      for ci := 1 to Length(INVALID_CHARS) do
        if S[c] = INVALID_CHARS[ci] then S[c] := '_';
  end;

begin
  aPath := ExtractFilePath(aPath);

  if not Assigned(aNameFunction) then
    aNameFunction := GetReplayNameTitleOnly;

  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
      for i3 := 0 to fRanks[i].Levels[i2].Replays.Count-1 do
      begin
        S := aNameFunction(fRanks[i].Levels[i2]);
        S := S + '_' + fRanks[i].Levels[i2].Replays[i3].Name;
        FixInvalidFilename;
        if fRanks[i].Levels[i2].Replays[i3].IsNXRP then
          fRanks[i].Levels[i2].Replays[i3].SaveToFile(aPath + S + '.nxrp')
        else
          fRanks[i].Levels[i2].Replays[i3].SaveToFile(aPath + S + '.lrb');
      end;
end;

function TFFTKMain.GetReplayNameTitleOnly(aLevel: TDatSection): String;
begin
  Result := Trim(aLevel.LevelTitle);
end;

function TFFTKMain.GetReplayNameRRLLTitle(aLevel: TDatSection): String;
var
  R, L: Integer;

  procedure FindLevel;
  var
    LocR, LocL: Integer;
  begin
    for LocR := 0 to fRanks.Count-1 do
      for LocL := 0 to fRanks[LocR].Levels.Count-1 do
        if aLevel = fRanks[LocR].Levels[LocL] then
        begin
          R := LocR;
          L := LocL;
          Exit;
        end;
    raise Exception.Create('FindLevel called for non-existant level?');
  end;
begin
  FindLevel;
  Result := LeadZeroStr(R+1, 2) + LeadZeroStr(L+1, 2) + ' ' + Trim(aLevel.LevelTitle);
end;

function TFFTKMain.GetReplayNameRankLLTitle(aLevel: TDatSection): String;
var
  R, L: Integer;

  procedure FindLevel;
  var
    LocR, LocL: Integer;
  begin
    for LocR := 0 to fRanks.Count-1 do
      for LocL := 0 to fRanks[LocR].Levels.Count-1 do
        if aLevel = fRanks[LocR].Levels[LocL] then
        begin
          R := LocR;
          L := LocL;
          Exit;
        end;
    raise Exception.Create('FindLevel called for non-existant level?');
  end;
begin
  FindLevel;
  Result := Trim(fRanks[R].RankName) + ' ' + LeadZeroStr(L+1, 2) + ' ' + Trim(aLevel.LevelTitle);
end;

procedure TFFTKMain.UpdateInterface;
var
  savedpos: Integer;
  i, i2, n: Integer;
  s: String;
begin
  if fUpdating then Exit;
  fUpdating := true;

  // Settings Panel //

  if Trim(ebTitle.Text) <> Trim(fSystemDat.Title) then
    ebTitle.Text := fSystemDat.Title;
  //rgDefPercent.ItemIndex := fSystemDat.PercentOption-1;
  cbOptEnableDump.Checked := sdoAllowDump in fSystemDat.Options;
  cbMacRotation.Checked := sdoMacRotation in fSystemDat.Options;

  if Trim(fSystemDat.GraphicSet[0]) = '' then
    cbLemmingSprites.Text := cbLemmingSprites.Items[0]
  else
    cbLemmingSprites.Text := Trim(fSystemDat.GraphicSet[0]);

  if cbChooseText.ItemIndex = -1 then cbChooseText.ItemIndex := 0;
  i := cbChooseText.ItemIndex;
    if i = 0 then
    ebGameText.MaxLength := 32
  else
    ebGameText.MaxLength := 36;
  case i of
    0: s := fSystemDat.SecondLine;
    1..16: s := fSystemDat.ScrollerText[i-1];
    17..34: s := fSystemDat.StandardResult[(i-17) div 2, (i-1) mod 2];
  end;
  if Trim(s) <> Trim(ebGameText.Text) then
    ebGameText.Text := s;


  // Levels Panel //
  savedpos := lbRanks.ItemIndex;
  lbRanks.Clear;
  for i := 0 to fRanks.Count-1 do
    lbRanks.AddItem(fRanks[i].RankName, nil);
  if (savedpos < 0) or (savedpos > lbRanks.Count-1) then savedpos := 0;
  lbRanks.ItemIndex := savedpos;

  cbRankSign.OnClick := nil;
  cbRankSign.Checked := fCustomImages[lbRanks.ItemIndex + CUSTOM_IMAGE_COUNT].Size <> 0;
  cbRankSign.OnClick := cbRankSignClick;

  lbLevels.Items.BeginUpdate;

  if (lbRanks.Count > 0) then
  begin
    if Trim(ebRankName.Text) <> Trim(fRanks[lbRanks.ItemIndex].RankName) then
      ebRankName.Text := fRanks[lbRanks.ItemIndex].RankName;
    for i := 0 to fRanks[lbRanks.ItemIndex].Levels.Count-1 do
      with fRanks[lbRanks.ItemIndex].Levels do
      begin
        s := LeadZeroStr(i+1, 2) + ': ';
        case Items[i].SectionType of
          dst_Level2KB, dst_Level10KB, dst_LevelVar: s := s + Items[i].LevelTitle;
          else s := s + '<<invalid level>>';
        end;
        if i < lbLevels.Items.Count then
          lbLevels.Items[i] := s
        else
          lbLevels.AddItem(s, nil);
      end;
    for i := lbLevels.Items.Count-1 downto fRanks[lbRanks.ItemIndex].Levels.Count do
      lbLevels.Items.Delete(i);
  end;
  lbLevels.Items.EndUpdate;


  // Graphic Sets Panel //
  fGraphicSets.SortByNames;

  lbGraphics.Items.BeginUpdate;

  i := 0;
  while i < fGraphicSets.Count-2 do
  begin
    if fGraphicSets[i].Name = fGraphicSets[i+1].Name then
      fGraphicSets.Delete(i)
    else
      i := i + 1;
  end;

  for i := 0 to fGraphicSets.Count-1 do
    if i < lbGraphics.Items.Count then
      lbGraphics.Items[i] := fGraphicSets[i].Name
    else
      lbGraphics.AddItem(fGraphicSets[i].Name, nil);

  for i := lbGraphics.Items.Count-1 downto fGraphicSets.Count do
    lbGraphics.Items.Delete(i);

  lbGraphics.Items.EndUpdate;


  // Musics Panel //
  lbMusics.Items.BeginUpdate;
  //lbMusics.Clear;

  for i := fMusics.Count-1 downto 0 do
    if fMusics[i].Size = 0 then
      fMusics.Delete(i);

  for i := 0 to fMusics.Count-1 do
  begin
    s := ChangeFileExt(fMusics[i].Name, '');
    if i < lbMusics.Items.Count then
      lbMusics.Items[i] := s
    else
      lbMusics.AddItem(s, nil);
  end;

  for i := lbMusics.Items.Count-1 downto fMusics.Count do
    lbMusics.Items.Delete(i);

  lbMusics.Items.EndUpdate;

  // Other Data Panel //

  for i := 0 to CUSTOM_IMAGE_COUNT-1 do
  begin
    s := ImageDescriptions[i];
    if fCustomImages[i].Size = 0 then s := s + ' (placeholder)';
    if i >= lbCustomImageList.Count then
      lbCustomImageList.Items.Add(s)
    else
      lbCustomImageList.Items[i] := s;
  end;

  for i := 0 to fRanks.Count-1 do
  begin
    s := fRanks[i].RankName;
    if fCustomImages[i + CUSTOM_IMAGE_COUNT].Size = 0 then
      s := s + ' (placeholder)';
    if i + CUSTOM_IMAGE_COUNT >= lbCustomImageList.Count then
      lbCustomImageList.Items.Add(s)
    else
      lbCustomImageList.Items[i + CUSTOM_IMAGE_COUNT] := s;
  end;

  while lbCustomImageList.Items.Count > (CUSTOM_IMAGE_COUNT + fRanks.Count) do
    lbCustomImageList.Items.Delete(lbCustomImageList.Items.Count-1);

  lblDataCount.Caption := IntToStr(fDataFiles.Count) + ' Data Files';

  fUpdating := false;
end;

procedure TFFTKMain.ebTitleChange(Sender: TObject);
begin
  fSystemDat.Title := ebTitle.Text;
  UpdateInterface;
end;

procedure TFFTKMain.ebGameTextChange(Sender: TObject);
var
  s: String;
  i: Integer;
begin
  s := ebGameText.Text;
  i := cbChooseText.ItemIndex;
  case i of
    0: fSystemDat.SecondLine := s;
    1..16: fSystemDat.ScrollerText[i-1] := s;
    17..34: fSystemDat.StandardResult[(i-17) div 2, (i-1) mod 2] := s;
  end;
  UpdateInterface;
end;

procedure TFFTKMain.cbChooseTextChange(Sender: TObject);
begin
  UpdateInterface;
end;

procedure TFFTKMain.cbOptEnableDumpClick(Sender: TObject);
begin
  if cbOptEnableDump.Checked then
    fSystemDat.Options := fSystemDat.Options + [sdoAllowDump]
  else
    fSystemDat.Options := fSystemDat.Options - [sdoAllowDump];
end;

procedure TFFTKMain.btnAddRankClick(Sender: TObject);
begin
  if fRanks.Count = 15 then
  begin
    ShowMessage('A single pack cannot contain more than 15 ranks.');
    Exit;
  end else begin
    fRanks.Add(TGameRank.Create);
    fRanks.Items[fRanks.Count-1].RankName := 'Rank ' + IntToStr(fRanks.Count);
    lbRanks.AddItem('', nil);
  end;
  lbRanks.ItemIndex := lbRanks.Items.Count-1;
  UpdateInterface;
end;

procedure TFFTKMain.btnDelRankClick(Sender: TObject);
begin
  if lbRanks.ItemIndex = -1 then Exit;
  fRanks.Delete(lbRanks.ItemIndex);
  UpdateInterface;
end;

procedure TFFTKMain.btnRankUpClick(Sender: TObject);
begin
  if lbRanks.ItemIndex < 1 then Exit;
  fRanks.Move(lbRanks.ItemIndex, lbRanks.ItemIndex-1);
  lbRanks.ItemIndex := lbRanks.ItemIndex - 1;
  UpdateInterface;
end;

procedure TFFTKMain.btnRankDownClick(Sender: TObject);
begin
  if (lbRanks.ItemIndex = 1) or (lbRanks.ItemIndex = fRanks.Count-1) then Exit;
  fRanks.Move(lbRanks.ItemIndex, lbRanks.ItemIndex+1);
  lbRanks.ItemIndex := lbRanks.ItemIndex + 1;
  UpdateInterface;
end;

procedure TFFTKMain.ebRankNameChange(Sender: TObject);
begin
  if lbRanks.ItemIndex = -1 then Exit;
  fRanks[lbRanks.ItemIndex].RankName := ebRankName.Text;
  UpdateInterface;
end;

procedure TFFTKMain.lbRanksClick(Sender: TObject);
begin
  UpdateInterface;
//  UpdateInterface; // not an error. doing it twice prevents a glitch; I have no idea why.
end;

procedure TFFTKMain.btnAddLevelClick(Sender: TObject);
var
  s: String;
  i: Integer;
  TempSec: TDatSection;
  OpenFile: TOpenMulti;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  OpenFile := TOpenMulti.Create('Level Files|*.lvl');
  if OpenFile.Count = 0 then
  begin
    OpenFile.Free;
    Exit;
  end;
  s := '';
  for i := 0 to OpenFile.Count-1 do
  begin
    TempSec := TDatSection.Create;
    TempSec.LoadFromFile(OpenFile[i]);
    TempSec.FileName := ExtractFileName(OpenFile[i]);
    if TempSec.SectionType = dst_Level2KB then
      s := s + ExtractFileName(OpenFile[i]) + ' appears to be a traditional Lemmix level or in an outdated format.' + #13
    else if not TempSec.SectionType in [dst_Level10KB, dst_LevelVar] then
    begin
      s := s + ExtractFileName(OpenFile[i]) + ' is not a valid level file and was skipped.' + #13;
      TempSec.Free;
      Continue;
    end;
    fRanks[lbRanks.ItemIndex].Levels.Add(TempSec);
  end;
  if s <> '' then
    ShowMessage('The following issues occurred:' + #13 + s);
  OpenFile.Free;
  UpdateInterface;
end;

procedure TFFTKMain.btnDelLevelClick(Sender: TObject);
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if lbLevels.ItemIndex = -1 then Exit;
  fRanks[lbRanks.ItemIndex].Levels.Delete(lbLevels.ItemIndex);
  UpdateInterface;
end;

procedure TFFTKMain.btnLevelMoveUpClick(Sender: TObject);
var
  i: Integer;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if lbLevels.ItemIndex < 1 then Exit;
  i := lbLevels.ItemIndex;
  fRanks[lbRanks.ItemIndex].Levels.Move(i, i-1);
  UpdateInterface;
  lbLevels.ItemIndex := i-1;
end;

procedure TFFTKMain.btnLevelMoveDownClick(Sender: TObject);
var
  i: Integer;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if (lbLevels.ItemIndex = -1) or (lbLevels.ItemIndex = lbLevels.Items.Count-1) then Exit;
  i := lbLevels.ItemIndex;
  fRanks[lbRanks.ItemIndex].Levels.Move(i, i+1);
  UpdateInterface;
  lbLevels.ItemIndex := i+1;
end;

procedure TFFTKMain.btnLevelExtractClick(Sender: TObject);
var
  s: String;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if lbLevels.ItemIndex = -1 then Exit;
  s := SaveDialog('Level Files|*.lvl', fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex].LevelTitle + '.lvl');
  if s = '' then Exit;
  fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex].SaveToFile(s);
end;

procedure TFFTKMain.New1Click(Sender: TObject);
var
  i: Integer;
begin
  fSystemDat.Clear;
  fSystemDat.DefaultOptions; 
  fRanks.Clear;
  fGraphicSets.Clear;
  fMusics.Clear;
  fTalismans.Clear;
  for i := 0 to fCustomImages.Count-1 do
    fCustomImages[i].Clear;
  fDataFiles.Clear;
  //fIcon.Clear;
  ResetMusicList;
  mmIssues.Clear;
  SetDefaultLabels;
  UpdateInterface;
end;

procedure TFFTKMain.Open1Click(Sender: TObject);
var
  s: String;
begin
  s := OpenDialog('SYSTEM.DAT File|SYSTEM.DAT');
  if s = '' then Exit;
  DoLoad(s);
  fFileName := s;
end;

procedure TFFTKMain.Save1Click(Sender: TObject);
var
  s: String;
begin
  if fFileName = '' then
    s := SaveDialog('SYSTEM.DAT File|SYSTEM.DAT', 'SYSTEM.DAT')
  else
    s := fFileName;
  if s = '' then Exit;
  DoSave(s);
  fFileName := s;
end;

procedure TFFTKMain.SaveAs1Click(Sender: TObject);
var
  s: String;
begin
  s := SaveDialog('SYSTEM.DAT File|SYSTEM.DAT', 'SYSTEM.DAT');
  if s = '' then Exit;
  DoSave(s);
  fFileName := s;
end;

procedure TFFTKMain.Exit1Click(Sender: TObject);
begin
  Halt(0);
end;

procedure TFFTKMain.btnGsAddClick(Sender: TObject);
var
  s: String;
  i: Integer;
  TempNS: TNamedMemoryStream;
  OpenFile: TOpenMulti;
begin
  //s := OpenDialog();
  //if s = '' then Exit;
  OpenFile := TOpenMulti.Create('NeoLemmix Graphic Set|*.dat');
  if OpenFile.Count = 0 then
  begin
    OpenFile.Free;
    Exit;
  end;
  for i := 0 to OpenFile.Count-1 do
  begin
    s := OpenFile[i];
    TempNS := TNamedMemoryStream.Create;
    TempNS.LoadFromFile(s);
    s := LowerCase(ExtractFileName(s));
    TempNS.Name := ChangeFileExt(s, '');
    fGraphicSets.Add(TempNS);
  end;
  OpenFile.Free;
  UpdateInterface;
end;

procedure TFFTKMain.btnGsDeleteClick(Sender: TObject);
begin
  if (lbGraphics.ItemIndex <> -1) then
    fGraphicSets.Delete(lbGraphics.ItemIndex);

  UpdateInterface;
end;

procedure TFFTKMain.btnMusicPlayClick(Sender: TObject);
var
  PlayStream: TNamedMemoryStream;
  TargetSet: TNamedStreams;
  TargetPos: Integer;
const
  PlayFlags = BASS_MUSIC_LOOP or BASS_MUSIC_RAMPS or BASS_MUSIC_SURROUND or
               BASS_MUSIC_POSRESET or BASS_SAMPLE_SOFTWARE;
begin
  if (lbMusics.ItemIndex = -1) then Exit;

    TargetSet := fMusics;
    TargetPos := lbMusics.ItemIndex;

  PlayStream := TargetSet[TargetPos];

  BASS_StreamFree(fMusicHandle);
  BASS_MusicFree(fMusicHandle);

  fMusicHandle := BASS_StreamCreateFile(true, PlayStream.Memory, 0, PlayStream.Size, PlayFlags);
  if fMusicHandle = 0 then
    fMusicHandle := BASS_MusicLoad(true, PlayStream.Memory, 0, PlayStream.Size, PlayFlags, 1)
  else
    BASS_LoadLoopData(fMusicHandle);
  if fMusicHandle <> 0 then
    BASS_ChannelPlay(fMusicHandle, false);
end;

procedure TFFTKMain.btnMusicStopClick(Sender: TObject);
begin
  BASS_StreamFree(fMusicHandle);
  BASS_MusicFree(fMusicHandle);
end;

procedure TFFTKMain.btnMusicAdd1Click(Sender: TObject);
var
  s: String;
  i: Integer;
  TempStream: TNamedMemoryStream;
  OpenFile: TOpenMulti;
begin
  s := '';
  for i := 0 to MUSIC_EXT_COUNT-1 do
  begin
    s := s + '*' + MUSIC_EXTENSIONS[i];
    if i <> MUSIC_EXT_COUNT-1 then s := s + ';';
  end;
  OpenFile := TOpenMulti.Create('Supported Music Files|' + s);
  if OpenFile.Count = 0 then
  begin
    OpenFile.Free;
    Exit;
  end;
  for i := 0 to OpenFile.Count-1 do
  begin
    TempStream := TNamedMemoryStream.Create;
    TempStream.Name := ExtractFileName(OpenFile[i]);
    TempStream.LoadFromFile(OpenFile[i]);
    fMusics.Add(TempStream);
  end;
  OpenFile.Free;
  UpdateInterface;
end;

procedure TFFTKMain.btnMusicDel1Click(Sender: TObject);
var
  TargetSet: TNamedStreams;
  TargetPos: Integer;
begin
  if (lbMusics.ItemIndex = -1) then Exit;

  TargetSet := fMusics;
  TargetPos := lbMusics.ItemIndex;

  TargetSet.Delete(TargetPos);
  UpdateInterface;
end;

procedure TFFTKMain.btnMusicExtractClick(Sender: TObject);
var
  PlayStream: TNamedMemoryStream;
  s: String;
  TargetSet: TNamedStreams;
  TargetPos: Integer;
begin
  if (lbMusics.ItemIndex = -1) then Exit;

  TargetSet := fMusics;
  TargetPos := lbMusics.ItemIndex;

  PlayStream := TargetSet[TargetPos];

  s := SaveDialog(ExtractFileExt(PlayStream.Name) + ' files|*' + ExtractFileExt(PlayStream.Name));
  if s = '' then Exit;
  PlayStream.SaveToFile(s);
end;

procedure TFFTKMain.cbMainDatClick(Sender: TObject);
begin
  UpdateInterface;
end;

procedure TFFTKMain.cbTalismansClick(Sender: TObject);
begin
  UpdateInterface;
end;

procedure TFFTKMain.lbLevelsDblClick(Sender: TObject);
var
  TextForm: TFTextsForm;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if lbLevels.ItemIndex = -1 then Exit;
  TextForm := TFTextsForm.Create(self, fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex]);
  TextForm.ShowModal;
  TextForm.Free;
end;

procedure TFFTKMain.BuildEXE1Click(Sender: TObject);
begin
  DoBuildNXP;
end;

procedure TFFTKMain.DoBuildNXP(aSilent: Boolean = false);
var
  ResStream: TResourceStream;
  TempArc: TArchive;
  TempStream: TMemoryStream;
  s, ms: String;
  srcPath: String;
  i, i2: Integer;
  RotationZero: Boolean;
  SL: TStringList;
  ActualDst: String;
  Level: TDatSection;

  procedure GetCompatibleVersion;
  var
    MainVer: Integer;
    SubVer: Integer;
    MinorVer: Integer;
    i, i2: Integer;
    b: Byte;
    w: Word;
    FoundRotate: Boolean;
  begin
    MainVer := PVerNum_Format;
    SubVer := PVerNum_Core;
    //MinorVer := PVerNum_Minor;

    SL.Add('format=' + IntToStr(MainVer));
    SL.Add('core=' + IntToStr(SubVer));
    //SL.Add(IntToStr(MinorVer));

  end;

begin

  //if Win32MajorVersion < 5 then
  //  if MessageDlg('The Build EXE feature does not work on Windows 95, 98 or ME, and it appears' + #13 +
  //                'you are using one of these versions. Do you want to attempt to build the EXE anyway?', mtCustom, [mbYes, mbNo], 0) = mrNo then Exit;

  if fFileName = '' then
  begin
    ShowMessage('Please save this project first.');
    Exit;
  end;

  S := HasLevelIDConflict;
  if S <> '' then
  begin
    ShowMessage('This pack contains multiple levels with the same Level ID. Duplicates occur in:' + #13 + S);
    Exit;
  end;

  if aSilent then
  begin
    s := ExtractFilePath(fFileName) + 'temp.nxp';
    ActualDst := s;
  end else begin
    s := SaveDialog('NeoLemmix Level Pack|*.nxp', '.nxp');
    if s = '' then Exit;
    ActualDst := s;
    s := ExtractFilePath(fFileName) + ExtractFileName(s);
    ShowMessage('Your NXP will now be built. This may take a while.');
  end;

  try
    DoSave(fFileName);
    //fRanks.SaveDatFiles(ExtractFilePath(fFileName));
  except
    ShowMessage('There was an error saving the data.');
    Exit;
  end;

  srcPath := ExtractFilePath(fFileName);

  TempArc := TArchive.Create;

  if FileExists(s) then DeleteFile(s);
  TempArc.OpenArchive(s, amCreate);

  for i := 0 to fGraphicSets.Count-1 do
    TempArc.AddFiles(srcPath + fGraphicSets[i].Name + '.dat');
  TempArc.AddFiles(srcPath + 'x_*.dat');
  TempArc.AddFiles(srcPath + 'system.dat');
  //TempArc.AddFiles(srcPath + 'main.dat');
  TempArc.AddFiles(srcPath + 'talisman.dat');
  TempArc.AddFiles(srcPath + 'i*.txt');
  TempArc.AddFiles(srcPath + 'p*.txt');
  TempArc.AddFiles(srcPath + 'codes.txt');
  TempArc.AddFiles(srcPath + 'music.txt');
  for i := 0 to MUSIC_EXT_COUNT-1 do
    TempArc.AddFiles(srcPath + '*' + MUSIC_EXTENSIONS[i]);
  for i := 0 to fCustomImages.Count-1 do
    TempArc.AddFiles(srcPath + fCustomImages[i].Name);
  TempArc.AddFiles(srcPath + 'files\*');

  SL := TStringList.Create;
  for i := 0 to fRanks.Count-1 do
    SL.Add('LEVELCOUNT ' + IntToStr(fRanks[i].Levels.Count));
  SL.Add('');
  ForceDirectories(srcPath + 'temp/');
  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
    begin
      Level := fRanks[i].Levels[i2];
      SL.Add('LEVEL ' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2));
      SL.Add('  TITLE ' + Level.LevelTitle);
      SL.Add('  ID ' + IntToHex(Level.LevelID, 8));
      SL.Add('');

      Level.SaveToFile(srcPath + 'temp/' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl');
      TempArc.AddFiles(srcPath + 'temp/' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl');
      DeleteFile(srcPath + 'temp/' + LeadZeroStr(i, 2) + LeadZeroStr(i2, 2) + '.lvl');
    end;
  RemoveDir(srcPath + 'temp');

  SL.SaveToFile(srcPath + 'levels.nxmi');
  TempArc.AddFiles(srcPath + 'levels.nxmi');
  DeleteFile(srcPath + 'levels.nxmi');

  SL.Clear;

  GetCompatibleVersion;
  SL.SaveToFile(srcPath + 'version.txt');
  TempArc.AddFiles(srcPath + 'version.txt');
  DeleteFile(srcPath + 'version.txt');
  SL.Free;

  TempArc.CloseArchive;
  TempArc.Free;

  if s <> ActualDst then
  begin
    if FileExists(ActualDst) then
      DeleteFile(ActualDst);
    MoveFile(PChar(s), PChar(ActualDst));
  end;

  if not aSilent then
    ShowMessage('NXP created. Enjoy!');

end;

procedure TFFTKMain.About1Click(Sender: TObject);
begin
  ShowMessage('NeoLemmix Flexi Toolkit ' + SVersion + ' by namida' + #13 +
              {$ifdef exp}'EXPERIMENTAL VERSION!' + #13 +{$endif}
              'Target NeoLemmix Player: ' + PVersion + #13 +
              'Uses some code fragments by EricLang and Mindless' + #13 +
              'Uses BASS library by Un4seen');
end;

procedure TFFTKMain.btnLevelReplaceClick(Sender: TObject);
var
  s: String;
  i: Integer;
  TempSec: TDatSection;
begin
  if lbRanks.ItemIndex = -1 then Exit;
  if lbLevels.ItemIndex = -1 then Exit;

  s := OpenDialog('Level Files|*.lvl');
  if s = '' then Exit;

  TempSec := TDatSection.Create;
  TempSec.LoadFromFile(s);
  TempSec.FileName := ExtractFileName(s);
  s := '';
  if TempSec.SectionType = dst_Level2KB then
    s := s + ExtractFileName(s) + ' appears to be a traditional Lemmix level or in an outdated format.' + #13
  else if not TempSec.SectionType in [dst_Level10KB, dst_LevelVar] then
  begin
    s := s + ExtractFileName(s) + ' is not a valid level file.' + #13;
    TempSec.Free;
    Exit;
  end;

  TempSec.Seek(0, soFromBeginning);
  fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex].LoadFromStream(TempSec);
  fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex].FileName := TempSec.FileName;
  fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex].Replays.Clear;
  if s <> '' then
    ShowMessage('The following issues occurred:' + #13 + s);
  TempSec.Free;
  UpdateInterface;
end;

procedure TFFTKMain.btnIssueCheckClick(Sender: TObject);
var
  i, i2, i3, i4: Integer;
  tv: Integer;
  s: String;
  ts: Array[0..15] of Char;
  TempSecs: TSections;
  TempMusicName: String;
  b: Byte;
  w: Word;
  MainSet: String;

  function CheckStyleExists(aName: String): Boolean;
  var
    i: Integer;
  const
    DEFAULT_GS_NAME_MAX = 47;
    DEFAULT_GS_NAMES: array[0..DEFAULT_GS_NAME_MAX] of string =
                    ('dirt', 'fire', 'marble', 'pillar', 'crystal',                  //orig styles
                     'brick', 'rock', 'snow', 'bubble',                              //ohno styles
                     'tree', 'purple', 'psychedelic', 'metal', 'desert',             //lpii styles
                     'sky', 'circuit', 'martian', 'lab',                             //lpiii styles
                     'lpiv_candy', 'lpiv_space', 'lpiv_clockwork', 'lpiv_wasteland', //lpiv styles
                     'lpv_abstract', 'lpv_honeycomb', 'lpv_mineshaft', 'lpv_machine', //lpv styles
                     'xmas', 'sega', 'horror', 'special', 'nx_sketches',             //misc styles
                     'beach', 'cavelem', 'circus', 'egyptian', 'highland', 'medieval', 'medival', // l2 styles
                     'outdoor', 'polar', 'shadow', 'space', 'sports',
                     'l3_biolab', 'gigalem_l3egypt', 'l3_egypt', 'gigalem_l3shadow', 'l3_shadow'); // l3 styles
  begin
    Result := false;

    for i := 0 to DEFAULT_GS_NAME_MAX do
      if LowerCase(Trim(aName)) = DEFAULT_GS_NAMES[i] then
      begin
        Result := true;
        Exit;
      end;
    for i := 0 to fGraphicSets.Count-1 do
      if LowerCase(Trim(aName)) = LowerCase(Trim(fGraphicSets[i].Name)) then Result := true;

  end;

  function CheckMusicExists(aName: String): Boolean;
  var
    i: Integer;
  const
    DEFAULT_MUSIC_NAME_MAX = 25;
    DEFAULT_MUSIC_NAMES: array[0..DEFAULT_MUSIC_NAME_MAX] of string =
                       ('orig_01', 'orig_02', 'orig_03', 'orig_04', 'orig_05',
                        'orig_06', 'orig_07', 'orig_08', 'orig_09', 'orig_10',
                        'orig_11', 'orig_12', 'orig_13', 'orig_14', 'orig_15',
                        'orig_16', 'orig_17', 'ohno_01', 'ohno_02', 'ohno_03',
                        'ohno_04', 'ohno_05', 'ohno_06', 'gimmick', 'frenzy',
                        'orig_00');
  begin
    Result := false;
    for i := 0 to DEFAULT_MUSIC_NAME_MAX do
      if LowerCase(Trim(aName)) = DEFAULT_MUSIC_NAMES[i] then
      begin
        Result := true;
        Exit;
      end;
    for i := 0 to fMusics.Count-1 do
      if LowerCase(ChangeFileExt(fMusics[i].Name, '')) = aName then
      begin
        Result := true;
        Exit;
      end;
  end;

  function FindStylesList(aLevelStream: TDatSection): Boolean;
  var
    b: Byte;
    w: Word;
  begin
    Result := false;
    if aLevelStream.SectionType <> dst_LevelVar then Exit;
    aLevelStream.Position := $B0;
    repeat
      aLevelStream.Read(b, 1);

      case b of
        1, 3: aLevelStream.Position := aLevelStream.Position + $14;
        2: aLevelStream.Position := aLevelStream.Position + $10;
        4: repeat
             aLevelStream.Read(w, 4);
           until (w = $FFFF) or (aLevelStream.Position >= aLevelStream.Size);
        5: aLevelStream.Position := aLevelStream.Position + $2A;
        6: Result := true;
      end;

      if aLevelStream.Position >= aLevelStream.Size then Exit;
    until b = 6;
  end;

begin
  mmIssues.Clear;

  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
    begin
      s := fRanks[i].RankName + ' ' + IntToStr(i2+1) + ': ';

      if not (fRanks[i].Levels[i2].SectionType in [dst_Level10KB, dst_LevelVar]) then
      begin
        if fRanks[i].Levels[i2].SectionType = dst_Level2KB then
          mmIssues.Lines.Add(s + 'Please load and re-save this level in the editor')
        else
          mmIssues.Lines.Add(s + 'Unrecognized level format');
        Continue;
      end;

      if fRanks[i].Levels[i2].SectionType = dst_Level10KB then
      begin
        mmIssues.Lines.Add(s + 'Warning: Older level format. May have problems.');
        fRanks[i].Levels[i2].Seek(96, soFromBeginning);
      end else
        fRanks[i].Levels[i2].Seek(112, soFromBeginning);

      fRanks[i].Levels[i2].Read(ts, 16);
      if Trim(ts) = '' then
      begin
        mmIssues.Lines.Add(s + 'Please load and re-save this level in the editor');
        Continue;
      end else
        if not CheckStyleExists(ts) then mmIssues.Lines.Add(s + 'Graphic set "' + trim(ts) + '" not found.');
      MainSet := Trim(ts);

      if FindStylesList(fRanks[i].Levels[i2]) then
      begin
        fRanks[i].Levels[i2].Read(w, 2);
        for i3 := 1 to w do
        begin
          fRanks[i].Levels[i2].Read(ts, 16);
          if Trim(ts) = MainSet then Continue;
          if not CheckStyleExists(ts) then mmIssues.Lines.Add(s + 'Graphic set "' + trim(ts) + '" not found.');
        end;
      end;

      TempMusicName := fRanks[i].Levels[i2].MusicName;
      if (TempMusicName <> '') and (LeftStr(TempMusicName, 1) <> '?') then
        if not CheckMusicExists(TempMusicName) then
          mmIssues.Lines.Add(s + 'Music file "' + TempMusicName + '" not found.');

      if fRanks[i].Levels[i2].LevelID = 0 then
        mmIssues.Lines.Add(s + 'No level ID set, please use the Reset Level ID option in the editor')
      else begin
        for i3 := 0 to fRanks.Count-1 do
          for i4 := 0 to fRanks[i3].Levels.Count-1 do
            if (i3 = i) and (i4 = i2) then
              Continue
            else if fRanks[i].Levels[i2].LevelID = fRanks[i3].Levels[i4].LevelID then
              mmIssues.Lines.Add(s + 'Duplicate level ID with ' + fRanks[i3].RankName + ' ' + IntToStr(i4+1));
      end;

      fRanks[i].Levels[i2].Seek(9, soFromBeginning);
      fRanks[i].Levels[i2].Read(b, 1);
      if b and $10 <> 0 then
        mmIssues.Lines.Add(s + 'Level uses oddtabling which is no longer supported.');
    end;

  for i := 0 to fMusics.Count-1 do
  begin
    if (Lowercase(ChangeFileExt(fMusics[i].Name, '')) = 'success') or (Lowercase(ChangeFileExt(fMusics[i].Name, '')) = 'failure') then
      mmIssues.Lines.Add('Success and Failure jingles should be included as data files, not musics.');
  end;

  if mmIssues.Lines.Count = 0 then mmIssues.Lines.Add('No issues identified.')
end;

procedure TFFTKMain.btnReplayCheckClick(Sender: TObject);
var
  cat: Integer;
  Tot, Suc, Cur, SucCur, None: Integer;

  procedure GetReplayClassificationCount(aCat: Integer);
  var
    Replay: TReplayStream;
    r, l, i: Integer;
    IsSuc, IsCur: Boolean;
  begin
    Tot := 0;
    Suc := 0;
    Cur := 0;
    SucCur := 0;
    for r := 0 to fRanks.Count-1 do
      for l := 0 to fRanks[r].Levels.Count-1 do
        for i := 0 to fRanks[r].Levels[l].Replays.Count-1 do
          if fRanks[r].Levels[l].Replays[i].Classification = aCat then
          begin
            Replay := fRanks[r].Levels[l].Replays[i];
            IsSuc := Replay.Success;
            IsCur := Replay.CRC = fRanks[r].Levels[l].CRC;
            if IsSuc then Inc(Suc);
            if IsCur then Inc(Cur);
            if IsSuc and IsCur then Inc(SucCur);
            Inc(Tot);
          end;
  end;
begin
  mmIssues.Clear;

  for cat := 0 to Length(fReplayLabels)-1 do
  begin
    GetReplayClassificationCount(cat);
    if Tot = 0 then Continue;
    mmIssues.Lines.Add('---- ' + fReplayLabels[cat] + ' ----');
    mmIssues.Lines.Add('  TOTAL: ' + IntToStr(Tot));
    if SucCur > 0 then mmIssues.Lines.Add('     ' + IntToStr(SucCur) + ' current and successful');
    if SucCur - Cur > 0 then mmIssues.Lines.Add('     ' + IntToStr(Cur) + ' current but unsuccessful');
    if SucCur - Suc > 0 then mmIssues.Lines.Add('     ' + IntToStr(Suc) + ' successful but outdated');
    None := Tot - (Cur + Suc - SucCur);
    if None > 0 then mmIssues.Lines.Add('     ' + IntToStr(None) + ' unsuccessful and outdated');
    mmIssues.Lines.Add('');
  end;
end;

procedure TFFTKMain.btnIssuesSaveClick(Sender: TObject);
var
  s: String;
  i: Integer;
  Proceed: Boolean;
begin
  Proceed := false;
  for i := 0 to mmIssues.Lines.Count-1 do
    if mmIssues.Lines[i] <> '' then
    begin
      Proceed := true;
      Break;
    end;
  if not Proceed then Exit;
  s := SaveDialog('Text File|*.txt', '.txt');
  mmIssues.Lines.SaveToFile(s);
end;

procedure TFFTKMain.btnMusicListClick(Sender: TObject);
var
  MusicForm: TFMusicList;
begin
  MusicForm := TFMusicList.Create(self);
  MusicForm.SetMusicList(fMusicList);
  MusicForm.ShowModal;
  MusicForm.Free; 
end;

procedure TFFTKMain.btnLoadCustomImageClick(Sender: TObject);
var
  ImportForm: TF_ImportStrip;
begin
  if lbCustomImageList.ItemIndex = -1 then Exit;
  {s := OpenDialog('PNG Image File|*.png');
  if s = '' then Exit;
  fCustomImages[lbCustomImageList.ItemIndex].LoadFromFile(s);}
  ImportForm := TF_ImportStrip.Create(self);
  try
    if ImportForm.ImportStrip then
      if IsPositiveResult(ImportForm.ShowModal) then
      begin
        fCustomImages[lbCustomImageList.ItemIndex].Clear;
        TPngInterface.SavePngStream(fCustomImages[lbCustomImageList.ItemIndex], ImportForm.Bitmap);
      end;
  finally
    ImportForm.Free;
    UpdateInterface;
  end;
end;

procedure TFFTKMain.btnSaveCustomImageClick(Sender: TObject);
var
  s: String;
begin
  if lbCustomImageList.ItemIndex = -1 then Exit;
  if fCustomImages[lbCustomImageList.ItemIndex].Size = 0 then Exit;
  s := SaveDialog('PNG Image File|*.png', ImageFileNames[lbCustomImageList.ItemIndex]);
  if s = '' then Exit;
  fCustomImages[lbCustomImageList.ItemIndex].SaveToFile(s);
end;

procedure TFFTKMain.btnClearCustomImageClick(Sender: TObject);
begin
  if lbCustomImageList.ItemIndex = -1 then Exit;
  fCustomImages[lbCustomImageList.ItemIndex].Clear;
  UpdateInterface;
end;

procedure TFFTKMain.btnMassEditTextsClick(Sender: TObject);
var
  F: TFMassEditText;

  procedure SetTextsToForm;
  var
    i, i2: Integer;
  begin
    F.mmTexts.Clear;
    F.mmTexts.Lines.Add(fSystemDat.SecondLine);
    for i := 0 to 15 do
      F.mmTexts.Lines.Add(fSystemDat.ScrollerText[i]);
    for i := 0 to 8 do
      for i2 := 0 to 1 do
        F.mmTexts.Lines.Add(fSystemDat.StandardResult[i, i2]);
  end;

  procedure SetTextsFromForm;
  var
    i, i2: Integer;
  begin
    while F.mmTexts.Lines.Count < 41 do // 1 Second Line + 16 Scroller Texts + 9x2 Standard Postview + 3x2 Karoshi Postview
      F.mmTexts.Lines.Add('');

    fSystemDat.SecondLine := F.mmTexts.Lines[0];
    for i := 0 to 15 do
      fSystemDat.ScrollerText[i] := F.mmTexts.Lines[1+i];
    for i := 0 to 8 do
      for i2 := 0 to 1 do
        fSystemDat.StandardResult[i, i2] := F.mmTexts.Lines[17+(i*2)+i2];
  end;

begin
  F := TFMassEditText.Create(self);
  SetTextsToForm;
  if F.ShowModal = mrOk then
    SetTextsFromForm;
  F.Free;
end;

procedure TFFTKMain.btnResetRotationClick(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to reset the music rotation to default (Orig + OhNo tracks)?', mtCustom, [mbYes, mbNo], 0) = mrYes then
    ResetMusicList;
end;

procedure TFFTKMain.btnPrevRankClick(Sender: TObject);
var
  SrcDatSection, TempDatSection: TDatSection;
begin
  if lbRanks.ItemIndex = 0 then Exit;
  if lbLevels.ItemIndex = -1 then Exit;
  SrcDatSection := fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex];
  TempDatSection := TDatSection.Create;
  TempDatSection.LoadFromStream(SrcDatSection);
  TempDatSection.Text1 := SrcDatSection.Text1;
  TempDatSection.Text2 := SrcDatSection.Text2;
  TempDatSection.FileName := SrcDatSection.FileName;
  TempDatSection.Replays.Clone(SrcDatSection.Replays);
  fRanks[lbRanks.ItemIndex-1].Levels.Add(TempDatSection);
  fTalismans.Relink(SrcDatSection, TempDatSection);
  fRanks[lbRanks.ItemIndex].Levels.Delete(lbLevels.ItemIndex);
  UpdateInterface;
end;

procedure TFFTKMain.btnNextRankClick(Sender: TObject);
var
  SrcDatSection, TempDatSection: TDatSection;
begin
  if lbRanks.ItemIndex = (fRanks.Count-1) then Exit;
  if lbLevels.ItemIndex = -1 then Exit;
  SrcDatSection := fRanks[lbRanks.ItemIndex].Levels[lbLevels.ItemIndex];
  TempDatSection := TDatSection.Create;
  TempDatSection.LoadFromStream(SrcDatSection);
  TempDatSection.Text1 := SrcDatSection.Text1;
  TempDatSection.Text2 := SrcDatSection.Text2;
  TempDatSection.FileName := SrcDatSection.FileName;
  TempDatSection.Replays.Clone(SrcDatSection.Replays);
  fRanks[lbRanks.ItemIndex+1].Levels.Add(TempDatSection);
  fTalismans.Relink(SrcDatSection, TempDatSection);
  fRanks[lbRanks.ItemIndex].Levels.Delete(lbLevels.ItemIndex);
  UpdateInterface;
end;

procedure TFFTKMain.cbRankSignClick(Sender: TObject);
var
  ImportForm: TF_ImportStrip;
  Success: Boolean;

  procedure SetCheck(aValue: Boolean);
  begin
    // To avoid re-triggering the procedure when the check is changed
    // from within it.

    cbRankSign.OnClick := nil;
    cbRankSign.Checked := aValue;
    cbRankSign.OnClick := cbRankSignClick;
  end;

begin
  if lbRanks.ItemIndex = -1 then
  begin
    SetCheck(false);
    Exit;
  end;

  if cbRankSign.Checked then
  begin
    Success := false;
    ImportForm := TF_ImportStrip.Create(self);
    try
      if ImportForm.ImportStrip then
        if IsPositiveResult(ImportForm.ShowModal) then
        begin
          fCustomImages[lbRanks.ItemIndex + CUSTOM_IMAGE_COUNT].Clear;
          TPngInterface.SavePngStream(fCustomImages[lbRanks.ItemIndex + CUSTOM_IMAGE_COUNT], ImportForm.Bitmap);
          Success := true;
        end;
    finally
      if not Success then SetCheck(false);
      ImportForm.Free;
    end;
  end else
    fCustomImages[lbRanks.ItemIndex + CUSTOM_IMAGE_COUNT].Clear;
  UpdateInterface;    
end;

function TFFTKMain.GetSaveReplayPath: String;
begin
  Result := SaveDialog('(Select any filename in target folder)|*.lrb');
  Result := ExtractFilePath(Result);
end;

procedure TFFTKMain.LevelTitle1Click(Sender: TObject);
var
  S: String;
begin
  S := GetSaveReplayPath;
  if S = '' then Exit;
  DoSaveReplays(S, GetReplayNameTitleOnly);
end;

procedure TFFTKMain.RRLLLevelTitle1Click(Sender: TObject);
var
  S: String;
begin
  S := GetSaveReplayPath;
  if S = '' then Exit;
  DoSaveReplays(S, GetReplayNameRRLLTitle);
end;

procedure TFFTKMain.RankLLLevelTitle1Click(Sender: TObject);
var
  S: String;
begin
  S := GetSaveReplayPath;
  if S = '' then Exit;
  DoSaveReplays(S, GetReplayNameRankLLTitle);
end;

procedure TFFTKMain.ReplayManager1Click(Sender: TObject);
var
  RM: TReplayManagerForm;
begin
  if HasLevelIDConflict <> '' then
  begin
    ShowMessage('Please fix Level ID conflicts before using the replay manager. Use the issue checker' + #13 + 'to see which levels have conflicts.');
    Exit;
  end;

  RM := TReplayManagerForm.Create(self, fRanks);
  RM.Labels := fReplayLabels;

  RM.ShowModal;

  fReplayLabels := RM.Labels;
end;

procedure TFFTKMain.SetDefaultLabels;
begin
  SetLength(fReplayLabels, 6);
  fReplayLabels[0] := 'Unclassified';
  fReplayLabels[1] := 'Solution';
  fReplayLabels[2] := 'Acceptable';
  fReplayLabels[3] := 'Backroute';
  fReplayLabels[4] := 'Attempt';
  fReplayLabels[5] := 'Talisman';
end;



procedure TFFTKMain.btnGsDownloadClick(Sender: TObject);
var
  F: TFDownloadForm;
  i: Integer;
begin
  F := TFDownloadForm.Create(fGraphicSets);
  if F.GetGraphicSets then
    F.ShowModal
  else
    ShowMessage('Flexi Toolkit was unable to connect with the NeoLemmix website.');
  UpdateInterface;
end;

procedure TFFTKMain.SetLVLFilenamesFromLevelNames1Click(Sender: TObject);
var
  i, i2: Integer;

  function FixFilename(aName: String): String;
  var
    c, c2: Integer;
  const
    FORBIDDEN = '/?<>\:*|"^ ';
  begin
    Result := aName;
    for c := 1 to Length(Result) do
      for c2 := 1 to Length(FORBIDDEN) do
        if Result[c] = FORBIDDEN[c2] then
          Result[c] := '_';
  end;
begin
  if MessageDlg('This will overwrite all levels'' filenames. Are you sure?', mtCustom, [mbYes, mbNo], 0) = mrNo then
    Exit;
  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
      fRanks[i].Levels[i2].FileName := FixFilename(Trim(fRanks[i].Levels[i2].LevelTitle)) + '.lvl';
end;

procedure TFFTKMain.cbLemmingSpritesChange(Sender: TObject);
begin
  if cbLemmingSprites.ItemIndex = 0 then
    fSystemDat.GraphicSet[0] := ''
  else
    fSystemDat.GraphicSet[0] := cbLemmingSprites.Text;
end;

procedure TFFTKMain.SetReplayFilenamesFromClassifications1Click(
  Sender: TObject);
var
  i, i2, i3, i4, n: Integer;
  R: TReplayStream;
  S: String;

  function FixFilename(aName: String): String;
  var
    c, c2: Integer;
  const
    FORBIDDEN = '/?<>\:*|"^ ';
  begin
    Result := aName;
    for c := 1 to Length(Result) do
      for c2 := 1 to Length(FORBIDDEN) do
        if Result[c] = FORBIDDEN[c2] then
          Result[c] := '_';
  end;

begin
  if MessageDlg('This will overwrite all replays'' filenames. Are you sure?', mtCustom, [mbYes, mbNo], 0) = mrNo then
    Exit;

  for i := 0 to fRanks.Count-1 do
    for i2 := 0 to fRanks[i].Levels.Count-1 do
      for i3 := 0 to fRanks[i].Levels[i2].Replays.Count-1 do
      begin
        R := fRanks[i].Levels[i2].Replays[i3];
        S := fReplayLabels[R.Classification];
        n := 1;
        for i4 := 0 to i3-1 do
          if fRanks[i].Levels[i2].Replays[i4].Classification = R.Classification then
            Inc(n);
        if n > 1 then
          S := S + ' (' + IntToStr(n) + ')';
        R.Name := S;
      end;
end;

procedure TFFTKMain.btnManageDataFilesClick(Sender: TObject);
var
  F: TFDataFiles;
begin
  F := TFDataFiles.Create(self);
  F.SetStreams(fDataFiles);
  F.ShowModal;
  F.Free;
  UpdateInterface;
end;

procedure TFFTKMain.btnTalismansClick(Sender: TObject);
var
  F: TF_TalisTool;
begin
  F := TF_TalisTool.Create(self);
  try
    fTalismans.FixLevelLinks(fRanks);
    F.Prepare(fTalismans, fRanks);
    F.ShowModal;
    fTalismans.SetLevelLinks(fRanks);
  finally
    F.Free;
  end;
end;

procedure TFFTKMain.estReplays1Click(Sender: TObject);
var
  NLPath: String;
  Params: String;

  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;

  R, L, i: Integer;
begin
  if fFileName = '' then
  begin
    ShowMessage('This pack must be saved first.');
    Exit;
  end;

  if FileExists(ExtractFilePath(ParamStr(0)) + 'NeoLemmix.exe') then
    NLPath := ExtractFilePath(ParamStr(0)) + 'NeoLemmix.exe'
  else begin
    ShowMessage('Please locate a copy of NeoLemmix to use for testing (V10.13.16 or higher)');
    NLPath := OpenDialog('NeoLemmix.exe|*.exe');
    if NLPath = '' then Exit;
  end;

  DoBuildNXP(true);

  if not FileExists(ExtractFilePath(fFilename) + 'temp.nxp') then
    Exit;

  Params := '"' + ExtractFilePath(fFilename) + 'temp.nxp" replaytest "' + ExtractFilePath(fFilename) + 'replays\"';

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
  begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(NLPath);
    lpParameters := PChar(Params);
    nShow := SW_SHOWNORMAL;
  end;
  if ShellExecuteEx(@SEInfo) then
  begin
    repeat
      Application.ProcessMessages;
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
    until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
  end else begin
    ShowMessage('There was an error starting NeoLemmix to run the test.');
  end;

  DeleteFile(ExtractFilePath(fFilename) + 'temp.nxp');

  DoLoad(fFilename);

  for R := 0 to fRanks.Count-1 do
    for L := 0 to fRanks[R].Levels.Count-1 do
      for i := 0 to fRanks[R].Levels[L].Replays.Count-1 do
        if fRanks[R].Levels[L].Replays[i].Success then
          fRanks[R].Levels[L].Replays[i].CRC := fRanks[R].Levels[L].CRC;
end;

function TFFTKMain.HasLevelIDConflict(aID: Cardinal = 0): String;
var
  CheckList: array of Cardinal;
  LevelCount: Integer;
  R, L: Integer;

  function FindConflict(aID: Cardinal; aName: String = ''): Boolean;
  // filename instead of exact position or similar is used to avoid flagging filler levels
  var
    R, L: Integer;
  begin
    Result := false;
    for R := 0 to fRanks.Count-1 do
      for L := 0 to fRanks[R].Levels.Count-1 do
        if (fRanks[R].Levels[L].FileName <> aName) and (fRanks[R].Levels[L].LevelID = aID) then
        begin
          Result := true;
          Exit;
        end;
  end;
begin
  Result := '';
  if aID <> 0 then
  begin
    if FindConflict(aID) then
      Result := ' ';
  end else
    for R := 0 to fRanks.Count-1 do
      for L := 0 to fRanks[R].Levels.Count-1 do
        if FindConflict(fRanks[R].Levels[L].LevelID, fRanks[R].Levels[L].FileName) then
          Result := Result + fRanks[R].RankName + ' ' + IntToStr(L+1) + ' "' + Trim(fRanks[R].Levels[L].LevelTitle) + '"' + #13;
end;

procedure TFFTKMain.cbMacRotationClick(Sender: TObject);
begin
  if cbMacRotation.Checked then
    fSystemDat.Options := fSystemDat.Options + [sdoMacRotation]
  else
    fSystemDat.Options := fSystemDat.Options - [sdoMacRotation];
end;

end.
