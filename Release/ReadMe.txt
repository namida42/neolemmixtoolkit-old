NeoLemmix Flexi Toolkit

            Tool Version: V10.13.15-B
Minimum Supported Engine: V10.13.XX

This tool manages the contents of custom NeoLemmix packs, and can compile them into the NXP files
needed to play them as a pack on NeoLemmix.

For help on how to use it, please see the tutorial at:
http://www.neolemmix.com/old/flexitutorial.html

V10.13.15-B
-----------
> Fixed bug: Issue checker didn't detect all level ID duplicates properly.
> Gave more info in the error message when trying to build an NXP while level ID duplicates exist

V10.13.15
---------
> Fixed bug: Incorrect numbers would be appended to replay files when naming conflicts exist.
> Fixed bug: Error check would report an error when L2 / L3 / LPV styles aren't built into the NXP, even though they no longer need to be.
> Added option to use Mac-style music rotation, enabled by default.
> Replay files can now be added to or removed from the "replays" subfolder of a project without needing to manually modify the INI.
> Prevents building NXPs where level ID conflicts exist (can cause issues with replays, especially when running mass tests).
> Added an option in the Replay Manager to delete all failed replays.
> Removed "Hidden" option for talismans as this no longer serves any purpose.

V10.12.14
---------
> Fixes the issue where Flexi Toolkit may not show up on alt-tab menu for some users
> Fencer requirements are no longer hidden in the talisman editor

V10.12.13
---------
> Image import dialog can now be resized
> Image import dialog is offered when importing BMP or PNG files as custom data files

V10.12.12
---------
> Creates NXPs compatible with NeoLemmix V10.12.XX
> Talisman editor has been integrated into Flexi Toolkit, negating the need for a seperate app
> Removed direct support for many rarely-used custom images
> Added support for arbitrary custom data files, which can be used for the purpose of said custom images
> Removed the no-longer-useful "Export / Import SYSTEM.DAT" buttons
> As far as possible, custom graphics will be auto-converted to V10.12.XX layout
> Can set a forced lemming spriteset other than "default" or "xmas"

V10.011.011-B
-------------
> Fixes an error when trying to delete or extract music files

V10.011.011
-----------
> Marks packs as compatible with V10.011.XXX
> No longer includes Music Pack functionality

V1.48
-----
> No longer builds NXPs with LEVELxxx.DAT files
> Removed a few remaining no-longer-relevant options
> Issue detector checks if "success" and "failure" are in OGG format

V1.47n-C
--------
> Removed "V1.43n-F compatibility" option
> Fixed a bug with removing deleted replays from the folder
> Builds some extra metainfo into NXPs, to speed things up when using NeoLemmix V1.47n-B or higher
> Added options to quickly mass-rename level files and replays

V1.47n-B
--------
> Fixed a bug with mass export of NXRP replays

V1.47n
------
> Switched to a version numbering scheme that corresponds more closely with NeoLemmix itself
> Removed many no-longer-relevant options
> Graphic sets can now be obtained directly from the online collection
> And several more minor things that I don't want to make the effort to list :P

V1.18
-----
> Replaced the "can include a replay for each level" system from the previous version with a
  full-blown replay manager that can track multiple replays per level, allows you to categorise
  replays (for example, you can mark some replays as backroutes), and selectively export replays
  based on factors such as their category or whether they're up to date.
> NXPs no longer need to be exported to the project folder, they can now be exported to any folder.
> The issue checker will now inform the user if there are multiple levels with the same Level ID.

V1.17
-----
> Replays can now be tracked alongside levels.

V1.16
-----
> Fixes a bug which may corrupt levelpacks that are saved after using the buttons to move levels
  to different ranks.

V1.15
-----
> Fixes a bug where an error may occur when attempting to open very old packs.

V1.14
-----
> Level filenames can now be arbitrary. The list of which level is which position is kept in the
  levels.ini file in the levels subfolder.

V1.13
-----
> Actually fixed the bug with deleted LVL files.

V1.12
-----
> Added a checkbox on the Levels tab to indicate if a rank has a Rank Sign image or
  not; clicking it when it is unselected will allow selecting one.
> Default music rotation list for a new project is now the 17 Orig + 6 OhNo musics,
  rather than a blank list.
> Added an option to reset the music rotation to default.
> Added buttons to move levels to other ranks.
> System / lemming graphics can now be imported from GIF or BMP files as well as PNG,
  and selection of a transparent color can be done rather than having to create the
	images with transparency. Export is still PNG-only.
> Fixed a bug where deleted LVL files may remain in the output, and be re-added next
  time the project is loaded.

V1.11
-----
> Removes the "Disable Direct Drop" option; direct drop is now always disabled.

V1.10
-----
> Levels are now stored as single LVL files in a "levels" subdirectory. They are still built into
  DAT files when actually building the NXP, so no new player version is required to support this.

V1.09
-----
> Accepts all NeoLemmix-supported music formats, rather than only OGG and IT.
> Fixes various issues with the Check Issues option.
> Removed the warning about large music formats. This doesn't mean it's no longer unrecommended to
  build them into your NXPs.

V1.08
-----
> Supports PNGs of custom lemming sprites (at this stage, only regular and Xmas, not custom labels)
> Added an option to mass-edit system texts

V1.07
-----
> Supports use of PNG images rather than MAIN.DAT for custom graphics.
> Removed support for MAIN.DAT as it is no longer used by NeoLemmix V1.40n, except in older packs.
> Removed the "Disable Bomber Mask Adjust" option, as the mask is no longer customizable in any way
  so the fixed, built-in one will always be used.
> Added the option (under Show All Options) to force a pack to always use Regular or Santa lemmings,
  rather than determining it based on the graphic set.
> Changes the texts on several buttons to make it more obvious what they do.
> Fixes the bug where changes to the pack's contents wouldn't be saved before building an NXP.

V1.06
-----
> Major changes to the Music panel. The musics are no longer a preset list of fillable slots; they're
  now a collection of files, much like the graphic set tab. They're now referenced by name, so a button
  has been added to open a window where you can specify the rotation order (if you wish to use one).
> Changing the music number in the level popup window has been replaced with changing the level's name.
  If this doesn't work for any given level, open and re-save the level in the NeoLemmix Editor and this
  will fix it. (This happens because the level is in an older format, and Flexi Toolkit does not have
  the capability to update the format.)
> The button to add default graphic sets and musics have been removed, as NXPs no longer need to contain
  copies of these. In the case of musics, the names to use to refer to default ones are orig_01 to orig_17,
  ohno_01 to ohno_06, gimmick, and frenzy.

V1.05
-----
> Will mark an NXP as needing V1.38n or higher of NeoLemmix, if any levels use terrain rotation.
> Removed the "Timed Bombers/Stoners" option (no longer supported in NeoLemmix Player versions after
  V1.37n).
> Fixed a reference to EXEs where it should have referred to NXPs.

V1.04
-----
> Builds NXP files (for NeoLemmix V1.37n+, which no longer build data into the EXE) instead of EXEs.
> Can now assign pre-determined codes to levels, rather than solely using auto-generated ones.
> Always unlocking all non-secret levels is now the default; it remains possible to turn this option off.
> Fixed the bug where changes to the code seed wouldn't be saved.
> Fixed one of the default Karoshi postview texts that was partly cut off.

V1.03
-----
> Contains NeoLemmix V1.36n player
> Popup dialogs (such as that to edit preview/postview texts) now display centered on the toolkit's
  window, instead of in illogical fixed locations
> Newly-added ranks will now have an auto-generated generic name instead of just a blank name
> Added a confirmation dialog on the reset to defaults buttons
> Enabling replay checks is now the default option (rather than disabling them)
> Added a "Show All Options" button on the Settings tab; this allows access to options previously
  only accessible through the NeoLemmix Editor's SYSTEM.DAT editor (it does not, however, display
  any options that are not relevant to NeoLemmix)
> Title screen scroller texts now default to some NeoLemmix credits, as these are no longer forced
  in the Flexi player
> It is now possible to set the "Reset standard rotation after track" value on the musics tab to
  zero; doing so will set this value automatically when building an EXE so that every track is used


V1.02
-----
> Contains NeoLemmix V1.35n-C player
> Contains updated versions of graphic sets; to facilitate updating to these, a "Refresh" button
  has been added to the graphic sets tab which updates to the new versions
> Fixed a bug which may cause MAIN.DAT corruption when importing or exporting TALISMAN.DAT file
> Fixed various bugs on the Musics panel
> Added an "Issue Checker" on the Other panel; this will check for common problems with the pack,
  such as missing musics / graphic sets. It is NOT a comprehensive test of every possible issue;
  it just spots some of the most common and easily-overlooked matters.

V1.01
-----
> Music packs will no longer be deleted when saving a project; only when building an EXE (and
  even then, only if overwriting it with one being made during the build process)
> Gives a warning when attempting to build an EXE with a rank name that contains characters
  NeoLemmix currently cannot handle in rank names
> Fixes an issue with the Musics panel which may cause failure to save level packs
> Fixes an issue where the "Save" option (as opposed to Save As) may corrupt level packs
> Fixes an issue where building an EXE may corrupt level packs, unless the EXE is built
  immediately after either opening the pack or saving via the Save As function
> Fixes an issue where trying to play the Frenzy music in the toolkit would play the
  Gimmick music instead (this does not affect the actual project; only which track
  you hear upon clicking the "Play" button in the editor)

V1.00
-----
> Updated the contained NeoLemmix player to V1.35n-B
> Built-in copy of Martian graphic set has been updated to fix the exit's trigger area
> Fixed the bug where if a music pack was present in the project folder, it would get
  loaded among the graphic sets

V0.99
-----
> Can now create a music pack in addition to the standard built-in musics
> Now supports adding a custom icon to built EXEs
> Will give a warning when attempting to build an EXE on a version of Windows that doesn't
  support the nessecary API calls
> Music numbers for levels can be edited in the same popup window as pre/postview texts
> Fixed the bug where .EXE extension was not automatically appended to the EXE filename
  if no extension is specified
> Fixed the bug where the "Move Up" button on the Musics panel moved the track down instead
> Fixed the bug where the Gimmick music would be saved with the same filename extension as
  the Frenzy music, even if they were different formats
> Fixed the bug where some save file popup boxes may have an extension but no filename set
  as a default value
> Fixed the name of the edit pre/postview texts popup box
> Fixed various issues relating to pre/postview texts

(There is no V0.93 ~ V0.98. Blame my weird version numbering methods. :P)

V0.92
-----
> Official release version! :)
> If adding a graphic set with the same name as an existing one, it will overwrite
  it rather than result in a duplication with unpredictable results when saving
> All default graphic sets are now included and can be added to a project without
  needing a copy of the files. The same goes for musics, and a default MAIN.DAT.
> Fixed the bug where the default Code Seed value would follow a predictable pattern
  rather than being truly random each time a new pack is created.
> The tool now has its own icon.

V0.91
-----
> It is no longer impossible to have spaces at the start or end of text inputs
> Adding multiple files at a time is now possible in the Levels, Graphics and
  Musics tab (for Musics, only possible with "Add", not "Replace")
> Fixed the bug where graphic sets would have a blank name (thus also wouldn't
  save properly) when added via the Graphic Sets tab.
> Fixed the bug where it was impossible to select graphic sets / vgaspecs
  from the lists for deletion
> Fixed the bug where regular graphic sets may be attempted to be saved as
  VGASPECs and vice versa.

V0.90
-----
Initial version.