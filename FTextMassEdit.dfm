object FMassEditText: TFMassEditText
  Left = 91
  Top = 298
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Mass Edit Texts'
  ClientHeight = 377
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 106
    Height = 13
    Caption = '(text is set in the code)'
  end
  object mmTexts: TMemo
    Left = 176
    Top = 8
    Width = 289
    Height = 305
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object btnOK: TButton
    Left = 152
    Top = 344
    Width = 81
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 240
    Top = 344
    Width = 81
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
