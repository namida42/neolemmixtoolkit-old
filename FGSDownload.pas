unit FGSDownload;

interface

uses
  DatManage, StrUtils, OpenSaveDlgs, FlexiNamedStreams, LemNeoOnline,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFDownloadForm = class(TForm)
    lbSets: TListBox;
    btnDownload: TButton;
    btnExit: TButton;
    Label1: TLabel;
    procedure lbSetsClick(Sender: TObject);
    procedure btnDownloadClick(Sender: TObject);
  private
    fGsStreams: TNamedStreams;
  public
    constructor Create(aGsStreams: TNamedStreams);
    function GetGraphicSets: Boolean;
  end;

var
  FDownloadForm: TFDownloadForm;

implementation

{$R *.dfm}

constructor TFDownloadForm.Create(aGsStreams: TNamedStreams);
begin
  inherited Create(nil);
  fGsStreams := aGsStreams;
end;

function TFDownloadForm.GetGraphicSets: Boolean;
var
  SL: TStringList;
  i: Integer;
  S: String;

  function CheckIfAlreadyHas(aName: String): Boolean;
  var
    i: Integer;
  begin
    Result := false;
    aName := Lowercase(aName);
    for i := 0 to fGsStreams.Count-1 do
      if Lowercase(fGsStreams[i].Name) = aName then
        Result := true;
  end;

  function GetNameFromLine(aLine: String): String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Length(aLine) do
      if aLine[i] = '=' then
        Break
      else
        Result := Result + aLine[i];
  end;
begin
  Result := false;
  OnlineEnabled := true;
  SL := TStringList.Create;
  try
    if not DownloadToStringList('http://online.neolemmix.com/styles.php', SL) then
      Exit;
    for i := 0 to SL.Count-1 do
    begin
      if LeftStr(SL[i], 1) = '#' then Continue;
      if LeftStr(SL[i], 2) = 'x_' then Continue;
      if SL[i] = '' then Continue;
      S := GetNameFromLine(SL[i]);
      if CheckIfAlreadyHas(S) then Continue;
      lbSets.Items.Add(S);
    end;
    Result := true;
  finally
    SL.Free;
  end;
end;

procedure TFDownloadForm.lbSetsClick(Sender: TObject);
begin
  btnDownload.Enabled := lbSets.ItemIndex <> -1;
end;

procedure TFDownloadForm.btnDownloadClick(Sender: TObject);
var
  S: String;
  TempStream: TNamedMemoryStream;
begin
  if lbSets.ItemIndex = -1 then Exit;
  S := Lowercase(lbSets.Items[lbSets.ItemIndex]);
  TempStream := TNamedMemoryStream.Create;
  try
    if not DownloadToStream('http://www.neolemmix.com/styles/' + S + '.dat', TempStream) then
    begin
      ShowMessage('Download failed.');
      Exit;
    end;
    TempStream.Name := S;
    lbSets.Items.Delete(lbSets.ItemIndex);
    lbSets.ItemIndex := -1;
  except
    TempStream.Free;
    ShowMessage('Download failed due to an exception.');
    Exit;
  end;
  fGsStreams.Add(TempStream);
end;

end.
