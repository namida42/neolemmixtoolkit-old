unit FTSysDat;

interface

uses
  SysUtils, Classes, StrUtils;

type
  TSysDatOption = (sdoLookForLVLs,          // Options1
                   sdoIniCheat,
                   sdoCheatSecret,
                   sdoReplayCheck,
                   sdoTimedBomb,
                   sdoExtraMode,
                   sdoAutosteel,
                   sdoSimplesteel,
                   sdoOneWayRightMinerFix,  // Options2
                   sdoXmasPal,
                   sdoCenterSkills,
                   sdoPercentMode1,
                   sdoPercentMode2,
                   sdoDisableBomberMaskFix,
                   sdo60pxFall,
                   sdoABBAEntrance,
                   sdoX24Spawn,             // Options3
                   sdoShruggerGlitch,
                   sdoAllowDump,
                   sdoUnlockAll,
                   sdoNoDirectDrop,
                   sdoUsePngFiles,
                   sdoAlwaysXmasLemmings,
                   sdoAlwaysNormalLemmings,
                   sdoCompatible,           // Options4
                   sdoLemmingSpritesName,
                   sdoMacRotation,
                   sdo27,
                   sdo28,
                   sdo29,
                   sdo30,
                   sdo31);
  TSysDatOptions = set of TSysDatOption;

  TSysDatRec = packed record
    {0000}  PackName                   : array[0..31] of Char;
	          SecondLine                 : array[0..31] of Char;
            RankNames                  : array[0..14] of array[0..15] of Char;
            RankCount                  : Byte;
            SecretLevelCounts          : array[0..14] of Byte;
            TrackCount                 : Byte;
            CodeSeed                   : Byte;
            CheatCode                  : array[0..9] of Char;
            Options                    : LongWord;
            KResult                    : array[0..2] of array[0..1] of array[0..35] of Char;
            SResult                    : array[0..8] of array[0..1] of array[0..35] of Char;
            Congrats                   : array[0..17] of array[0..35] of Char;
            ScrollerTexts              : array[0..15] of array[0..35] of Char;
            StyleNames                 : array[0..255] of array[0..15] of Char;
            VgaspecNames               : array[0..255] of array[0..15] of Char;
  end;

  type TSystemDat = class
    private
      fSysDat: TSysDatRec;
      function GetOptions: TSysDatOptions;
      procedure SetOptions(aValue: TSysDatOptions);
      function GetSecondLine: String;
      procedure SetSecondLine(aValue: String);
      function GetStandardResult(Index: Integer; Line: Integer): String;
      procedure SetStandardResult(Index: Integer; Line: Integer; aValue: String);
      function GetScrollerText(Index: Integer): String;
      procedure SetScrollerText(Index: Integer; aValue: String);
      function GetTitle: String;
      procedure SetTitle(aValue: String);
      function GetRankName(Index: Integer): String;
      procedure SetRankName(Index: Integer; aValue: String);
      function GetRankCount: Integer;
      procedure SetRankCount(aValue: Integer);
      function GetGraphicSet(Index: Integer): String;
      procedure SetGraphicSet(Index: Integer; aValue: String);
      function GetVgaspec(Index: Integer): String;
      procedure SetVgaspec(Index: Integer; aValue: String);
    public
      constructor Create;
      procedure Clear;
      procedure DefaultOptions(AdvancedOnly: Boolean = false);
      procedure LoadFromFile(aFile: String);
      procedure LoadFromStream(aStream: TStream);
      procedure SaveToFile(aFile: String);
      procedure SaveToStream(aStream: TStream);
      procedure WipeStyleName(aValue: String);
      procedure WipeVgaspecName(aValue: String);
      property GraphicSet[Index: Integer]: String read GetGraphicSet write SetGraphicSet;
      property Options: TSysDatOptions read GetOptions write SetOptions;
      property StandardResult[Index: Integer; Line: Integer]: String read GetStandardResult write SetStandardResult;
      property RankCount: Integer read GetRankCount write SetRankCount;
      property RankName[Index: Integer]: String read GetRankName write SetRankName;
      property ScrollerText[Index: Integer]: String read GetScrollerText write SetScrollerText;
      property SecondLine: String read GetSecondLine write SetSecondLine;
      property Title: String read GetTitle write SetTitle;
      property Vgaspec[Index: Integer]: String read GetVgaspec write SetVgaspec;
  end;

const
  SysOptionsDefault = [sdoAllowDump,
                       sdoMacRotation];

implementation

constructor TSystemDat.Create;
begin
  inherited;
  Clear;
  DefaultOptions;
end;

procedure TSystemDat.Clear;
begin
  FillChar(fSysDat, SizeOf(fSysDat), 0);
  FillChar(fSysDat.PackName[0], 320, ' ');
  FillChar(fSysDat.CheatCode[0], 10, ' ');
  FillChar(fSysDat.KResult[0], 2088, ' ');
  FillChar(fSysDat.StyleNames[0], 8192, ' ');

    Vgaspec[0] := 'none            ';

    Title := 'Untitled Level Pack';
    SecondLine := 'By Unknown Author';

    StandardResult[0, 0] := 'ROCK BOTTOM! I hope for your sake';
    StandardResult[0, 1] := 'that you nuked that level.';
    StandardResult[1, 0] := 'Better rethink your strategy before';
    StandardResult[1, 1] := 'you try this level again!';
    StandardResult[2, 0] := 'A little more practice on the level';
    StandardResult[2, 1] := 'is definitely recommended.';
    StandardResult[3, 0] := 'You got pretty close that time.';
    StandardResult[3, 1] := 'Now try again for that few % extra.';
    StandardResult[4, 0] := 'OH NO, So near and yet so far...';
    StandardResult[4, 1] := 'Maybe this time.....';
    StandardResult[5, 0] := 'RIGHT ON. You can''t get much closer';
    StandardResult[5, 1] := 'than that. Let''s try the next...';
    StandardResult[6, 0] := 'That level seemed no problem to you';
    StandardResult[6, 1] := 'on that attempt. Onto the next....';
    StandardResult[7, 0] := 'You totally stormed that level!';
    StandardResult[7, 1] := 'Let''s see if you can do it again...';
    StandardResult[8, 0] := 'Superb! You rescued every lemming';
    StandardResult[8, 1] := 'on that one. Can you do it again?';

    ScrollerText[15] := 'http://www.neolemmix.com/';
end;

procedure TSystemDat.DefaultOptions(AdvancedOnly: Boolean = false);
begin
  Options := SysOptionsDefault;
end;

function TSystemDat.GetOptions: TSysDatOptions;
begin
  Result := TSysDatOptions(fSysDat.Options);
end;

procedure TSystemDat.SetOptions(aValue: TSysDatOptions);
begin
  fSysDat.Options := LongWord(aValue);
end;

function TSystemDat.GetSecondLine: String;
begin
  Result := Trim(fSysDat.SecondLine);
end;

procedure TSystemDat.SetSecondLine(aValue: String);
begin
  aValue := Trim(aValue);
  if Length(aValue) > 36 then aValue := LeftStr(aValue, 36);
  FillChar(fSysDat.SecondLine, 36, ' ');
  Move(aValue[1], fSysDat.SecondLine, Length(aValue));
end;

function TSystemDat.GetTitle: String;
begin
  Result := Trim(fSysDat.PackName);
end;

procedure TSystemDat.SetTitle(aValue: String);
begin
  aValue := Trim(aValue);
  if Length(aValue) > 32 then aValue := LeftStr(aValue, 32);
  FillChar(fSysDat.PackName, 32, ' ');
  Move(aValue[1], fSysDat.PackName, Length(aValue));
end;

function TSystemDat.GetStandardResult(Index: Integer; Line: Integer): String;
begin
  Result := Trim(fSysDat.SResult[Index, Line]);
end;

procedure TSystemDat.SetStandardResult(Index: Integer; Line: Integer; aValue: String);
begin
  aValue := Trim(aValue);
  if Length(aValue) > 36 then aValue := LeftStr(aValue, 36);
  FillChar(fSysDat.SResult[Index][Line], 36, ' ');
  Move(aValue[1], fSysDat.SResult[Index][Line], Length(aValue));
end;

function TSystemDat.GetScrollerText(Index: Integer): String;
begin
  Result := Trim(fSysDat.ScrollerTexts[Index]);
end;

procedure TSystemDat.SetScrollerText(Index: Integer; aValue: String);
begin
  aValue := Trim(aValue);
  if Length(aValue) > 36 then aValue := LeftStr(aValue, 36);
  FillChar(fSysDat.ScrollerTexts[Index], 36, ' ');
  Move(aValue[1], fSysDat.ScrollerTexts[Index], Length(aValue));
end;

function TSystemDat.GetRankName(Index: Integer): String;
begin
  Result := Trim(fSysDat.RankNames[Index]);
end;

procedure TSystemDat.SetRankName(Index: Integer; aValue: String);
begin
  aValue := Trim(aValue);
  if Length(aValue) > 16 then aValue := LeftStr(aValue, 16);
  FillChar(fSysDat.RankNames[Index], 16, ' ');
  Move(aValue[1], fSysDat.RankNames[Index], Length(aValue));
end;

function TSystemDat.GetRankCount: Integer;
begin
  Result := fSysDat.RankCount;
end;

procedure TSystemDat.SetRankCount(aValue: Integer);
begin
  fSysDat.RankCount := aValue;
end;

function TSystemDat.GetGraphicSet(Index: Integer): String;
begin
  Result := Trim(fSysDat.StyleNames[Index]);
end;

procedure TSystemDat.SetGraphicSet(Index: Integer; aValue: String);
begin
  aValue := LowerCase(Trim(aValue));
  if Length(aValue) > 16 then aValue := LeftStr(aValue, 16);
  FillChar(fSysDat.StyleNames[Index], 16, ' ');
  Move(aValue[1], fSysDat.StyleNames[Index], Length(aValue));
end;

function TSystemDat.GetVgaspec(Index: Integer): String;
begin
  Result := Trim(fSysDat.VgaspecNames[Index]);
end;

procedure TSystemDat.SetVgaspec(Index: Integer; aValue: String);
begin
  aValue := LowerCase(Trim(aValue));
  if Length(aValue) > 16 then aValue := LeftStr(aValue, 16);
  FillChar(fSysDat.VgaspecNames[Index], 16, ' ');
  Move(aValue[1], fSysDat.VgaspecNames[Index], Length(aValue));
end;

procedure TSystemDat.WipeStyleName(aValue: String);
var
  i: Integer;
begin
  aValue := LowerCase(Trim(aValue));
  for i := 0 to 255 do
    if Trim(fSysDat.StyleNames[i]) = aValue then FillChar(fSysDat.StyleNames[i], 16, ' ');
end;

procedure TSystemDat.WipeVgaspecName(aValue: String);
var
  i: Integer;
begin
  aValue := LowerCase(Trim(aValue));
  for i := 0 to 255 do
    if Trim(fSysDat.VgaspecNames[i]) = aValue then FillChar(fSysDat.VgaspecNames[i], 16, ' ');
end;

procedure TSystemDat.LoadFromFile(aFile: String);
var
  TempStream: TFileStream;
begin
  TempStream := TFileStream.Create(aFile, fmOpenRead);
  try
    TempStream.Seek(0, soFromBeginning);
    LoadFromStream(TempStream);
  finally
    TempStream.Free;
  end;
end;

procedure TSystemDat.LoadFromStream(aStream: TStream);
begin
  aStream.ReadBuffer(fSysDat, SizeOf(fSysDat));
end;

procedure TSystemDat.SaveToFile(aFile: String);
var
  TempStream: TFileStream;
begin
  if FileExists(aFile) then DeleteFile(aFile);
  TempStream := TFileStream.Create(aFile, fmCreate);
  try
    TempStream.Seek(0, soFromBeginning);
    SaveToStream(TempStream);
  finally
    TempStream.Free;
  end;
end;

procedure TSystemDat.SaveToStream(aStream: TStream);
begin
  aStream.WriteBuffer(fSysDat, SizeOf(fSysDat));
end;

end.