unit OpenSaveDlgs;

interface

uses
  Classes, SysUtils, Dialogs;

type
  TOpenMulti = class
    private
      fResults: TStringList;
      fFilter: String;
      function GetResultCount: Integer;
      function GetResult(Index: Integer): String;
    public
      constructor Create(aFilter: String);
      destructor Destroy; override;
      procedure Execute;
      property Count: Integer read GetResultCount;
      property Items[Index: Integer]: String read GetResult; default;
  end;

function OpenDialog(aFilter: String): String;
function SaveDialog(aFilter: String; aDefault: String = ''): String;

implementation

// TOpenMulti //

constructor TOpenMulti.Create(aFilter: String);
begin
  inherited Create;
  fResults := TStringList.Create;
  fFilter := aFilter;
  Execute;
end;

destructor TOpenMulti.Destroy;
begin
  fResults.Free;
  inherited;
end;

procedure TOpenMulti.Execute;
var
  OpenDlg: TOpenDialog;
  i: Integer;
begin
  OpenDlg := TOpenDialog.Create(nil);
  OpenDlg.Title := 'Open File...';
  OpenDlg.Options := [ofHideReadOnly, ofFileMustExist, ofAllowMultiSelect];
  OpenDlg.Filter := fFilter;
  if OpenDlg.Execute then
    for i := 0 to OpenDlg.Files.Count-1 do
      fResults.Add(OpenDlg.Files[i]);
  OpenDlg.Free;
end;

function TOpenMulti.GetResultCount: Integer;
begin
  Result := fResults.Count;
end;

function TOpenMulti.GetResult(Index: Integer): String;
begin
  Result := fResults[Index];
end;

// Misc //

function OpenDialog(aFilter: String): String;
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(nil);
  OpenDlg.Title := 'Open File...';
  OpenDlg.Options := [ofHideReadOnly, ofFileMustExist];
  OpenDlg.Filter := aFilter;
  if OpenDlg.Execute then
    Result := OpenDlg.FileName
  else
    Result := '';
  OpenDlg.Free;
end;

function SaveDialog(aFilter: String; aDefault: String = ''): String;
var
  SaveDlg: TSaveDialog;
begin
  SaveDlg := TSaveDialog.Create(nil);
  SaveDlg.Title := 'Save File...';
  SaveDlg.Options := [ofOverwritePrompt];
  SaveDlg.Filter := aFilter;
  if ExtractFileExt(aDefault) <> '' then
    SaveDlg.DefaultExt := ExtractFileExt(aDefault);
  if ExtractFileName(ChangeFileExt(aDefault, '')) <> '' then
    SaveDlg.FileName := aDefault;
  if SaveDlg.Execute then
    Result := SaveDlg.FileName
  else
    Result := '';
  SaveDlg.Free;
end;

end.