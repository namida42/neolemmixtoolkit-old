object ReplayManagerForm: TReplayManagerForm
  Left = 139
  Top = 419
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Replay Manager'
  ClientHeight = 515
  ClientWidth = 734
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 163
    Top = 384
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Filename Suffix:'
  end
  object Label2: TLabel
    Left = 206
    Top = 408
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Notes:'
  end
  object Label3: TLabel
    Left = 176
    Top = 432
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classification'
  end
  object btnOK: TButton
    Left = 616
    Top = 480
    Width = 105
    Height = 25
    Caption = 'Exit'
    ModalResult = 1
    TabOrder = 0
  end
  object lvReplays: TListView
    Left = 16
    Top = 16
    Width = 697
    Height = 353
    Columns = <
      item
        Caption = 'Level'
        Width = 200
      end
      item
        Caption = 'Classification'
        Width = 80
      end
      item
        Caption = 'Success'
        Width = 60
      end
      item
        Caption = 'Format'
      end
      item
        Caption = 'Notes'
        Width = 900
      end>
    GridLines = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = lvReplaysClick
  end
  object btnAdd: TButton
    Left = 32
    Top = 376
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = btnAddClick
  end
  object btnDelete: TButton
    Left = 32
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDeleteClick
  end
  object btnReplace: TButton
    Left = 32
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 4
    OnClick = btnReplaceClick
  end
  object ebFilename: TEdit
    Left = 248
    Top = 381
    Width = 121
    Height = 21
    TabOrder = 5
    OnChange = ebFilenameChange
    OnExit = ebExit
  end
  object ebNotes: TEdit
    Left = 248
    Top = 405
    Width = 473
    Height = 21
    TabOrder = 6
    OnChange = ebNotesChange
    OnExit = ebExit
  end
  object cbClassification: TComboBox
    Left = 248
    Top = 428
    Width = 169
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = cbClassificationChange
  end
  object cbSuccess: TCheckBox
    Left = 392
    Top = 383
    Width = 97
    Height = 17
    Caption = 'Successful'
    TabOrder = 8
    OnClick = cbSuccessClick
  end
  object cbUpToDate: TCheckBox
    Left = 488
    Top = 383
    Width = 97
    Height = 17
    Caption = 'Up To Date'
    TabOrder = 9
    OnClick = cbUpToDateClick
  end
  object btnAllSuccess: TButton
    Left = 8
    Top = 480
    Width = 113
    Height = 25
    Caption = 'Mark All Successful'
    TabOrder = 10
    OnClick = btnAllSuccessClick
  end
  object btnAllUpToDate: TButton
    Left = 128
    Top = 480
    Width = 113
    Height = 25
    Caption = 'Mark All Up-To-Date'
    TabOrder = 11
    OnClick = btnAllUpToDateClick
  end
  object btnSpread: TButton
    Left = 248
    Top = 480
    Width = 113
    Height = 25
    Caption = 'Spread Classification'
    TabOrder = 12
    OnClick = btnSpreadClick
  end
  object btnExport: TButton
    Left = 488
    Top = 480
    Width = 113
    Height = 25
    Caption = 'Mass Export'
    TabOrder = 13
    OnClick = btnExportClick
  end
  object btnDeleteFailed: TButton
    Left = 368
    Top = 480
    Width = 113
    Height = 25
    Caption = 'Delete All Failed'
    TabOrder = 14
    OnClick = btnDeleteFailedClick
  end
end
