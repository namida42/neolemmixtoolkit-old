object F_TalisTool: TF_TalisTool
  Left = 101
  Top = 236
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsToolWindow
  Caption = 'Talisman Tool'
  ClientHeight = 433
  ClientWidth = 609
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 260
    Width = 56
    Height = 13
    Caption = 'Description:'
  end
  object Label2: TLabel
    Left = 196
    Top = 284
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Rank:'
  end
  object Label3: TLabel
    Left = 292
    Top = 284
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Level:'
  end
  object Label4: TLabel
    Left = 197
    Top = 324
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Save:'
  end
  object Label5: TLabel
    Left = 199
    Top = 348
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Time:'
  end
  object Label6: TLabel
    Left = 259
    Top = 348
    Width = 3
    Height = 13
    Caption = ':'
  end
  object Label7: TLabel
    Left = 294
    Top = 348
    Width = 6
    Height = 13
    Caption = '+'
  end
  object Label8: TLabel
    Left = 333
    Top = 348
    Width = 3
    Height = 13
    Caption = 'f'
  end
  object Label9: TLabel
    Left = 203
    Top = 372
    Width = 22
    Height = 13
    Alignment = taRightJustify
    Caption = 'RR.:'
  end
  object Label10: TLabel
    Left = 280
    Top = 372
    Width = 9
    Height = 13
    Caption = 'to'
  end
  object TalismansList: TListView
    Left = 8
    Top = 8
    Width = 593
    Height = 241
    Columns = <
      item
        Caption = 'Level'
        Width = 105
      end
      item
        Caption = 'Type'
        Width = 84
      end
      item
        Caption = 'Description'
        Width = 400
      end>
    ColumnClick = False
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnClick = TalismansListClick
  end
  object btnAdd: TButton
    Left = 8
    Top = 256
    Width = 89
    Height = 25
    Caption = 'Add'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object btnDel: TButton
    Left = 8
    Top = 288
    Width = 89
    Height = 25
    Caption = 'Delete'
    TabOrder = 2
    OnClick = btnDelClick
  end
  object btnSort: TButton
    Left = 8
    Top = 320
    Width = 89
    Height = 25
    Caption = 'Sort'
    TabOrder = 3
    OnClick = btnSortClick
  end
  object ebDescription: TEdit
    Left = 176
    Top = 256
    Width = 425
    Height = 21
    MaxLength = 64
    TabOrder = 4
    OnChange = ebDescriptionChange
  end
  object ebRank: TEdit
    Left = 232
    Top = 280
    Width = 49
    Height = 21
    TabOrder = 5
    OnChange = ebRankChange
  end
  object ebLevel: TEdit
    Left = 328
    Top = 280
    Width = 49
    Height = 21
    TabOrder = 6
    OnChange = ebLevelChange
  end
  object rgType: TRadioGroup
    Left = 112
    Top = 280
    Width = 73
    Height = 97
    Caption = 'Type'
    ItemIndex = 0
    Items.Strings = (
      'Bronze'
      'Silver'
      'Gold')
    TabOrder = 7
    OnClick = rgTypeClick
  end
  object ebSave: TEdit
    Left = 232
    Top = 320
    Width = 49
    Height = 21
    TabOrder = 8
    OnChange = ebSaveChange
  end
  object ebTimeMin: TEdit
    Tag = 1020
    Left = 232
    Top = 344
    Width = 25
    Height = 21
    TabOrder = 9
    OnChange = ebTimeChange
  end
  object ebTimeSec: TEdit
    Tag = 17
    Left = 264
    Top = 344
    Width = 25
    Height = 21
    TabOrder = 10
    OnChange = ebTimeChange
  end
  object ebTimeFrame: TEdit
    Tag = 1
    Left = 304
    Top = 344
    Width = 25
    Height = 21
    TabOrder = 11
    OnChange = ebTimeChange
  end
  object ebRRMin: TEdit
    Left = 232
    Top = 368
    Width = 41
    Height = 21
    TabOrder = 12
    OnChange = ebRRMinChange
  end
  object ebRRMax: TEdit
    Left = 296
    Top = 368
    Width = 41
    Height = 21
    TabOrder = 13
    OnChange = ebRRMaxChange
  end
  object gbSkills: TGroupBox
    Left = 392
    Top = 280
    Width = 209
    Height = 145
    Caption = 'Skill Limits'
    TabOrder = 14
    object cbSkillList: TComboBox
      Left = 16
      Top = 16
      Width = 177
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = 'Walker'
      OnChange = cbSkillListChange
      Items.Strings = (
        'Walker'
        'Climber'
        'Swimmer'
        'Floater'
        'Glider'
        'Mechanic'
        'Bomber'
        'Stoner'
        'Blocker'
        'Platformer'
        'Builder'
        'Stacker'
        'Basher'
        'Miner'
        'Digger'
        'Cloner'
        'Fencer')
    end
    object cbDoLimit: TCheckBox
      Left = 16
      Top = 40
      Width = 97
      Height = 25
      Caption = 'Limit this skill to'
      TabOrder = 1
      OnClick = cbDoLimitClick
    end
    object ebSkillLimit: TEdit
      Left = 128
      Top = 43
      Width = 65
      Height = 21
      TabOrder = 2
      OnChange = ebSkillLimitChange
    end
    object ebSkillTotal: TEdit
      Left = 128
      Top = 67
      Width = 65
      Height = 21
      TabOrder = 3
      OnChange = ebSkillTotalChange
    end
    object cbDoLimitTotal: TCheckBox
      Left = 16
      Top = 64
      Width = 105
      Height = 25
      Caption = 'Limit total skills to'
      TabOrder = 4
      OnClick = cbDoLimitTotalClick
    end
    object cbOneSkillPerLem: TCheckBox
      Left = 16
      Top = 88
      Width = 177
      Height = 25
      Caption = 'Limit to one skill per lemming'
      TabOrder = 5
      OnClick = cbOneSkillPerLemClick
    end
    object cbOneLem: TCheckBox
      Left = 16
      Top = 112
      Width = 177
      Height = 25
      Caption = 'Limit skills to one lemming'
      TabOrder = 6
      OnClick = cbOneLemClick
    end
  end
  object btnImportLVL: TButton
    Left = 8
    Top = 352
    Width = 89
    Height = 25
    Caption = 'Import LVL Data'
    TabOrder = 15
    OnClick = btnImportLVLClick
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 384
    Width = 177
    Height = 41
    Caption = 'Signature'
    TabOrder = 16
    object ebSignature: TEdit
      Left = 8
      Top = 14
      Width = 161
      Height = 21
      TabOrder = 0
      OnChange = ebSignatureChange
    end
  end
  object btnExit: TButton
    Left = 248
    Top = 400
    Width = 75
    Height = 25
    Caption = 'Exit'
    ModalResult = 1
    TabOrder = 17
  end
end
