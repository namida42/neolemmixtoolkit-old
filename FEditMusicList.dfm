object FMusicList: TFMusicList
  Left = 343
  Top = 434
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Music Rotation'
  ClientHeight = 377
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 305
    Height = 13
    Alignment = taCenter
    Caption = 'Enter track names for standard rotation; one per line.'
  end
  object mmTracks: TMemo
    Left = 16
    Top = 32
    Width = 289
    Height = 305
    TabOrder = 0
  end
  object btnOK: TButton
    Left = 72
    Top = 344
    Width = 81
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 168
    Top = 344
    Width = 81
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
