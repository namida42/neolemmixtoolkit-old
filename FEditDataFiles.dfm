object FDataFiles: TFDataFiles
  Left = 343
  Top = 434
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Custom Data Files'
  ClientHeight = 377
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 276
    Height = 13
    Caption = 'Please read the readme or tutorial before messing with this!'
  end
  object btnOK: TButton
    Left = 120
    Top = 344
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object lbFiles: TListBox
    Left = 16
    Top = 40
    Width = 281
    Height = 257
    ItemHeight = 13
    TabOrder = 1
  end
  object btnAdd: TButton
    Left = 40
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = btnAddClick
  end
  object btnDelete: TButton
    Left = 120
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDeleteClick
  end
  object btnExtract: TButton
    Left = 200
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Extract'
    TabOrder = 4
    OnClick = btnExtractClick
  end
end
