unit Talis_Main;

interface

uses
  TalisData, FlexiRanks,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, StdCtrls, ExtCtrls;

type
  TF_TalisTool = class(TForm)
    TalismansList: TListView;
    btnAdd: TButton;
    btnDel: TButton;
    btnSort: TButton;
    ebDescription: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ebRank: TEdit;
    ebLevel: TEdit;
    rgType: TRadioGroup;
    ebSave: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    ebTimeMin: TEdit;
    ebTimeSec: TEdit;
    ebTimeFrame: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    ebRRMin: TEdit;
    Label10: TLabel;
    ebRRMax: TEdit;
    gbSkills: TGroupBox;
    cbSkillList: TComboBox;
    cbDoLimit: TCheckBox;
    ebSkillLimit: TEdit;
    btnImportLVL: TButton;
    ebSkillTotal: TEdit;
    cbDoLimitTotal: TCheckBox;
    cbOneSkillPerLem: TCheckBox;
    cbOneLem: TCheckBox;
    GroupBox2: TGroupBox;
    ebSignature: TEdit;
    btnExit: TButton;
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnSortClick(Sender: TObject);
    procedure ebDescriptionChange(Sender: TObject);
    procedure TalismansListClick(Sender: TObject);
    procedure ebRankChange(Sender: TObject);
    procedure ebLevelChange(Sender: TObject);
    procedure rgTypeClick(Sender: TObject);
    procedure ebSaveChange(Sender: TObject);
    procedure ebTimeChange(Sender: TObject);
    procedure ebRRMinChange(Sender: TObject);
    procedure ebRRMaxChange(Sender: TObject);
    procedure btnImportLVLClick(Sender: TObject);
    procedure cbSkillListChange(Sender: TObject);
    procedure cbDoLimitClick(Sender: TObject);
    procedure ebSkillLimitChange(Sender: TObject);
    procedure cbDoLimitTotalClick(Sender: TObject);
    procedure ebSkillTotalChange(Sender: TObject);
    procedure cbOneSkillPerLemClick(Sender: TObject);
    procedure cbOneLemClick(Sender: TObject);
    procedure ebSignatureChange(Sender: TObject);
  private
    fTalismanData: TTalismans;
    fRanks: TGameRanks;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Prepare(aTal: TTalismans; aRanks: TGameRanks);
    procedure UpdateList;
    property Talismans: TTalismans read fTalismanData;
  end;

  function LeadZeroStr(aValue: Integer; aLen: Integer): String;
  procedure SetCheck(aCheckbox: TCheckbox; aValue: Boolean);

var
  F_TalisTool: TF_TalisTool;

implementation

{$R *.dfm}

constructor TF_TalisTool.Create(aOwner: TComponent);
begin
  inherited;
end;

destructor TF_TalisTool.Destroy;
begin
  inherited;
end;

procedure TF_TalisTool.Prepare(aTal: TTalismans; aRanks: TGameRanks);
begin
  fTalismanData := aTal;
  fRanks := aRanks;
  UpdateList;
end;

procedure TF_TalisTool.UpdateList;
var
  i: Integer;
  ListItem: TListItem;
begin
  //Sel := TalismansList.ItemIndex;
  for i := 0 to fTalismanData.Count-1 do
  begin
    if i < TalismansList.Items.Count then
      ListItem := TalismansList.Items[i]
    else
      ListItem := TalismansList.Items.Add;
    with fTalismanData[i] do
    begin

      // Level
      ListItem.Caption := 'Rank ' + IntToStr(RankNumber+1) + ', Level ' + IntToStr(LevelNumber+1);
      ListItem.SubItems.Clear;

      // Type
      case TalismanType of
        1: ListItem.SubItems.Add('Bronze');
        2: ListItem.SubItems.Add('Silver');
        3: ListItem.SubItems.Add('Gold');
        else ListItem.SubItems.Add('Hidden');
      end;

      ListItem.SubItems.Add(Description);
    end;
  end;
  for i := TalismansList.Items.Count-1 downto fTalismanData.Count do
    TalismansList.Items.Delete(i);
  //TalismansList.ItemIndex := Sel;
  TalismansList.Update;
end;

procedure TF_TalisTool.btnAddClick(Sender: TObject);
begin
  fTalismanData.Add;
  UpdateList;
  TalismansList.ItemIndex := TalismansList.Items.Count-1;
  TalismansListClick(nil); 
end;

procedure TF_TalisTool.btnDelClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  fTalismanData.Delete(TalismansList.ItemIndex);
  TalismansList.ItemIndex := -1;
  UpdateList;
end;

procedure TF_TalisTool.btnSortClick(Sender: TObject);
begin
  fTalismanData.SortTalismans; 
  UpdateList;
end;

procedure TF_TalisTool.ebDescriptionChange(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  fTalismanData[TalismansList.ItemIndex].Description := ebDescription.Text;
  UpdateList;
end;

procedure TF_TalisTool.TalismansListClick(Sender: TObject);
var
  TLim: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit; //just in case
  with fTalismanData[TalismansList.ItemIndex] do
  begin
    ebDescription.Text := Description;
    ebRank.Text := IntToStr(RankNumber + 1);
    ebLevel.Text := IntToStr(LevelNumber + 1);
    if not (TalismanType in [0, 1, 2, 3]) then
      TalismanType := 0;
    rgType.ItemIndex := TalismanType - 1;
    ebSave.Text := IntToStr(SaveRequirement);
    TLim := TimeLimit;
    ebTimeMin.Text := IntToStr(TLim div ebTimeMin.Tag);
    ebTimeSec.Text := LeadZeroStr((TLim mod ebTimeMin.Tag) div ebTimeSec.Tag, 2);
    ebTimeFrame.Text := IntToStr(TLim mod ebTimeFrame.Tag);
    ebRRMin.Text := IntToStr(RRMin);
    ebRRMax.Text := IntToStr(RRMax);
    SetCheck(cbDoLimit, SkillLimit[cbSkillList.ItemIndex] >= 0);
    if cbDoLimit.Checked then
      ebSkillLimit.Text := IntToStr(SkillLimit[cbSkillList.ItemIndex])
    else
      ebSkillLimit.Text := '';
    SetCheck(cbDoLimitTotal, TotalSkillLimit >= 0);
    if cbDoLimitTotal.Checked then
      ebSkillTotal.Text := IntToStr(TotalSkillLimit)
    else
      ebSkillTotal.Text := '';
    SetCheck(cbOneSkillPerLem, tmOneSkill in MiscOptions);
    SetCheck(cbOneLem, tmOneLemming in MiscOptions);
    ebSignature.Text := IntToHex(Signature, 8);
  end;
end;

procedure TF_TalisTool.ebRankChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebRank.Text, -1);
  NewN := NewN - 1;
  if (NewN < 0) or (NewN > 14) then Exit;
  fTalismanData[TalismansList.ItemIndex].RankNumber := NewN;
  UpdateList;
end;

procedure TF_TalisTool.ebLevelChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebLevel.Text, -1);
  NewN := NewN - 1;
  if (NewN < 0) or (NewN > 255) then Exit;
  fTalismanData[TalismansList.ItemIndex].LevelNumber := NewN;
  UpdateList;
end;

procedure TF_TalisTool.rgTypeClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  fTalismanData[TalismansList.ItemIndex].TalismanType := rgType.ItemIndex + 1;
  UpdateList;
end;

procedure TF_TalisTool.ebSaveChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebSave.Text, -1);
  if (NewN < 0) then Exit;
  fTalismanData[TalismansList.ItemIndex].SaveRequirement := NewN;
end;

procedure TF_TalisTool.ebTimeChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebTimeMin.Text, -1);
  if (NewN < 0) then Exit;
  NewN := StrToIntDef(ebTimeSec.Text, -1);
  if (NewN < 0) or (NewN > 59) then Exit;
  NewN := StrToIntDef(ebTimeFrame.Text, -1);
  if (NewN < 0) or (NewN > 16) then Exit;
  NewN := (StrToInt(ebTimeMin.Text) * ebTimeMin.Tag)
        + (StrToInt(ebTimeSec.Text) * ebTimeSec.Tag)
        + (StrToInt(ebTimeFrame.Text) * ebTimeFrame.Tag);
  fTalismanData[TalismansList.ItemIndex].TimeLimit := NewN;
end;

function LeadZeroStr(aValue: Integer; aLen: Integer): String;
begin
  Result := IntToStr(aValue);
  while Length(Result) < aLen do
    Result := '0' + Result;
end;

procedure TF_TalisTool.ebRRMinChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebRRMin.Text, -1);
  if (NewN < 1) or (NewN > 99) then Exit;
  fTalismanData[TalismansList.ItemIndex].RRMin := NewN;
end;

procedure TF_TalisTool.ebRRMaxChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToIntDef(ebRRMax.Text, -1);
  if (NewN < 1) or (NewN > 99) then Exit;
  fTalismanData[TalismansList.ItemIndex].RRMax := NewN;
end;

procedure TF_TalisTool.btnImportLVLClick(Sender: TObject);
var
  Tal: TTalisman;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  try
    Tal := fTalismanData[TalismansList.ItemIndex];
    Tal.LoadFromLevelStream(fRanks[Tal.RankNumber].Levels[Tal.LevelNumber]);
  finally
    UpdateList;
    TalismansListClick(nil); //such hacky way to update the properties xD
  end;
end;

procedure TF_TalisTool.cbSkillListChange(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  with fTalismanData[TalismansList.ItemIndex] do
  begin
    SetCheck(cbDoLimit, SkillLimit[cbSkillList.ItemIndex] >= 0);
    if cbDoLimit.Checked then
      ebSkillLimit.Text := IntToStr(SkillLimit[cbSkillList.ItemIndex])
    else
      ebSkillLimit.Text := '';
  end;
end;

procedure TF_TalisTool.cbDoLimitClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  if cbDoLimit.Checked then
  begin
    fTalismanData[TalismansList.ItemIndex].SkillLimit[cbSkillList.ItemIndex] := 0;
    ebSkillLimit.Text := '0';
  end else begin
    fTalismanData[TalismansList.ItemIndex].SkillLimit[cbSkillList.ItemIndex] := -1;
    ebSkillLimit.Text := '';
  end;
end;

procedure TF_TalisTool.ebSkillLimitChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  if not cbDoLimit.Checked then Exit;
  NewN := StrToIntDef(ebSkillLimit.Text, -1);
  if (NewN < 0) then Exit;
  fTalismanData[TalismansList.ItemIndex].SkillLimit[cbSkillList.ItemIndex] := NewN;
end;

procedure SetCheck(aCheckbox: TCheckbox; aValue: Boolean);
var
  TempEvent: TNotifyEvent;
begin
  TempEvent := aCheckbox.OnClick;
  aCheckbox.OnClick := nil;
  aCheckbox.Checked := aValue;
  aCheckbox.OnClick := TempEvent;
end;

procedure TF_TalisTool.cbDoLimitTotalClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  if cbDoLimitTotal.Checked then
  begin
    fTalismanData[TalismansList.ItemIndex].TotalSkillLimit := 0;
    ebSkillTotal.Text := '0';
  end else begin
    fTalismanData[TalismansList.ItemIndex].TotalSkillLimit := -1;
    ebSkillTotal.Text := '';
  end;
end;

procedure TF_TalisTool.ebSkillTotalChange(Sender: TObject);
var
  NewN: Integer;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  if not cbDoLimitTotal.Checked then Exit;
  NewN := StrToIntDef(ebSkillTotal.Text, -1);
  if (NewN < 0) then Exit;
  fTalismanData[TalismansList.ItemIndex].TotalSkillLimit := NewN;
end;

procedure TF_TalisTool.cbOneSkillPerLemClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  with fTalismanData[TalismansList.ItemIndex] do
    if cbOneSkillPerLem.Checked then
      MiscOptions := MiscOptions + [tmOneSkill]
    else
      MiscOptions := MiscOptions - [tmOneSkill];
end;

procedure TF_TalisTool.cbOneLemClick(Sender: TObject);
begin
  if TalismansList.ItemIndex = -1 then Exit;
  with fTalismanData[TalismansList.ItemIndex] do
    if cbOneLem.Checked then
      MiscOptions := MiscOptions + [tmOneLemming]
    else
      MiscOptions := MiscOptions - [tmOneLemming];
end;

procedure TF_TalisTool.ebSignatureChange(Sender: TObject);
var
  NewN: Int64;
begin
  if TalismansList.ItemIndex = -1 then Exit;
  NewN := StrToInt64Def('0x' + ebSignature.Text, -1);
  if (NewN < 0) or (NewN > $FFFFFFFF) then Exit;
  fTalismanData[TalismansList.ItemIndex].Signature := NewN;
end;

end.
