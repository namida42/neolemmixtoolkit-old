object FFTKMain: TFFTKMain
  Left = 166
  Top = 317
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'NeoLemmix Flexi Toolkit'
  ClientHeight = 445
  ClientWidth = 353
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pcMain: TPageControl
    Left = 0
    Top = 0
    Width = 353
    Height = 465
    ActivePage = pcOther
    TabOrder = 0
    object pcSettings: TTabSheet
      Caption = 'Settings'
      ImageIndex = 4
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Title:'
      end
      object Splitter1: TSplitter
        Left = 0
        Top = 48
        Width = 345
        Height = 3
        Cursor = crArrow
        Align = alNone
        Beveled = True
        ResizeStyle = rsNone
      end
      object Label4: TLabel
        Left = 8
        Top = 56
        Width = 67
        Height = 13
        Caption = 'Game Options'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 192
        Width = 345
        Height = 3
        Cursor = crArrow
        Align = alNone
        Beveled = True
        ResizeStyle = rsNone
      end
      object Label5: TLabel
        Left = 8
        Top = 200
        Width = 57
        Height = 13
        Caption = 'Game Texts'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 8
        Top = 144
        Width = 77
        Height = 13
        Caption = 'Lemming Sprites'
      end
      object ebTitle: TEdit
        Left = 40
        Top = 13
        Width = 297
        Height = 21
        MaxLength = 32
        TabOrder = 0
        OnChange = ebTitleChange
      end
      object cbOptEnableDump: TCheckBox
        Left = 16
        Top = 72
        Width = 169
        Height = 17
        Caption = 'Enable LVL/Image Mass Dump'
        TabOrder = 1
        OnClick = cbOptEnableDumpClick
      end
      object ebGameText: TEdit
        Left = 8
        Top = 357
        Width = 329
        Height = 21
        MaxLength = 32
        TabOrder = 2
        OnChange = ebGameTextChange
      end
      object btnMassEditTexts: TButton
        Left = 8
        Top = 384
        Width = 113
        Height = 25
        Caption = 'Mass Edit'
        TabOrder = 3
        OnClick = btnMassEditTextsClick
      end
      object cbLemmingSprites: TComboBox
        Left = 16
        Top = 160
        Width = 153
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 4
        Text = '(select from graphic set)'
        OnChange = cbLemmingSpritesChange
        Items.Strings = (
          '(select from graphic set)'
          'default'
          'xmas')
      end
      object cbChooseText: TListBox
        Left = 8
        Top = 216
        Width = 329
        Height = 129
        ItemHeight = 13
        Items.Strings = (
          'Title Screen Second Line'
          'Scroller Text 1'
          'Scroller Text 2'
          'Scroller Text 3'
          'Scroller Text 4'
          'Scroller Text 5'
          'Scroller Text 6'
          'Scroller Text 7'
          'Scroller Text 8'
          'Scroller Text 9'
          'Scroller Text 10'
          'Scroller Text 11'
          'Scroller Text 12'
          'Scroller Text 13'
          'Scroller Text 14'
          'Scroller Text 15'
          'Scroller Text 16'
          'Postview Screen - None Saved, Line 1'
          'Postview Screen - None Saved, Line 2'
          'Postview Screen - Very Bad, Line 1'
          'Postview Screen - Very Bad, Line 2'
          'Postview Screen - Bad, Line 1'
          'Postview Screen - Bad, Line 2'
          'Postview Screen - Near-Pass, Line 1'
          'Postview Screen - Near-Pass, Line 2'
          'Postview Screen - One Lemming Short, Line 1'
          'Postview Screen - One Lemming Short, Line 2'
          'Postview Screen - Exact Pass, Line 1'
          'Postview Screen - Exact Pass, Line 2'
          'Postview Screen - Good, Line 1'
          'Postview Screen - Good, Line 2'
          'Postview Screen - Great, Line 1'
          'Postview Screen - Great, Line 2'
          'Postview Screen - 100%, Line 1'
          'Postview Screen - 100%, Line 2')
        TabOrder = 5
        OnClick = cbChooseTextChange
      end
      object btnTalismans: TButton
        Left = 240
        Top = 104
        Width = 83
        Height = 25
        Caption = 'Talismans'
        TabOrder = 6
        OnClick = btnTalismansClick
      end
      object cbMacRotation: TCheckBox
        Left = 16
        Top = 88
        Width = 169
        Height = 17
        Caption = 'Mac-Style Music Rotation'
        TabOrder = 7
        OnClick = cbMacRotationClick
      end
    end
    object pcLevels: TTabSheet
      Caption = 'Levels'
      object Label6: TLabel
        Left = 16
        Top = 8
        Width = 31
        Height = 13
        Caption = 'Ranks'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 120
        Width = 60
        Height = 13
        Caption = 'Rank Name:'
      end
      object Splitter4: TSplitter
        Left = 0
        Top = 152
        Width = 345
        Height = 3
        Cursor = crArrow
        Align = alNone
        Beveled = True
        ResizeStyle = rsNone
      end
      object Label9: TLabel
        Left = 16
        Top = 164
        Width = 31
        Height = 13
        Caption = 'Levels'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object lbRanks: TListBox
        Left = 16
        Top = 32
        Width = 177
        Height = 81
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbRanksClick
      end
      object btnAddRank: TButton
        Left = 200
        Top = 20
        Width = 65
        Height = 25
        Caption = 'Add'
        TabOrder = 1
        OnClick = btnAddRankClick
      end
      object btnDelRank: TButton
        Left = 272
        Top = 20
        Width = 65
        Height = 25
        Caption = 'Delete'
        TabOrder = 2
        OnClick = btnDelRankClick
      end
      object btnRankUp: TButton
        Left = 200
        Top = 52
        Width = 65
        Height = 25
        Caption = 'Move Up'
        TabOrder = 3
        OnClick = btnRankUpClick
      end
      object btnRankDown: TButton
        Left = 272
        Top = 52
        Width = 65
        Height = 25
        Caption = 'Move Down'
        TabOrder = 4
        OnClick = btnRankDownClick
      end
      object ebRankName: TEdit
        Left = 80
        Top = 117
        Width = 105
        Height = 21
        MaxLength = 16
        TabOrder = 5
        OnChange = ebRankNameChange
      end
      object lbLevels: TListBox
        Left = 16
        Top = 184
        Width = 321
        Height = 153
        ItemHeight = 13
        TabOrder = 6
        OnDblClick = lbLevelsDblClick
      end
      object btnAddLevel: TButton
        Left = 25
        Top = 356
        Width = 72
        Height = 25
        Caption = 'Add'
        TabOrder = 7
        OnClick = btnAddLevelClick
      end
      object btnDelLevel: TButton
        Left = 101
        Top = 356
        Width = 72
        Height = 25
        Caption = 'Delete'
        TabOrder = 8
        OnClick = btnDelLevelClick
      end
      object btnLevelMoveUp: TButton
        Left = 25
        Top = 388
        Width = 72
        Height = 25
        Caption = 'Move Up'
        TabOrder = 9
        OnClick = btnLevelMoveUpClick
      end
      object btnLevelMoveDown: TButton
        Left = 101
        Top = 388
        Width = 71
        Height = 25
        Caption = 'Move Down'
        TabOrder = 10
        OnClick = btnLevelMoveDownClick
      end
      object btnLevelExtract: TButton
        Left = 253
        Top = 356
        Width = 72
        Height = 25
        Caption = 'Extract'
        TabOrder = 11
        OnClick = btnLevelExtractClick
      end
      object btnLevelReplace: TButton
        Left = 177
        Top = 356
        Width = 72
        Height = 25
        Caption = 'Replace'
        TabOrder = 12
        OnClick = btnLevelReplaceClick
      end
      object btnPrevRank: TButton
        Left = 177
        Top = 388
        Width = 72
        Height = 25
        Caption = 'To Prev Rank'
        TabOrder = 13
        OnClick = btnPrevRankClick
      end
      object btnNextRank: TButton
        Left = 253
        Top = 388
        Width = 72
        Height = 25
        Caption = 'To Next Rank'
        TabOrder = 14
        OnClick = btnNextRankClick
      end
      object cbRankSign: TCheckBox
        Left = 208
        Top = 88
        Width = 121
        Height = 17
        Caption = 'Has Rank Sign'
        TabOrder = 15
        OnClick = cbRankSignClick
      end
    end
    object pcGraphics: TTabSheet
      Caption = 'Graphic Sets'
      ImageIndex = 1
      object Label10: TLabel
        Left = 16
        Top = 8
        Width = 61
        Height = 13
        Caption = 'Graphic Sets'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object lbGraphics: TListBox
        Left = 8
        Top = 24
        Width = 329
        Height = 353
        ItemHeight = 13
        TabOrder = 0
      end
      object btnGsAdd: TButton
        Left = 48
        Top = 384
        Width = 65
        Height = 25
        Caption = 'Add'
        TabOrder = 1
        OnClick = btnGsAddClick
      end
      object btnGsDelete: TButton
        Left = 120
        Top = 384
        Width = 65
        Height = 25
        Caption = 'Delete'
        TabOrder = 2
        OnClick = btnGsDeleteClick
      end
      object btnGsDownload: TButton
        Left = 208
        Top = 384
        Width = 97
        Height = 25
        Caption = 'Download'
        TabOrder = 3
        OnClick = btnGsDownloadClick
      end
    end
    object pcMusic: TTabSheet
      Caption = 'Musics'
      ImageIndex = 3
      object Label12: TLabel
        Left = 16
        Top = 8
        Width = 33
        Height = 13
        Caption = 'Musics'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object lbMusics: TListBox
        Left = 8
        Top = 24
        Width = 329
        Height = 289
        ItemHeight = 13
        TabOrder = 0
      end
      object btnMusicPlay: TButton
        Left = 12
        Top = 352
        Width = 157
        Height = 25
        Caption = 'Play'
        TabOrder = 1
        OnClick = btnMusicPlayClick
      end
      object btnMusicStop: TButton
        Left = 180
        Top = 352
        Width = 157
        Height = 25
        Caption = 'Stop'
        TabOrder = 2
        OnClick = btnMusicStopClick
      end
      object btnMusicExtract: TButton
        Left = 236
        Top = 320
        Width = 101
        Height = 25
        Caption = 'Extract'
        TabOrder = 3
        OnClick = btnMusicExtractClick
      end
      object btnMusicDelete: TButton
        Left = 124
        Top = 320
        Width = 101
        Height = 25
        Caption = 'Delete'
        TabOrder = 4
        OnClick = btnMusicDel1Click
      end
      object btnMusicAdd: TButton
        Left = 12
        Top = 320
        Width = 101
        Height = 25
        Caption = 'Add'
        TabOrder = 5
        OnClick = btnMusicAdd1Click
      end
      object btnMusicList: TButton
        Left = 12
        Top = 384
        Width = 157
        Height = 25
        Caption = 'Edit Music Rotation'
        TabOrder = 6
        OnClick = btnMusicListClick
      end
      object btnResetRotation: TButton
        Left = 180
        Top = 384
        Width = 157
        Height = 25
        Caption = 'Reset Music Rotation'
        TabOrder = 7
        OnClick = btnResetRotationClick
      end
    end
    object pcOther: TTabSheet
      Caption = 'Other Data'
      ImageIndex = 2
      object Splitter7: TSplitter
        Left = 0
        Top = 280
        Width = 345
        Height = 3
        Cursor = crArrow
        Align = alNone
        Beveled = True
        ResizeStyle = rsNone
      end
      object Label15: TLabel
        Left = 16
        Top = 288
        Width = 68
        Height = 13
        Caption = 'Issue Checker'
      end
      object Label13: TLabel
        Left = 16
        Top = 8
        Width = 75
        Height = 13
        Caption = 'Custom Images:'
      end
      object Splitter6: TSplitter
        Left = 0
        Top = 232
        Width = 345
        Height = 2
        Cursor = crArrow
        Align = alNone
        Beveled = True
        ResizeStyle = rsNone
      end
      object Label3: TLabel
        Left = 16
        Top = 240
        Width = 88
        Height = 13
        Caption = 'Custom Data Files:'
      end
      object lblDataCount: TLabel
        Left = 24
        Top = 256
        Width = 56
        Height = 13
        Caption = '0 Data Files'
      end
      object mmIssues: TMemo
        Left = 8
        Top = 304
        Width = 249
        Height = 105
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object btnIssueCheck: TButton
        Left = 264
        Top = 304
        Width = 73
        Height = 25
        Caption = 'Error Check'
        TabOrder = 1
        OnClick = btnIssueCheckClick
      end
      object btnIssuesSave: TButton
        Left = 264
        Top = 384
        Width = 73
        Height = 25
        Caption = 'Save'
        TabOrder = 2
        OnClick = btnIssuesSaveClick
      end
      object lbCustomImageList: TListBox
        Left = 16
        Top = 24
        Width = 241
        Height = 193
        ItemHeight = 13
        TabOrder = 3
      end
      object btnLoadCustomImage: TButton
        Left = 272
        Top = 78
        Width = 49
        Height = 25
        Caption = 'Load'
        TabOrder = 4
        OnClick = btnLoadCustomImageClick
      end
      object btnSaveCustomImage: TButton
        Left = 272
        Top = 110
        Width = 49
        Height = 25
        Caption = 'Save'
        TabOrder = 5
        OnClick = btnSaveCustomImageClick
      end
      object btnClearCustomImage: TButton
        Left = 272
        Top = 142
        Width = 49
        Height = 25
        Caption = 'Clear'
        TabOrder = 6
        OnClick = btnClearCustomImageClick
      end
      object btnReplayCheck: TButton
        Left = 264
        Top = 336
        Width = 73
        Height = 25
        Caption = 'Replay Check'
        TabOrder = 7
        OnClick = btnReplayCheckClick
      end
      object btnManageDataFiles: TButton
        Left = 208
        Top = 248
        Width = 75
        Height = 25
        Caption = 'Manage'
        TabOrder = 8
        OnClick = btnManageDataFilesClick
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 328
    object File1: TMenuItem
      Caption = '&File'
      object New1: TMenuItem
        Caption = '&New'
        OnClick = New1Click
      end
      object Open1: TMenuItem
        Caption = '&Open...'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = '&Save...'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As...'
        OnClick = SaveAs1Click
      end
      object BuildEXE1: TMenuItem
        Caption = '&Build NXP'
        OnClick = BuildEXE1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object ools1: TMenuItem
      Caption = 'Replays'
      object ReplayManager1: TMenuItem
        Caption = 'Replay Manager'
        OnClick = ReplayManager1Click
      end
      object ExportReplays1: TMenuItem
        Caption = 'Quick Export All'
        object LevelTitle1: TMenuItem
          Caption = '[Level Title]'
          OnClick = LevelTitle1Click
        end
        object RRLLLevelTitle1: TMenuItem
          Caption = '[RRLL] [Level Title]'
          OnClick = RRLLLevelTitle1Click
        end
        object RankLLLevelTitle1: TMenuItem
          Caption = '[Rank] [LL] [Level Title]'
          OnClick = RankLLLevelTitle1Click
        end
      end
      object estReplays1: TMenuItem
        Caption = 'Test Replays'
        OnClick = estReplays1Click
      end
    end
    object ools2: TMenuItem
      Caption = 'Tools'
      object SetLVLFilenamesFromLevelNames1: TMenuItem
        Caption = 'Set LVL Filenames From Level Names'
        OnClick = SetLVLFilenamesFromLevelNames1Click
      end
      object SetReplayFilenamesFromClassifications1: TMenuItem
        Caption = 'Set Replay Filenames From Classifications'
        OnClick = SetReplayFilenamesFromClassifications1Click
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Caption = '&About...'
        OnClick = About1Click
      end
    end
  end
end
