unit FEditTexts;

interface

uses
  DatManage, StrUtils, OpenSaveDlgs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFTextsForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    mmPreview: TMemo;
    Label2: TLabel;
    mmPostview: TMemo;
    Label3: TLabel;
    ebMusicNum: TEdit;
    ebFilename: TEdit;
    Label5: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    fLevel: TDatSection;
  public
    constructor Create(aOwner: TComponent; aLevel: TDatSection);
  end;

var
  FTextsForm: TFTextsForm;

implementation

{$R *.dfm}

constructor TFTextsForm.Create(aOwner: TComponent; aLevel: TDatSection);
var
  i: Integer;
  s: String;
begin
  inherited Create(aOwner);

  if not (aOwner is TForm) then Exit;
  Left := ((TForm(aOwner).Width - Width) div 2) + TForm(aOwner).Left;
  Top := ((TForm(aOwner).Height - Height) div 2) + TForm(aOwner).Top;

  fLevel := aLevel;

  s := '';
  mmPreview.Clear;
  for i := 1 to Length(aLevel.Text1) do
  begin
    if i < Length(aLevel.Text1) then
      if (aLevel.Text1[i] = #13) and (aLevel.Text1[i+1] = #10) then Continue;
    if (aLevel.Text1[i] = #10) or (aLevel.Text1[i] = #13) then
    begin
      mmPreview.Lines.Add(s);
      S := '';
      Continue;
    end;
    if aLevel.Text1[i] <> #00 then
      s := s + aLevel.Text1[i];
  end;
  if s <> '' then mmPreview.Lines.Add(s);

  s := '';
  mmPostview.Clear;
  for i := 1 to Length(aLevel.Text2) do
  begin
    if i < Length(aLevel.Text2) then
      if (aLevel.Text2[i] = #13) and (aLevel.Text2[i+1] = #10) then Continue;
    if (aLevel.Text2[i] = #10) or (aLevel.Text2[i] = #13) then
    begin
      mmPostview.Lines.Add(s);
      s := '';
      Continue;
    end;
    if aLevel.Text2[i] <> #00 then
      s := s + aLevel.Text2[i];
  end;
  if s <> '' then mmPostview.Lines.Add(s);

  ebMusicNum.Text := aLevel.MusicName;
  ebFilename.Text := aLevel.FileName;

  self.Caption := aLevel.LevelTitle;
end;

procedure TFTextsForm.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  fLevel.Text1 := '';
  for i := 0 to mmPreview.Lines.Count-1 do
  begin
    mmPreview.Lines[i] := Trim(mmPreview.Lines[i]);
    if (mmPreview.Lines[i] <> '') or (i <> mmPreview.Lines.Count-1) then
    begin
      fLevel.Text1 := fLevel.Text1 + mmPreview.Lines[i];
      if i <> mmPreview.Lines.Count-1 then fLevel.Text1 := fLevel.Text1 + #13 + #10;
    end;
  end;

  fLevel.Text2 := '';
  for i := 0 to mmPostview.Lines.Count-1 do
  begin
    mmPostview.Lines[i] := Trim(mmPostview.Lines[i]);
    if (mmPostview.Lines[i] <> '') or (i <> mmPostview.Lines.Count-1) then
    begin
      fLevel.Text2 := fLevel.Text2 + mmPostview.Lines[i];
      if i <> mmPostview.Lines.Count-1 then fLevel.Text2 := fLevel.Text2 + #13 + #10;
    end;
  end;

  fLevel.MusicName := ebMusicNum.Text;

  fLevel.FileName := ebFilename.Text;
  if ExtractFileExt(fLevel.FileName) = '' then fLevel.FileName := fLevel.FileName + '.lvl';
end;

end.
