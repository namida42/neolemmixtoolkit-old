unit FTextMassEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFMassEditText = class(TForm)
    mmTexts: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

implementation

{$R *.dfm}

procedure TFMassEditText.FormCreate(Sender: TObject);
begin
  Label1.Caption := 'Text Order:' + #13 +
                    '  Title Screen Second Line' + #13 +
                    '  Scroller Texts (x16)' + #13 +
                    '  Postview Texts (2 lines each):' + #13 +
                    '    None Saved; Very Bad; Bad;' + #13 +
                    '    Near-Pass; One Lemming Short;' + #13 +
                    '    Exact Pass; Good; Great;' + #13 +
                    '    100%' + #13 +
                    #13 +
                    #13 +
                    #13 +
                    'Maximum length 36 characters per' + #13 +
                    'line, except Title Screen Second' + #13 +
                    'line (max 32 characters).';
end;

end.
