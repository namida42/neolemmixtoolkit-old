unit FImportStrip;

{procedure TF_GSTool.btnLoadFrameClick(Sender: TObject);
var
  SubForm: TF_ImportStrip;
begin
  if (SelectedObject = nil) and (SelectedTerrain = nil) then Exit;
  SubForm := TF_ImportStrip.Create(self);
  if SubForm.ImportStrip then
  begin
    if IsPositiveResult(SubForm.ShowModal) then
    begin
      if SelectedTerrainNumber <> -1 then
      begin
        GraphicSet.TerrainImages[SelectedTerrainNumber].Assign(SubForm.BitmapSet[0]);
        imgPiece.Bitmap.Assign(GraphicSet.TerrainImages[SelectedTerrainNumber]);
        ScaleDisplayImage;
      end;

      if SelectedObjectNumber <> -1 then
      begin
        GraphicSet.ObjectImages[SelectedObjectNumber][FrameNumber].Assign(SubForm.BitmapSet[0]);
        RefreshObjectSettings;
        DisplayObjectImage;
      end;

      UpdateLists(false);
    end;
  end;
  SubForm.Free;
end;}

interface

uses
  PngInterface, {LemGraphicSet,}
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GR32, StdCtrls, GR32_Image, ExtCtrls;

type
  TF_ImportStrip = class(TForm)
    imgFrame: TImage32;
    btnCancel: TButton;
    btnOK: TButton;
    rgTransparency: TRadioGroup;
    procedure rgTransparencyClick(Sender: TObject);
    procedure imgFrameClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    fStripBMP: TBitmap32;
    fImage: TBitmap32;
    fTransparentColor: TColor32;
    procedure UpdateImageSet;
    procedure ShowDisplayFrame;
    procedure ScaleDisplayImage;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function ImportStrip: Boolean;
    function ImportFile(aFilename: String; Silent: Boolean = false): Boolean;
    property Bitmap: TBitmap32 read fImage;
  end;

implementation

{$R *.dfm}

constructor TF_ImportStrip.Create(aOwner: TComponent);
begin
  inherited;
  fStripBMP := TBitmap32.Create;
  fImage := TBitmap32.Create;
end;

destructor TF_ImportStrip.Destroy;
begin
  fStripBMP.Free;
  fImage.Free;
  inherited;
end;

function TF_ImportStrip.ImportStrip: Boolean;
var
  OpenDialog: TOpenDialog;
begin
  Result := false;
  OpenDialog := TOpenDialog.Create(self);
  try
    OpenDialog.Title := 'Select source image file';
    OpenDialog.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDialog.Filter := 'Image (BMP / PNG) File|*.bmp;*.png';
    if not OpenDialog.Execute then
      Exit;
    if not ImportFile(OpenDialog.FileName) then
      Exit;
    Result := true;
  finally
    OpenDialog.Free;
  end;
end;

function TF_ImportStrip.ImportFile(aFilename: String; Silent: Boolean = false): Boolean;
begin
  Result := false;
  try
    if Lowercase(ExtractFileExt(aFileName)) = '.png' then
    begin
      TPngInterface.LoadPngFile(aFileName, fStripBMP);
    end else begin
      fStripBMP.LoadFromFile(aFileName);
    end;
  except
    if not Silent then
      ShowMessage('Error: The image could not be loaded.');
    Exit;
  end;
  rgTransparency.ItemIndex := 0;
  fTransparentColor := $FFFF00FF;
  Result := true;
  UpdateImageSet;
end;

procedure TF_ImportStrip.UpdateImageSet;
var
  x, y: Integer;
begin
  fImage.Clear;
  fImage.SetSize(fStripBMP.Width, fStripBmp.Height);
  fStripBMP.DrawTo(fImage, 0, 0);
  if rgTransparency.ItemIndex = 1 then
    for y := 0 to fImage.Height-1 do
      for x := 0 to fImage.Width-1 do
        if (fImage.Pixel[x, y] and $FFFFFF) = (fTransparentColor and $FFFFFF) then
          fImage.Pixel[x, y] := 0
        else
          fImage.Pixel[x, y] := fImage.Pixel[x, y] or $FF000000;
  ShowDisplayFrame;
end;

procedure TF_ImportStrip.ShowDisplayFrame;
begin
  imgFrame.Bitmap.Assign(fImage);
  ScaleDisplayImage;
end;

procedure TF_ImportStrip.ScaleDisplayImage;
var
  TempBmp: TBitmap32;
  x, y, w, h: Integer;
  xx, yy: Integer;
begin
  TempBmp := TBitmap32.Create;
  TempBmp.Assign(fStripBMP);
  w := fStripBMP.Width;
  h := fStripBMP.Height;
  while (w > imgFrame.Width)
     or (h > imgFrame.Height) do
  begin
    w := w div 2;
    h := h div 2;
  end;

  while (w * 2 < imgFrame.Width)
    and (h * 2 < imgFrame.Height) do
  begin
    w := w * 2;
    h := h * 2;
  end;

  imgFrame.Bitmap.SetSize(imgFrame.Width, imgFrame.Height);
  imgFrame.Bitmap.Clear(ColorToRGB(imgFrame.Color));

  x := (imgFrame.Bitmap.Width - w) div 2;
  y := (imgFrame.Bitmap.Height - h) div 2;

  for yy := 0 to TempBmp.Height-1 do
    for xx := 0 to TempBmp.Width-1 do
      if rgTransparency.ItemIndex = 0 then
      begin
        if (TempBmp.Pixel[xx, yy] and $FF000000) = 0 then
          TempBmp.Pixel[xx, yy] := ColorToRGB(imgFrame.Color);
      end else begin
        if (TempBmp.Pixel[xx, yy] = fTransparentColor) then
          TempBmp.Pixel[xx, yy] := ColorToRGB(imgFrame.Color);
      end;


  TempBmp.DrawTo(imgFrame.Bitmap, Rect(x, y, x+w, y+h));
  imgFrame.Update;
  TempBmp.Free;
end;

procedure TF_ImportStrip.rgTransparencyClick(Sender: TObject);
begin
  UpdateImageSet;
end;

procedure TF_ImportStrip.imgFrameClick(Sender: TObject);
var
  ClickPoint: TPoint;
begin
  if rgTransparency.ItemIndex = 0 then Exit;
  ClickPoint := Mouse.CursorPos;
  ClickPoint := imgFrame.ScreenToClient(ClickPoint);
  ClickPoint := imgFrame.ControlToBitmap(ClickPoint);
  fTransparentColor := imgFrame.Bitmap.Pixel[ClickPoint.X, ClickPoint.Y];
  UpdateImageSet;
end;

procedure TF_ImportStrip.FormResize(Sender: TObject);
var
  BasePos: Integer;
begin
  BasePos := ClientWidth div 2;
  rgTransparency.Left := BasePos - 72;
  btnOK.Left := BasePos - 104;
  btnCancel.Left := BasePos + 8;
  ScaleDisplayImage;
end;

end.
