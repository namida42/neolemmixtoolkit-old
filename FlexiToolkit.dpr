program FlexiToolkit;

uses
  Forms,
  FFlexiToolMain in 'FFlexiToolMain.pas' {FFTKMain};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'NeoLemmix Flexi Toolkit';
  Application.CreateForm(TFFTKMain, FFTKMain);
  Application.Run;
end.
