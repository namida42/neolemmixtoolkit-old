unit FEditMusicList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFMusicList = class(TForm)
    Label1: TLabel;
    mmTracks: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
    procedure btnOKClick(Sender: TObject);
  private
    fMusics: TStringList;
  public
    procedure SetMusicList(aMusicList: TStringList);
  end;

var
  FMusicList: TFMusicList;

implementation

{$R *.dfm}

procedure TFMusicList.SetMusicList(aMusicList: TStringList);
var
  i: Integer;
begin
  fMusics := aMusicList;
  mmTracks.Clear;
  for i := 0 to fMusics.Count-1 do
    mmTracks.Lines.Add(fMusics[i]);
end;

procedure TFMusicList.btnOKClick(Sender: TObject);
var
  i: Integer;
begin
  fMusics.Clear;
  for i := 0 to mmTracks.Lines.Count-1 do
    if mmTracks.Lines[i] <> '' then
      fMusics.Add(mmTracks.Lines[i]);
end;

end.
