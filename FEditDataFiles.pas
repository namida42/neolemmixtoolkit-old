unit FEditDataFiles;

interface

uses
  FlexiNamedStreams, OpenSaveDlgs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFDataFiles = class(TForm)
    btnOK: TButton;
    lbFiles: TListBox;
    Label1: TLabel;
    btnAdd: TButton;
    btnDelete: TButton;
    btnExtract: TButton;
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnExtractClick(Sender: TObject);
  private
    fStreams: TNamedStreams;
    procedure UpdateList;
  public
    procedure SetStreams(aStreams: TNamedStreams);
  end;

  function CompareStreams(A, B: Pointer): Integer;

implementation

uses
  PngInterface, FImportStrip, CustomPopup;

{$R *.dfm}

function CompareStreams(A, B: Pointer): Integer;
var
  S1: TNamedMemoryStream absolute A;
  S2: TNamedMemoryStream absolute B;
begin
  Result := CompareStr(S1.Name, S2.Name);
end;

procedure TFDataFiles.UpdateList;
var
  i: Integer;
begin
  fStreams.Sort(CompareStreams);

  lbFiles.Items.BeginUpdate;
  try
    lbFiles.Items.Clear;
    for i := 0 to fStreams.Count-1 do
      lbFiles.Items.Add(fStreams[i].Name);
  finally
    lbFiles.Items.EndUpdate;
  end;
end;

procedure TFDataFiles.SetStreams(aStreams: TNamedStreams);
begin
  fStreams := aStreams;
  UpdateList;
end;

procedure TFDataFiles.btnAddClick(Sender: TObject);
var
  O: TOpenMulti;
  i: Integer;
  S: TNamedMemoryStream;
  ImageOpenType: Integer;
  UseImportDialog: Boolean;
  F: TF_ImportStrip;
  Opts: String;

  procedure MakeSpace(aName: String);
  var
    i: Integer;
  begin
    for i := fStreams.Count-1 downto 0 do
      if Lowercase(fStreams[i].Name) = Lowercase(aName) then
        fStreams.Delete(i);
  end;
const
  NOT_SELECTED = 0;
  YES_TO_ALL = 1;
  NO_TO_ALL = 2;
begin
  ImageOpenType := NOT_SELECTED;
  O := TOpenMulti.Create('All Files|*');
  try
    for i := 0 to O.Count-1 do
    begin
      if (Lowercase(ExtractFileExt(O[i])) = '.bmp') or (Lowercase(ExtractFileExt(O[i])) = '.png') then
        if ImageOpenType <> NO_TO_ALL then
        begin
          if ImageOpenType = YES_TO_ALL then
            UseImportDialog := true
          else begin
            Opts := 'Yes|No';
            if O.Count > 1 then
              Opts := Opts + '|Yes to all|No to all';
            case RunCustomPopup(self, 'Image Import', ExtractFileName(O[i]) + ' is a BMP or PNG image. Do you want to use the image import tool to set transparency' + #13 + 'and convert to a 32-bit PNG file?', Opts) of
              1: UseImportDialog := true;
              2: UseImportDialog := false;
              3: begin
                   UseImportDialog := true;
                   ImageOpenType := YES_TO_ALL;
                 end;
              4: begin
                   UseImportDialog := false;
                   ImageOpenType := NO_TO_ALL;
                 end;
            end;
          end;

          if UseImportDialog then
          begin
            F := TF_ImportStrip.Create(self);
            try
              if F.ImportFile(O[i]) then
              begin
                if IsPositiveResult(F.ShowModal) then
                begin
                  S := TNamedMemoryStream.Create;
                  S.Name := ChangeFileExt(ExtractFileName(O[i]), '.png');
                  TPngInterface.SavePngStream(S, F.Bitmap);
                  MakeSpace(S.Name);
                  fStreams.Add(S);
                  Continue;
                end;
              end;
            finally
              F.Free;
            end;
          end;
        end;

      S := TNamedMemoryStream.Create;
      S.Name := ExtractFileName(O[i]);
      S.LoadFromFile(O[i]);
      MakeSpace(S.Name);
      fStreams.Add(S);
    end;
  finally
    O.Free;
    UpdateList;
  end;
end;

procedure TFDataFiles.btnDeleteClick(Sender: TObject);
begin
  if lbFiles.ItemIndex = -1 then Exit;
  fStreams.Delete(lbFiles.ItemIndex);
  UpdateList;
end;

procedure TFDataFiles.btnExtractClick(Sender: TObject);
var
  S: TNamedMemoryStream;
  fn: String;
begin
  if lbFiles.ItemIndex = -1 then Exit;
  S := fStreams[lbFiles.ItemIndex];
  fn := SaveDialog('Files|*', S.Name);
  if fn <> '' then
    S.SaveToFile(fn);
end;

end.
