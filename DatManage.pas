unit DatManage;

interface

uses
  LemDosCmp, SysUtils, Classes, Contnrs, StrUtils, crc32;

const
  dst_Undetermined = 0;
  dst_Unknown = 1;
  dst_Level2KB = 2;
  dst_Level10KB = 3;
  dst_LevelVar = 4;

type
  TReplayStream = class(TMemoryStream)
    private
      fName: String;
      fClassification: Integer;
      fCrc: Cardinal; // CRC of the associated LVL file last time this replay was modified / validated
      fNote: String;
      fSuccess: Boolean;
      fIsNXRP: Integer;
      function GetIsNXRP: Boolean;
      function GetLevelID: Cardinal;
    public
      procedure ClearData;
      procedure Clear;
      property Name: String read fName write fName;
      property IsNXRP: Boolean read GetIsNXRP;
      property Classification: Integer read fClassification write fClassification;
      property CRC: Cardinal read fCrc write fCrc;
      property Note: String read fNote write fNote;
      property Success: Boolean read fSuccess write fSuccess;
      property LevelID: Cardinal read GetLevelID;
  end;

  TReplayStreams = class(TObjectList)
    private
      function GetItem(Index: Integer): TReplayStream;
    public
      constructor Create;
      function Add(Item: TReplayStream): Integer; overload;
      function Add: TReplayStream; overload;
      procedure Load(aPath, aLevel: String; aStringList: TStringList);
      procedure Save(aPath, aLevel: String; aStringList: TStringList);
      procedure Clone(aSrc: TReplayStreams);
      property Items[Index: Integer]: TReplayStream read GetItem; default;
      property List;
  end;

  TDatSection = class(TMemoryStream) // this has evolved into being primarily level-focused now instead of primarily DAT focused
    private
      fText1: String;
      fText2: String;
      fFileName: String;
      fReplays: TReplayStreams;
      function GetSectionType: Integer;
      function GetTitle: String;
      function GetMusicNumber: Integer;
      procedure SetMusicNumber(aValue: Integer);
      function GetMusicName: String;
      procedure SetMusicName(aValue: String);
      function GetToMusicName: Boolean;
      function GetLevelID: Cardinal;
      function GetCRC: Cardinal;
    public
      constructor Create;
      destructor Destroy; override;
      property SectionType: Integer read GetSectionType;
      property LevelTitle: String read GetTitle;
      property Text1: String read fText1 write fText1;
      property Text2: String read fText2 write fText2;
      property MusicNumber: Integer read GetMusicNumber write SetMusicNumber;
      property MusicName: String read GetMusicName write SetMusicName;
      property FileName: String read fFileName write fFileName;
      property Replays: TReplayStreams read fReplays;
      property CRC: Cardinal read GetCRC;
      property LevelID: Cardinal read GetLevelID;
  end;

  TSections = class(TObjectList)
  private
    function GetItem(Index: Integer): TDatSection;
  public
    function Add(Item: TDatSection): Integer;
    procedure Insert(Index: Integer; Item: TDatSection);
    procedure LoadFromFile(aFile: String);
    procedure LoadFromStream(aStream: TStream);
    procedure SaveToFile(aFile: String);
    procedure SaveToStream(aStream: TStream);
    property Items[Index: Integer]: TDatSection read GetItem; default;
    property List;
  end;

  function LeadZeroStr(aNum, aCnt: Integer): String;

implementation

{ Misc }

function LeadZeroStr(aNum, aCnt: Integer): String;
var
  Neg: Boolean;
begin
  Neg := aNum < 0;
  if Neg then aNum := aNum * -1;
  Result := IntToStr(aNum);
  while Length(Result) < aCnt do
    Result := '0' + Result;
  if Neg then Result := '-' + Result;
end;

{ TReplayStream }

function TReplayStream.GetLevelID: Cardinal;
var
  SL: TStringList;
  S: String;
  i: Integer;
begin
  if IsNXRP then
  begin
    SL := TStringList.Create;
    try
      Position := 0;
      SL.LoadFromStream(self);
      for i := 0 to SL.Count-1 do
        if Uppercase(LeftStr(Trim(SL[i]), 3)) = 'ID ' then
        begin
          S := Trim(SL[i]);
          S := RightStr(S, Length(S)-3);
          if LeftStr(S, 1) <> 'x' then
            S := 'x' + S; // compatibility with both older and newer NXRP replays
          Result := StrToIntDef(S, 0);
        end;
    finally
      SL.Free;
    end;
  end else begin
    Result := 0;
    Position := 30;
    Read(Result, 4);
  end;
end;

procedure TReplayStream.Clear;
begin
  Name := '';
  Classification := 0;
  CRC := 0;
  Note := '';
  Success := false;
  ClearData;
end;

procedure TReplayStream.ClearData;
begin
  fIsNXRP := 0;
  inherited Clear;
end;

function TReplayStream.GetIsNXRP: Boolean;
var
  Key: array[0..2] of Char;
begin
  if fIsNXRP = 0 then
  begin
    Position := 0;
    Read(Key, 3);
    if Key = 'NEO' then
      fIsNXRP := 2
    else
      fIsNXRP := 1;
  end;
  Result := fIsNXRP = 1;
end;

{ TDatSection }

constructor TDatSection.Create;
begin
  inherited;
  fReplays := TReplayStreams.Create;
end;

destructor TDatSection.Destroy;
begin
  fReplays.Free;
  inherited;
end;

function TDatSection.GetSectionType: Integer;
var
  b: Byte;
  T: Array[0..31] of Char;
  i: Integer;
  Invalid: Boolean;
begin
  Result := dst_Unknown;
  Seek(0, soFromBeginning);
  Read(b, 1);
  if b = 4 then
  begin
    Invalid := false;
    Seek(80, soFromBeginning);
    Read(T, 32);
    for i := 0 to 31 do
      if not Ord(T[i]) in [0, 33..126] then Invalid := true;
    if Invalid = false then
    begin
      Result := dst_LevelVar;
      Exit;
    end;
  end;
  case Size of
    2048: begin
            if b <> 0 then Exit;
            Seek(2016, soFromBeginning);
            Read(T, 32);
            for i := 0 to 31 do
              if not Ord(T[i]) in [0, 33..126] then Exit;
            Result := dst_Level2KB;
          end;
   10240: begin
            if not b in [1, 2, 3] then Exit;
            Seek(64, soFromBeginning);
            Read(T, 32);
            for i := 0 to 31 do
              if not Ord(T[i]) in [0, 33..126] then Exit;
            Result := dst_Level10KB;
          end;
  end;
end;

function TDatSection.GetTitle: String;
var
  st: Integer;
  T: Array[0..31] of Char;
begin
  Result := '';
  st := GetSectionType;
  if not st in [dst_Level2KB, dst_Level10KB, dst_LevelVar] then Exit;
  case st of
    dst_Level2KB: Seek(2016, soFromBeginning);
    dst_Level10KB: Seek(64, soFromBeginning);
    dst_LevelVar: Seek(80, soFromBeginning);
  end;
  Read(T, 32);
  Result := Trim(T);
end;

function TDatSection.GetMusicNumber: Integer;
var
  b: Byte;
begin
  Result := 0;
  if not SectionType in [dst_Level10KB, dst_LevelVar] then Exit;
  Seek(1, soFromBeginning);
  Read(b, 1);
  Result := b;
end;

function TDatSection.GetToMusicName: Boolean;
var
  b: Byte;
  w: Word;
begin
  // I really need to incorporate full-blown level loading/saving code into
  // the Flexi Toolkit; seeking file positions and stuff is really messy.
  Result := false;
  if not SectionType in [dst_LevelVar] then Exit;
  Seek($B0, soFromBeginning);
  repeat
    Read(b, 1);
    case b of
      0: Exit;
      1: Position := Position + $14;
      2: Position := Position + $10;
      3: Position := Position + $14;
      4: repeat
           Read(w, 2);
         until w = $FFFF;
      5: begin
           Position := Position + $10;
           Result := true;
           Exit;
         end;
      else Exit;
    end;
  until Position >= Size;
end;

function TDatSection.GetMusicName: String;
var
  TempResult: array[0..15] of Char;
begin
  Result := '';
  if not SectionType in [dst_LevelVar] then Exit;
  if not GetToMusicName then Exit;
  Read(TempResult, 16);
  Result := Trim(TempResult);
end;

procedure TDatSection.SetMusicName(aValue: String);
begin
  if not SectionType in [dst_LevelVar] then Exit;
  if not GetToMusicName then Exit; // should really add the header, but without proper loading/saving code it's complex as hell to do so
  if length(aValue) > 16 then
    aValue := LeftStr(aValue, 16);
  while length(aValue) < 16 do
    aValue := aValue + ' ';
  Write(aValue[1], 16);
end;

procedure TDatSection.SetMusicNumber(aValue: Integer);
var
  b: Byte;
  w: Word;
  s: String;
  i: Integer;
begin
  if not SectionType in [dst_Level10KB, dst_LevelVar] then Exit;
  if aValue < 0 then aValue := 0;
  if aValue > 255 then aValue := 0;
  b := aValue;
  Seek(1, soFromBeginning);
  Write(b, 1);
  if SectionType = dst_Level10KB then Exit;
  Seek(176, soFromBeginning);
  while Read(b, 1) = 1 do
    case b of
      0: Break;
      1: Seek(20, soFromCurrent);
      2: Seek(16, soFromCurrent);
      3: Seek(20, soFromCurrent);
      4: while Read(w, 2) = 2 do
           if w = $FFFF then Break;
      5: begin
           Seek(16, soFromCurrent);
           case aValue of
             0: s :=   '                ';
             253: s := '*               ';
             254: s := 'frenzy          ';
             255: s := 'gimmick         ';
             else begin
                    s := 'track_' + LeadZeroStr(aValue, 2);
                    while Length(s) < 16 do
                      s := s + ' ';
                  end;
           end;
           for i := 1 to 16 do
           begin
             b := Ord(s[i]);
             Write(b, 1);
           end;
         end;
      else Break;
    end;
end;

function TDatSection.GetLevelID: Cardinal;
var
  b: Byte;
begin
  Result := 0;
  Position := 0;
  Read(b, 1);
  if b <> 4 then Exit;
  Position := $38;
  Read(Result, 4);
end;

function TDatSection.GetCRC: Cardinal;
begin
  Position := 0;
  Result := CalculateCRC32(self);
end;

{ TSections }

function TSections.Add(Item: TDatSection): Integer;
begin
  Result := inherited Add(Item);
end;

function TSections.GetItem(Index: Integer): TDatSection;
begin
  Result := inherited Get(Index);
end;

procedure TSections.Insert(Index: Integer; Item: TDatSection);
begin
  inherited Insert(Index, Item);
end;

procedure TSections.LoadFromFile(aFile: String);
var
  TempStream: TFileStream;
begin
  TempStream := TFileStream.Create(aFile, fmOpenRead);
  try
    TempStream.Seek(0, soFromBeginning);
    LoadFromStream(TempStream);
  finally
    TempStream.Free;
  end;
end;

procedure TSections.SaveToFile(aFile: String);
var
  TempStream: TFileStream;
begin
  if FileExists(aFile) then DeleteFile(aFile);
  TempStream := TFileStream.Create(aFile, fmCreate);
  try
    TempStream.Seek(0, soFromBeginning);
    SaveToStream(TempStream);
  finally
    TempStream.Free;
  end;
end;

procedure TSections.LoadFromStream(aStream: TStream);
var
  TempSec: TDatSection;
begin
  Clear;
  while aStream.Position < aStream.Size do
  begin
    TempSec := TDatSection.Create;
    TempSec.Seek(0, soFromBeginning);
    DecompressDAT(aStream, TempSec);
    Add(TempSec);
  end;
end;

procedure TSections.SaveToStream(aStream: TStream);
var
  i: Integer;
begin
  for i := 0 to Count-1 do
  begin
    Items[i].Seek(0, soFromBeginning);
    CompressDAT(Items[i], aStream);
  end;
end;

{ TReplayStreams }

constructor TReplayStreams.Create;
var
  aOwnsObjects: Boolean;
begin
  aOwnsObjects := true;
  inherited Create(aOwnsObjects);
end;

function TReplayStreams.Add(Item: TReplayStream): Integer;
begin
  Result := inherited Add(Item);
end;

function TReplayStreams.Add: TReplayStream;
begin
  Result := TReplayStream.Create;
  inherited Add(Result);
end;

function TReplayStreams.GetItem(Index: Integer): TReplayStream;
begin
  Result := inherited Get(Index);
end;

procedure TReplayStreams.Clone(aSrc: TReplayStreams);
var
  i: Integer;
  Src: TReplayStream;
begin
  Clear;
  for i := 0 to aSrc.Count-1 do
    with Add do
    begin
      Src := aSrc[i];
      LoadFromStream(Src);
      Name := Src.Name;
      Classification := Src.Classification;
      CRC := Src.CRC;
      Note := Src.Note;
      Success := Src.Success;
    end;
end;

procedure TReplayStreams.Load(aPath, aLevel: String; aStringList: TStringList);
var
  i: Integer;
  L: String;
begin
  for i := 0 to aStringList.Count-1 do
    if aStringList[i] = aLevel then Break;
  if i = aStringList.Count then Exit;

  aPath := aPath + 'replays\';

  Inc(i);

  repeat
    L := aStringList[i];
    if L = '' then Break;
    if L = ChangeFileExt(L, '') then
      L := L + '.lrb';

    if FileExists(aPath + aLevel + '_' + L) then
      with Add do
      begin
        LoadFromFile(aPath + aLevel + '_' + L);
        Name := ChangeFileExt(L, '');
        Classification := StrToIntDef(aStringList[i+1], 0);
        CRC := StrToIntDef('x' + aStringList[i+2], 0);
        Note := aStringList[i+3];
        Success := aStringList[i+4] = 'PASS';
      end;

    inc(i, 5);
  until (i >= aStringList.Count);
end;

procedure TReplayStreams.Save(aPath, aLevel: String; aStringList: TStringList);
var
  i: Integer;
  ext: String;
begin
  if Count = 0 then Exit;

  aPath := aPath + 'replays\';

  aStringList.Add(aLevel);

  for i := 0 to Count-1 do
  begin
    if Items[i].IsNXRP then
      ext := 'nxrp'
    else
      ext := 'lrb';
    Items[i].SaveToFile(aPath + aLevel + '_' + Items[i].Name + '.' + ext);
    aStringList.Add(Items[i].Name + '.' + ext);
    aStringList.Add(IntToStr(Items[i].Classification));
    aStringList.Add(IntToHex(Items[i].CRC, 8));
    aStringList.Add(Items[i].Note);
    if Items[i].Success then
      aStringList.Add('PASS')
    else
      aStringList.Add('FAIL');
  end;

  aStringList.Add('');
end;

end.